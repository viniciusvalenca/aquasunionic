import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-indisponivel',
  templateUrl: 'indisponivel.html',
})
export class IndisponivelPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {      
  }

  backButtonAction() {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad IndisponivelPage');
  }
}
