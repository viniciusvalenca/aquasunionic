import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, ToastController, Platform, Events } from 'ionic-angular';
import { ProcurarLuminariasProvider } from '../../providers/procurar-luminarias/procurar-luminarias';
import { RecarregarProvider } from '../../providers/recarregar/recarregar';
import { HomeProvider } from '../../providers/home/home';
import { LuminariaProvider } from '../../providers/luminaria/luminaria';
import { PresetsProvider } from '../../providers/presets/presets';
import { UtilProvider } from '../../providers/util/util';
import { ValuesProvider } from '../../providers/values/values';
import { AutenticacaoProvider } from '../../providers/autenticacao/autenticacao';
import { IndisponivelPage } from '../indisponivel/indisponivel';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { FusoHoraProvider } from '../../providers/fuso-hora/fuso-hora';
import { Storage } from '@ionic/storage';
import { App } from "ionic-angular";
import { Zeroconf } from '@ionic-native/zeroconf';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RecarregarPage } from '../recarregar/recarregar';
import { AcessoNegadoPage } from '../acesso-negado/acesso-negado';
import { ProcurarLuminariasPage } from '../procurar-luminarias/procurar-luminarias';
import { ResetValoresProvider } from '../../providers/reset-valores/reset-valores';
import { EfeitoTempestadeProvider } from '../../providers/efeito-tempestade/efeito-tempestade';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@IonicPage()
@Component({
  selector: 'page-recuperar-end',
  templateUrl: 'recuperar-end.html',
})
export class RecuperarEndPage extends RecarregarProvider {
  autorizacao: any;

  resposta: any;

  loading: any;

  servicoMDNS: any = [];

  timer: any;

  nome: any;

  nomeLuminaria: any;

  nomesLuminarias: Array<any> = [];

  luminarias: Array<any> = [];

  numIp: any;

  n: number = 0

  urls: Array<any>;

  autorizacoes: Array<any>;

  nomeOutra: String;

  constructor(public app: App, public navCtrl: NavController, public zeroconf: Zeroconf,
    public toast: ToastController, public loader: LoadingController, public efeitoTempestadeProvider: EfeitoTempestadeProvider,
    public storage: Storage, public http: HttpClient, public events: Events, private screenOrientation: ScreenOrientation,
    public util: UtilProvider, private value: ValuesProvider, private platform: Platform,
    public alertCtrlr: AlertController, private autenticacaoProvider: AutenticacaoProvider,
    public procurarLuminariasProvider: ProcurarLuminariasProvider, private resetValoresProvider: ResetValoresProvider,
    public homeProvider: HomeProvider, public luminariaProvider: LuminariaProvider,
    public presetsProvider: PresetsProvider, public fusoHoraProvider: FusoHoraProvider) {
    super(app, homeProvider, luminariaProvider, efeitoTempestadeProvider, presetsProvider, storage, util, loader, toast, fusoHoraProvider);
    //this.botaoVisivel = this.navParams.get('param');  
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT).then(() => {
      console.info("Orientação: Retrato.");
    });  
    console.log("ionViewDidEnter RecuperarEndPage")
    this.carregarAutorizacao();
    this.carregarUrls();
    this.carregarNomeLuminaria();
    this.watchMDNS();
  }

  backButtonAction() { }

  ionViewWillEnter() { }

  montarAlerta() {
    let opcoesAlerta = {
      title: "Problema de comunicação.",
      message: "Não foi possível se conectar à luminária " + this.nomeLuminaria + ". Por favor, certifique que ela esteja ligada e conectada a rede Wifi. ",
      enableBackdropDismiss: false,
      buttons: [],
    };

    if (this.nomesLuminarias.length > 0) {
      opcoesAlerta.buttons.push({
        text: 'Tentar novamente',
        handler: () => {
          console.log('Tentando novamente');
          this.watchMDNS();
        }
      },
        {
          text: 'Conectar-se a outra luminária',
          handler: () => {
            console.log('Conectando-se a outra luminária...');
            this.alertaSelecionarLuminarias(this.luminarias);
          }
        },
        {
          text: 'Adicionar outra luminária',
          handler: () => {
            console.log('Adicionar outra luminária...');
            this.navCtrl.push(ProcurarLuminariasPage, { param1: 2, cancelar: true, callback: this.callback });
          }
        },
        {
          text: 'Reiniciar conf. de fábrica',
          handler: () => {
            console.log('Reiniciando padrões');
            this.confirmacaoReset();
          }
        },
        {
          text: 'Sair do App Aurora',
          handler: () => {
            console.log('Saiu do programa');
            this.platform.exitApp();
          }
        });
    } else {
      opcoesAlerta.buttons.push({
        text: 'Tentar novamente',
        handler: () => {
          console.log('Tentando novamente');
          this.watchMDNS();
        }
      },
        {
          text: 'Adicionar outra luminária',
          handler: () => {
            console.log('Adicionar outra luminária...');
            this.navCtrl.push(ProcurarLuminariasPage, { param1: 2, cancelar: true, recover: true, callback: this.callback });
          }
        },
        {
          text: 'Reiniciar conf. de fábrica',
          handler: () => {
            console.log('Reiniciando padrões');
            this.confirmacaoReset();
          }
        },
        {
          text: 'Sair do App Aurora',
          handler: () => {
            console.log('Saiu do programa');
            this.platform.exitApp();
          }
        });
    }
    let alert = this.alertCtrlr.create(opcoesAlerta)
    alert.present();
  }

  callback = data => {
    console.log("Callback: data = ", data)
    if (data) {
      this.montarAlerta();
    }
  };

  alertaAcesoNegado(luminarias) {
    let opcoesAlerta = {
      title: 'Acesso Negado',
      message: 'Seu acesso a esta luminária foi negado. Por favor, reinicie a luminária pelo botão localizado em seu painel conforme consta no manual e em seguida, reinicie seu aplicativo Aurora.',
      enableBackdropDismiss: false,
      buttons: [],
    };
    if (luminarias.length > 1) {
      opcoesAlerta.buttons.push({
        text: "OK",
        handler: () => {
          console.log('Ciente Acesso Negado'); this.montarAlerta();
        }
      });
    } else {
      opcoesAlerta.buttons.push({
        text: 'Reiniciar conf. de fábrica',
        handler: () => {
          console.log('Reiniciando padrões');
          this.confirmacaoReset();
        }
      });
    }
    let alert = this.alertCtrlr.create(opcoesAlerta);
    alert.present();
  }

  alertaSelecionarLuminarias(luminarias) {
    let opcoesAlerta = {
      title: "Selecionar Luminária",
      message: "Por favor, selecione uma luminária. ",
      enableBackdropDismiss: false,
      inputs: [],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelando a escolha');
            this.montarAlerta();
          }
        },
        {
          text: 'OK',
          handler: (dados) => {
            this.selecionarOutraLuminaria(dados);
          }
        }
      ]
    }

    for (let i = 0; i < luminarias.length; i++) {
      opcoesAlerta.inputs.push({ name: luminarias[i].alias, value: { ix: i, nome: luminarias[i].name }, label: luminarias[i].alias, type: 'radio' });
    }

    let alert = this.alertCtrlr.create(opcoesAlerta);
    alert.present();
  }

  carregarAutorizacao() {
    this.storage.get("auth").then(resp => {
      this.autorizacao = resp;
      console.log("Autorização recuperada (Storage): ", resp);
    });
    this.storage.get("auths").then(resp => {
      this.autorizacoes = resp;
      console.log("recuperar-ends.ts Autorizações recuperadas (Storage): ", resp);
    });
  }

  carregarNomeLuminaria() {
    this.storage.get("nomeLuminaria").then(res => {
      this.nomeLuminaria = res;
      this.carregarNomesLuminarias();
      console.log("nomeLuminaria = ", this.nomeLuminaria)
    });
  }

  carregarNomesLuminarias() {
    this.storage.get("nomesLuminarias").then(res => {
      this.nomesLuminarias = res;
      this.montarSelectLuminarias();
      console.log("nomesLuminarias = ", this.nomesLuminarias)
    });
  }

  montarSelectLuminarias() {
    if (this.nomesLuminarias.length > 0) {
      this.luminarias = this.nomesLuminarias;
      let index = this.luminarias.findIndex(i => i.alias === this.nomeLuminaria);
      console.log("index = ", index);
      if (index >= 0) {
        this.luminarias.splice(index, 1);
        this.urls.splice(index, 1);
        this.autorizacoes.splice(index, 1);
      }
      console.log("luminarias = ", this.luminarias);
    }
  }

  carregarUrls() {
    this.storage.get("urls").then(res => {
      this.urls = res;
      console.log("Urls carregadas = ", this.urls)
    })
  }

  atualizarLuminariaSelecionada(ipmdns) {
    this.storage.get("urls").then(resUrls => {
      console.log("resUrls = ", resUrls);
      let index = resUrls.findIndex(i => i.url === ipmdns);
      console.log("Index resUrls = ", index);
      this.storage.get("nomesLuminarias").then(res => {
        for (let i = 0; i < res.length; i++) {
          res[i].selecionada = false;
        }
        res[index].selecionada = true;
        this.publicarNomesLuminariasMenu(res);
        this.storage.set("nomeLuminaria", res[index].alias)
          .then(respNome => {
            console.log("Nome da luminária alterado");
          })
        this.storage.set("nomesLuminarias", res)
          .then(respNomes => {
            console.log("recuperar-ends.ts nomesLuminarias alterado: ", respNomes);
          });
      });
    });
  }

  publicarNomesLuminariasMenu(nomesLuminarias) {
    this.events.publish('nomes', nomesLuminarias);
    console.log("Tentando mudar o nome da variavel em app.components.ts...");
    this.loading.dismiss();
  }

  selecionarOutraLuminaria(dados) {
    console.log("Escolheu a luminária")
    console.log("valor da seleção (index)= ", dados.ix)
    console.log("valor da seleção (nome)= ", dados.nome)
    this.nomeOutra = dados.nome;
    console.log("Nome da seleção (this.nomesLuminarias[dados.ix].name) = ", this.nomesLuminarias[dados.ix].name)
    console.log("Url da seleção (this.urls[dados.ix].url) = ", this.urls[dados.ix].url)
    this.autorizacao = this.autorizacoes[dados.ix].auth;
    this.loading = this.loader.create({
      content: "Conectando a luminária: " + dados.nome + "...",
    });
    this.loading.present();
    this.getMDNS(dados.nome, this.urls[dados.ix].url);
  }
  //////////////////////////UNIFICAR => ProcurarLumináriasPage//////////////////////

  watchMDNS() {
    this.n = 0;
    this.servicoMDNS = [];
    this.zeroconf.close();
    // this.tentarNovamente = false;
    let posicao = 0;
    this.loading = this.loader.create({
      content: "Procurando a Luminária...",
    });

    // this.tentarNovamente = true;
    this.loading.present()
      .then(() => {
        ////////////////////////////TESTAR////////////////////////////
        this.timer = setTimeout(() => {
          this.storage.get("nomeLuminaria").then(nomeLuminaria => {
            this.storage.get("nomesLuminarias").then(nomesLuminarias => {
              console.log("recuperarEndPage nomeLuminaria = ", nomeLuminaria)
              console.log("recuperarEndPage this.servicoMDNS = ", this.servicoMDNS)
              let index00;
              let index;
              if (this.servicoMDNS.length > 0) {
                if (this.nomeOutra != null) {
                  index00 = nomesLuminarias.findIndex(i => i.alias === this.nomeOutra);//Procura o indice da luminaria atual                  
                } else {
                  index00 = nomesLuminarias.findIndex(i => i.alias === nomeLuminaria);//Procura o indice da luminaria atual
                }
                if (index00 >= 0) {
                  index = this.servicoMDNS.findIndex(i => i.name === nomesLuminarias[index00].name);//Verifica se o [index00].nome bate com a busca MDNS
                }
                if (index >= 0) {
                  console.log("recuperarEndPage this.servicoMDNS[" + index + "].name = ", this.servicoMDNS[index].name)
                  console.log("recuperarEndPage this.servicoMDNS[" + index + "].ipv4Addresses = ", this.servicoMDNS[index].ipv4Addresses)
                  if (String(this.servicoMDNS[index].ipv4Addresses) != '0.0.0.0') {
                    console.log("entrei em => if (String(result.service.ipv4Addresses) != '0.0.0.0' && (result.service.name === nomesLuminarias[index].name)) {}")
                    this.nome = this.servicoMDNS[index].name;
                    this.numIp = this.servicoMDNS[index].ipv4Addresses;
                    console.log("###this.servicoMDNS.name### = ", this.nome);
                    console.log("###this.servicoMDNS.ipv4Addresses### = ", this.numIp);
                    // this.n++;
                    // console.log("n = ", this.n);
                    // console.log("this.servicoMDNS.length = ", this.servicoMDNS.length);
                    // if (this.n >= this.servicoMDNS.length) {
                    if (this.numIp.length > 0) {
                      this.numIp = this.numIp[0];
                    }
                    this.value.editUrl(this.numIp);
                    this.getMDNS(this.nome, this.numIp);
                    // }
                  }
                } else {
                  this.loading.dismiss();
                  this.montarAlerta();
                }
              } else {
                this.loading.dismiss();
                this.montarAlerta();
              }
            });
          });
          this.zeroconf.stop();
          // this.toast.create({ message: "Nenhuma luminária Encontrada!", duration: 2000, position: 'bottom' }).present();
          // console.log("Tentar Novamente: (dps do zeroconf unwatch) ", this.tentarNovamente);

          // this.zeroconf.reInit();
          // this.appthis.app.getRootNav().setRoot(RecarregarPage);
          console.log("Timeout: Nenhuma Luminária encontrada ");
        }, 4000);
        ////////////////////////////////////////////////////////   

        this.zeroconf.watch('_coil-aurora._tcp.', 'local.')
          .subscribe(result => {
            console.log("Iniciando a busca por luminárias... (recupararEndPage)")
            if (result.action == 'resolved') {
              console.log('service resolved', result.service);
              this.servicoMDNS.push(result.service);
              console.log("recuperarEndPage result.service.name = ", result.service.name);
              console.log("recuperarEndPage result.service.ipv4Addresses = ", result.service.ipv4Addresses);
            }
          })
      });
  }

  pararWatchMDNS() {
    this.zeroconf.unwatch('_http._tcp.', 'local.')
      .then(res => {
        console.log("unwatch() res: ", res);
      }).catch(err => {
        console.error("unwatch() err: ", err);
      });
    clearTimeout(this.timer);
  }

  getMDNS(nmdns, ipmdns) {
    let auth = new Date().toLocaleString();
    // this.pararWatchMDNS();//NOVO! TESTAR!
    // this.zeroconf.close();
    // this.loading = this.loader.create({
    //   content: "Configurando o APP Aurora...",
    // });
    // this.loading.present()
    //   .then(() => {
    this.procurarLuminariasProvider.getDadosLuminariaIP(ipmdns)//Tenta resolver por IP
      .then((resp) => {
        this.resposta = resp;
        console.log("GET Request Resolvendo IP Completo ", resp);
        this.autenticacaoProvider.getAutorizacaoJSON(ipmdns)//Busca a existência de uma chave no ESP
          .then(respauth => {
            console.log("Autorizacao IP: resp =", respauth);
            if (respauth == null || respauth['autorizacao'] == "0") {//Se não exitir chave...***Verificar Ncessidade*****
              this.storage.set("firstRun", 1)//Grava o atributo firstRun para indicar que o APP já foi executado 1 vez
                .then(res00 => {
                  this.storage.set("url", ipmdns)//Grava na memória o valor acima para futuras execuções do APP
                    .then(res01 => {
                      this.value.setUrl(ipmdns);//Altera o valor da variável url do ESP no APP
                      console.log("Sucesso ao gravar a url (IP) em values.provider: " + ipmdns + "=> ", res01)
                      // this.autenticacaoProvider.setAutorizacaoJSON(auth, ipmdns)//Grava no ESP a chave de autenticação
                      //   .then(res02 => {
                      // console.log("Sucesso ao gravar autorização no ESP!", res02);
                      this.loading.dismiss();
                      console.log("carregarLuminaria(nomeLuminaria) ", nmdns)
                      this.carregarLuminaria(nmdns);
                      // }).catch(err02 => {
                      //   this.loading.dismiss();
                      //   console.error("Erro ao gravar autorização no ESP!", err02);
                      // })
                    }).catch(err01 => {
                      this.loading.dismiss();
                      console.error("Erro ao gravar a url values.provider: " + ipmdns + "=> ", err01)
                    });
                })
                .catch((e) => {
                  this.resposta = e;
                  console.error("ERRO Resolvendo IP!", e);
                  this.loading.dismiss();
                });
            } else if (respauth['autorizacao'] == this.autorizacao) {
              this.storage.set("url", ipmdns)//Grava na memória o valor acima para futuras execuções do APP
                .then(res01 => {
                  console.log("Sucesso ao gravar a url (IP) em values.provider: " + ipmdns + "=> ", res01)
                  this.value.setUrl(ipmdns);
                  this.atualizarLuminariaSelecionada(ipmdns);
                  if (this.autorizacao.length > 0) {
                    this.autorizacao = this.autorizacao[0];
                  }
                  this.storage.set("auth", this.autorizacao).then(resAuth => {
                    console.log("Autorização atualizada", resAuth);
                  })
                  this.loading.dismiss();
                  console.log("carregarLuminaria(nomeLuminaria) ", nmdns)
                  this.carregarLuminaria(nmdns);
                });
            } else {
              console.log("Acesso NEGADO!");
              console.log("respauth['autorizacao']: ", respauth['autorizacao']);
              console.log("this.autorizacao ", this.autorizacao);
              this.storage.get("auth").then(resp => {
                this.autorizacao = resp;
                console.log("Autorização recuperada (Storage): ", resp);
              });
              this.loading.dismiss();
              this.alertaAcesoNegado(this.nomesLuminarias);
              // this.toast.create({ message: "Acesso Negado à esta luminária!", duration: 5000, position: 'top' }).present();
              // this.navCtrl.push(AcessoNegadoPage);
            }
          }).catch(erroauth => {
            console.error("IP => Erro (POST) ao tentar autenticar...", erroauth);
            this.loading.dismiss();
            // this.navCtrl.push(RecarregarPage);
            this.alertaAcesoNegado(this.nomesLuminarias);
          })
      }).catch((e) => {
        console.error("ERRO Resolvendo IP!", e);
        if (this.nomeOutra != null) {
          this.watchMDNS();
          this.nomeOutra = null;//Verificar se a variável não ficará nula antes de percorrer o método watchMdns();
        } else {
          this.loading.dismiss();
          // this.navCtrl.push(RecarregarPage);       
          this.montarAlerta();
        }

      });
    // });
  }

  confirmacaoReset() {
    this.resetValoresProvider.confirmacaoReset();
  }

}
