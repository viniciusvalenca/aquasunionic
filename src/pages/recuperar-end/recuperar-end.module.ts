import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecuperarEndPage } from './recuperar-end';

@NgModule({
  declarations: [
    RecuperarEndPage,
  ],
  imports: [
    IonicPageModule.forChild(RecuperarEndPage),
  ],
})
export class RecuperarEndPageModule {}
