import { Component } from '@angular/core';
import { IonicPage, AlertController, NavController } from 'ionic-angular';
import { Platform } from 'ionic-angular/platform/platform';
import { ResetValoresProvider } from '../../providers/reset-valores/reset-valores';
import { ProcurarLuminariasPage } from '../procurar-luminarias/procurar-luminarias';

@IonicPage()
@Component({
  selector: 'page-acesso-negado',
  templateUrl: 'acesso-negado.html',
})
export class AcessoNegadoPage {

  constructor(private alertCtrlr: AlertController, public navCtrl: NavController, 
    public platform: Platform, private resetValoresProvider: ResetValoresProvider) {
    this.AcessoNegadoAlerta();
  }

  backButtonAction() { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AcessoNegadoPage');
  }

  AcessoNegadoAlerta() {
    let alert = this.alertCtrlr.create({
      title: "Acesso negado à luminária",
      message: "Você não possui acesso à essa luminária. Caso ela tenha sido reiniciada manualmente para os padrões de fábrica ou aplicativo tenha sido reinstalado, será necessáio realizar o procedimento inicial novamente no aplicativo. ",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Reiniciar App Aurora',
          handler: () => {
            this.confirmacaoReset();
            console.log('Reiniciando padrões');
          }
        },
        {
          text: 'Adicionar outra luminária',
          handler: () => {
            console.log('Adicionar outra luminária...');
            this.navCtrl.push(ProcurarLuminariasPage, { param1: 2, cancelar: true });
          }
        },
        {
          text: 'Sair do App Aurora',
          handler: () => {
            console.log('Saiu do programa');
            this.platform.exitApp();
          }
        }
      ]
    });
    alert.present();
  }

  confirmacaoReset() {
    this.resetValoresProvider.confirmacaoReset();
  }

}
