import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcessoNegadoPage } from './acesso-negado';

@NgModule({
  declarations: [
    AcessoNegadoPage,
  ],
  imports: [
    IonicPageModule.forChild(AcessoNegadoPage),
  ],
})
export class AcessoNegadoPageModule {}
