import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TwinstarPage } from './twinstar';

@NgModule({
  declarations: [
    TwinstarPage,
  ],
  imports: [
    IonicPageModule.forChild(TwinstarPage),
  ],
})
export class TwinstarPageModule {}
