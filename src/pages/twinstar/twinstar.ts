import { TwinstarProvider } from './../../providers/twinstar/twinstar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { UtilProvider } from '../../providers/util/util';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-twinstar',
  templateUrl: 'twinstar.html',
})

export class TwinstarPage {
  horaAtual: any;
  msFuncionamento: any = 0;
  msIntervalo: any = 0;
  tempoFuncionamento: any = "00:00";
  tempoIntervalo: any = "00:00";
  ligarDesligar: boolean;
  valorIntensidade: any = 250;
  intensidade: any = "fraca";
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public util: UtilProvider,
    private storage: Storage, public events: Events, private twinStarProvider: TwinstarProvider,
    public loader: LoadingController) {
    // this.tempoFuncionamento = new Date(0).toISOString();
    this.carregarDados();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TwinstarPage');
  }

  carregarDados() {
    this.loading = this.loader.create({
      content: "Carregando...",
    });
    this.loading.present().
      then(() => {
        this.storage.get("msFuncionamento")
          .then((resMsFuncionamento) => {
            console.log("this.msFuncionamento (Storage) = ", resMsFuncionamento)
            this.tempoFuncionamento = this.util.convertMilisegundosHoraMinSeg(resMsFuncionamento);
            console.log("this.tempoFuncionamento (Storage) = ", this.tempoFuncionamento)
            this.storage.get("msIntervalo")
              .then((resMsIntervalo) => {
                console.log("this.msIntervalo (Storage) = ", resMsIntervalo)
                this.tempoIntervalo = this.util.convertMilisegundosHoraMinSeg(resMsIntervalo);
                console.log("this.tempoIntervalo (Storage) = ", this.tempoIntervalo)
                this.storage.get("valorIntensidade")
                  .then((resValorIntensidade) => {
                    console.log("this.valorIntensidade (Storage) = ", resValorIntensidade)
                    this.ajustarIntensidade(resValorIntensidade);
                    this.loading.dismiss();
                  })
              })
          })
      });
  }

  ligarDesligarTwinStar() {
    this.ligarDesligar = !this.ligarDesligar;
    let dados = {
      TSligado: this.ligarDesligar,
      TSintervalo: this.msIntervalo,
      TSduracao: this.msFuncionamento,
      TSpotencia: this.valorIntensidade
    }
    this.twinStarProvider.setTwinStar(dados);
  }

  ajustarTempo() { }

  ajustarIntensidade(valorIntensidade) {
    switch (valorIntensidade) {
      case 250: this.intensidade = "fraca";
        break;
      case 500: this.intensidade = "média";
        break;
      case 750: this.intensidade = "forte";
        break;
    }
    this.valorIntensidade = valorIntensidade;
    console.log("Intensidade = ", this.intensidade)
    this.storage.set("valorIntensidade", this.valorIntensidade);
  }

  tempoTwinStar() {
    console.log("this.tempoFuncionamento = ", this.tempoFuncionamento)
    this.msFuncionamento = this.util.convertHoraMinSegMilisegundos(this.tempoFuncionamento);
    console.log("this.msFuncionamento = ", this.msFuncionamento)
    this.storage.set("msFuncionamento", this.msFuncionamento);
  }

  intervaloTwinStar() {
    console.log("this.tempoIntervalo = ", this.tempoIntervalo)
    this.msIntervalo = this.util.convertHoraMinSegMilisegundos(this.tempoIntervalo);
    console.log("this.msIntervalo = ", this.msIntervalo)
    this.storage.set("msIntervalo", this.msIntervalo);
  }

}
