import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FusoHoraPage } from './fuso-hora';

@NgModule({
  declarations: [
    FusoHoraPage,
  ],
  imports: [
    IonicPageModule.forChild(FusoHoraPage),
  ],
})
export class FusoHoraPageModule {}
