import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FusoHoraProvider } from '../../providers/fuso-hora/fuso-hora';
import { Platform } from 'ionic-angular/platform/platform';
import { ViewController } from 'ionic-angular';
import { Select } from 'ionic-angular/components/select/select';

@IonicPage()
@Component({
  selector: 'page-fuso-hora',
  templateUrl: 'fuso-hora.html',
})
export class FusoHoraPage {

  resposta: any;

  loading: any;

  fusoHorario: any;

  utc: any;

  horarioVerao: boolean = false;

  horarioVeraoAtual: boolean = false;
  
  @ViewChild('selectFuso') select: Select;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrlr: AlertController,
    public loader: LoadingController, private storage: Storage, private toast: ToastController,
    private fusoHoraProvider: FusoHoraProvider, platform: Platform, private viewCtrl: ViewController) {
    this.carregarConfiguracoes();
  }

  backButtonAction() {
    console.log('backButtonAction FusoHoraPage');
    if (this.select._isFocus)
      this.select.close();
    else
      this.navCtrl.popToRoot();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad FusoHoraPage');
  }

  mudarHorarioVerao() {
    this.horarioVerao = !this.horarioVerao;
    // this.storage.set('horarioVerao', this.horarioVerao)
    //   .then(resp => {
    //     console.log("Horário de verão: ", resp);
    //   });
  }

  carregarConfiguracoes() {
    this.loading = this.loader.create({
      content: "Carregando configurações...",
    });
    this.loading.present()
      .then(() => {
        this.carregarHorarioVerao();
      });
  }

  carregarHorarioVerao() {
    this.storage.get('horarioVerao')
      .then((resp) => {
        this.horarioVerao = resp;
        this.horarioVeraoAtual = this.horarioVerao;
        console.log("Carregado horarioVerao => ", resp);
        console.log("this.horarioVerao => ", this.horarioVerao);
        this.carregarFusoHorario();
      }).catch(error => {
        this.loading.dismiss();
        console.error("Erro ao tentar recuperar UTC: ", error);
      });
  }

  carregarFusoHorario() {
    this.storage.get('utc')
      .then((resp) => {
        console.log("UTC => ", resp);
        this.utc = resp;
        console.log("this.utc = ", this.utc)
        this.fusoHorario = this.utc;
        this.loading.dismiss();
      }).catch(error => {
        this.loading.dismiss();
        console.error("Erro ao tentar recuperar UTC: ", error);
      });
  }

  mudarFusoHorario() {
    this.storage.set("horarioVerao", this.horarioVerao)
      .then(res => {
        console.log("horarioVerao guardado!");
      }).catch(err => {
        console.error("ERRO ao guardar o horarioVerao!");
      })
    console.log("Fuso horário escolhido: ", this.fusoHorario);
    if (this.horarioVerao) {
      this.fusoHorario = String(Math.floor(this.fusoHorario) + 1);
      console.log("this.fusoHorario com horário de verão: ", this.fusoHorario);
    }
    let dados = { utc: this.fusoHorario };
    this.resposta = this.fusoHoraProvider.setUtc(dados)
      .then((resp) => {
        console.log("POST resposta /setUtc(): ", resp);
        this.toast.create({ message: "Fuso Horário Alterado!", duration: 2000, position: 'top' }).present();
        console.log("this.fusoHorario: ", this.fusoHorario);
        if (this.horarioVerao) {
          this.fusoHorario = String(Math.floor(this.fusoHorario) - 1);
          console.log("this.fusoHorario Voltando ao valor original: ", this.fusoHorario);
        }
        this.utc = this.fusoHorario;
        this.storage.set('utc', this.utc)
          .then((resp) => {
            console.log("storage.set => this.utc = ", resp);
          }).catch(error => {
            console.error("Erro storage.set => UTC app.component.ts: ", error);
          });
        this.horarioVeraoAtual = this.horarioVerao;
      })
      .catch((e) => {
        console.error("ERRO! /setUtc(): ", e);
        this.toast.create({ message: "Falha ao alterar o Fuso Horário!", duration: 2000, position: 'top' }).present();
        // this.utc = this.fusoHorario;
      });

  }

}
