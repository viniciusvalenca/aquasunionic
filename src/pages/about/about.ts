import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  @ViewChild(Slides) slides: Slides;

  slideNum: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.slideNum = this.navParams.get('slide');
  }

  ionViewDidLoad() {
    console.log("this.slideNum", this.slideNum)
    setTimeout(()=>{
      this.goToSlide(this.slideNum);
    }, 500);    
  }

  goToSlide(slideNum) {
    console.log("goToSlide(slideNum) slideNum", slideNum)
    this.slides.slideTo(slideNum, 500);
  }

  fechar() {
    this.navCtrl.pop();
  }

}
