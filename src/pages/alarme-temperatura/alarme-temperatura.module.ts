import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlarmeTemperaturaPage } from './alarme-temperatura';
import { MultiPickerModule } from 'ion-multi-picker';

@NgModule({
  declarations: [
    AlarmeTemperaturaPage,
  ],
  imports: [
    IonicPageModule.forChild(AlarmeTemperaturaPage),
    MultiPickerModule,
  ],
})
export class AlarmeTemperaturaPageModule {}
