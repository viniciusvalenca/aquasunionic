import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Storage } from '@ionic/storage';
import { AlarmeTemperaturaProvider } from '../../providers/alarme-temperatura/alarme-temperatura';
import { Push, PushObject, PushOptions } from "@ionic-native/push";
import { Platform } from 'ionic-angular/platform/platform';
import { MultiPicker } from 'ion-multi-picker';
import { IndisponivelPage } from '../indisponivel/indisponivel';

@IonicPage()
@Component({
  selector: 'page-alarme-temperatura',
  templateUrl: 'alarme-temperatura.html',
})
export class AlarmeTemperaturaPage {
  loading: any;

  ligado: boolean;

  ligadoAtual: boolean;

  emailLigado: any;

  tempMax: any = '36';

  tempMaxAtual: any = '36';

  tempMin: any = '25';

  tempMinAtual: any = '25';

  temperaturas: any;

  email: any;

  emailAtual: any = '';

  senderID: any = '726312007995';

  msg: any;

  dismissedLoading: boolean;

  @ViewChild('tempMinOpt') tempMinOpt: MultiPicker;
  @ViewChild('tempMaxOpt') tempMaxOpt: MultiPicker;

  constructor(public navCtrl: NavController, public navParams: NavParams, private push: Push,
    private toast: ToastController, private storage: Storage, public alertCtrlr: AlertController,
    public loader: LoadingController, private alarmeTemperaturaProvider: AlarmeTemperaturaProvider,
    private platform: Platform) {
    let temps = [];
    for (let i = 0; i < 60; i++) {
      temps.push({ text: String(i + 1), value: String(i + 1) })
      console.log("Adicionando...", i);
    }
    this.temperaturas = [
      {
        name: 'col1',
        options: temps,
      },
    ];
    // this.carregarEmail();
    this.carregarConfiguracaoAlarme();
  }

  backButtonAction() {
    console.log('backButtonAction AlarmeTemperaturaPage');
    if (this.tempMinOpt._isOpen) {
    }
    else if (this.tempMaxOpt._isOpen) {
    }
    else
      this.navCtrl.popToRoot();
  }

  ionViewDidEnter() { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlarmeTemperaturaPage');
  }

  carregarEmail() {
    this.loading = this.loader.create({
      content: "Carregando configurações...",
    });
    this.loading.present()
      .then(() => {
        this.storage.get("email")
          .then(resp => {
            this.email = resp;
            this.emailAtual = this.email;
            console.log("Temperatura Email => ", this.email);
            this.loading.dismiss();
          }).catch(erro => {
            this.loading.dismiss();
          })
      });
  }

  carregarConfiguracaoAlarme() {
    this.loading = this.loader.create({
      content: "Carregando configurações...",
    });
    this.loading.present().then(() => {
      this.storage.get('alarmeLigado').then((respostaLigado) => {
        console.log('respostaLigado (Storage)', respostaLigado);
        this.ligado = respostaLigado;
        console.log('this.ligado (Storage)', this.ligado);
        this.ligadoAtual = this.ligado;
        this.storage.get('tempMax').then((respostaTempMax) => {
          this.tempMax = respostaTempMax;
          this.tempMaxAtual = this.tempMax;
          console.log("Temperatura Máxima Storage => ", this.tempMax)
        })
        this.storage.get('tempMin').then((respostaTempMin) => {
          this.tempMin = respostaTempMin;
          this.tempMinAtual = this.tempMin;
          console.log("Temperatura Mínima Storage => ", this.tempMin)
        })

        this.loading.dismiss();
      }).catch((e) => {
        this.loading.dismiss();
        console.log('erro estado do Alarme: ', this.ligado);
        this.toast.create({ message: "Erro ao tentar carregar os valores de temperatura do alarme!", duration: 2000, position: 'top' }).present();
      });
    });
  }

  paginaIndisponivel() {
    this.navCtrl.setRoot(IndisponivelPage);
  }

  checarTemperaturasMax() {
    let alerta = this.alertCtrlr.create({
      title: 'Atenção',
      subTitle: 'O valor de temperatura máxima não pode ser inferior ao valor de temperatura mínima.',
      buttons: ['OK']
    });
    if (this.tempMin != null) {
      if (this.tempMin > this.tempMax) {
        this.tempMin = this.tempMax;
        console.log("Diferença de temperatua detectada! tempMin: " + this.tempMin + " tempMax: " + this.tempMax)
        alerta.present();
      }
      this.salvarAlarme("Temperatura Máxima");
    }
  }

  checarTemperaturasMin() {
    let alerta = this.alertCtrlr.create({
      title: 'Atenção',
      subTitle: 'O valor de temperatura mínima não pode ser superior ao valor de temperatura máxima.',
      buttons: ['OK']
    });
    if (this.tempMax != null) {
      if (this.tempMax < this.tempMin) {
        this.tempMax = this.tempMin;
        console.log("Diferença de temperatua detectada! tempMax: " + this.tempMax + " tempMin: " + this.tempMin)
        alerta.present();
      }
      this.salvarAlarme("Temperatura Mínima");
    }
  }

  ligarDesligarAlarmeEmail() {
    this.emailLigado = !this.emailLigado;
    this.storage.set('emailLigado', this.emailLigado).then(resp => {
      let estado;
      if (!this.emailLigado) {
        estado = "Desativando ";
      } else {
        estado = "Ativando ";
      }
    });
    this.pushsetup();
  }

  ligarDesligarAlarme() {
    // let alert;
    // this.ligado = !this.ligado;
    // if ((this.tempMin == null || this.tempMin <= 0) || (this.tempMax == null || this.tempMax >= 127)) {
    //   this.ligado = false;
    //   console.log("Alarme ligado (dentro do if): ", this.ligado);
    //   alert = this.alertCtrlr.create({
    //     title: "Valores de temperatura",
    //     message: "Por favor, informe os valores mínimo e máximo desejados para o alarme de temperatura antes de liga-lo.",
    //     buttons: ['OK'],
    //   });
    //   alert.present();
    // } else {
    //   this.pushsetup();
    // }    
    this.storage.set('alarmeLigado', this.ligado)
      .then(resp => {
        console.log("alarmeLigado guardado (Storage) = " + this.ligado + ", resp = " + resp);
      });
    if (!this.ligado) {
      this.msg = "Desativando";
    } else {
      this.msg = "Ativando ";
    }
    this.pushsetup();
  }

  pushsetup() {
    console.log("[(ngModel)]");
    this.loading = this.loader.create({
      content: this.msg + " alarme",
    });
    console.log("Alarme ligado: ", this.ligado)
    let alert;
    const options: PushOptions = {
      ios: {
        alert: true,
        badge: true,
        sound: true,
      },
      android: {
        senderID: this.senderID,
        sound: true,
        vibrate: true,
        forceShow: true,
      },
      windows: {},
    };

    const pushObject: PushObject = this.push.init(options);
    console.log("this.emailLigado = ", this.emailLigado);
    console.log("this.ligado = ", this.ligado);
    if (this.ligado) {
      this.loading.present();
      setTimeout(() => {
        if (!this.dismissedLoading) {
          this.loading.dismiss();
        }
        this.dismissedLoading = false;
      }, 3000);
      console.log("pushObject.on(registration).subscribe((registration: any) - 1");
      pushObject.on("registration").subscribe((registration: any) => {
        console.log("Token: ->", registration.registrationId);
        this.storage.get("token").then(respGetToken => {
          // if (registration.registrationId != respGetToken) {
          let regId = { regId: registration.registrationId, regIdLigado: true };
          this.alarmeTemperaturaProvider.setRegId(regId)
            .then(respRegid => {
              this.dismissedLoading = true;
              this.loading.dismiss();
              this.storage.set("token", registration.registrationId)
                .then(respTken => {
                  console.log("Token guardado! ", respRegid);
                }).catch(erroToken => {
                  console.error("Erro ao guardar Token! ", respRegid);
                  // this.paginaIndisponivel();
                });
            }).catch(erroRegId => {
              this.dismissedLoading = true;
              this.loading.dismiss();
              this.exibirAlerta2();
              this.ligado = false;
              console.error("Erro ao Enviar Token (setRegId)! ", erroRegId);
              // this.paginaIndisponivel();
            });
          // }
        }).catch(err => {
          this.dismissedLoading = true;
          this.loading.dismiss();
          this.exibirAlerta2();
          this.ligado = false;
          console.error("ERRO ao recuperar token (STORAGE)")
        })
      });

      console.log("pushobject 1 = ", pushObject);


      pushObject.on("notification").subscribe((notification: any) => {
        this.loading.dismiss();
        this.dismissedLoading = true;
        console.log("Notification: ", notification);
        if (notification.additionalData.foreground) {
          alert = this.alertCtrlr.create({
            title: notification.label,
            message: notification.message,
          });
          alert.present();
        }
        this.dismissedLoading = true;
      });

      pushObject.on('error').subscribe(error => {
        this.loading.dismiss();
        console.error('Erro no plugin Push ', error)
        if (String(error) == "Error: SERVICE_NOT_AVAILABLE") {
          this.exibirAlerta();
          this.ligado = false;
        }
      });

      // this.push.hasPermission()
      //   .then((res: any) => {
      //     if (res.isEnabled) {
      //       console.log('Permissão para enviar notificaçãoes PUSH concedida!', res);
      //     } else {
      //       console.log('Permissão para enviar notificaçãoes PUSH NEGADA!', res);
      //     }

      //   });
    } else {
      this.loading.present();
      setTimeout(() => {
        if (!this.dismissedLoading) {
          this.loading.dismiss();
        }
        this.dismissedLoading = false;
      }, 3000);
      console.log("pushObject.on(registration).subscribe((registration: any) - 2");
      pushObject.on("registration").subscribe((registration: any) => {
        console.log("Token: ->", registration.registrationId);
        this.storage.get("token").then(respGetToken => {
          // if (registration.registrationId != respGetToken) {
          let regId = { regId: registration.registrationId, regIdLigado: false };
          this.alarmeTemperaturaProvider.setRegId(regId)
            .then(respRegid => {
              this.dismissedLoading = true;
              this.loading.dismiss();
              this.storage.set("token", registration.registrationId)
                .then(respTken => {
                  console.log("Token guardado! ", respRegid);
                }).catch(erroToken => {
                  console.error("Erro ao guardar Token! ", respRegid);
                  // this.paginaIndisponivel();
                });
            }).catch(erroRegId => {
              this.dismissedLoading = true;
              this.loading.dismiss();
              this.exibirAlerta2();
              this.ligado = true;
              console.error("Erro ao Enviar Token (setRegId)! ", erroRegId);
              // this.paginaIndisponivel();
            });
          // }
        }).catch(err => {
          this.dismissedLoading = true;
          this.loading.dismiss();
          this.exibirAlerta2();
          console.error("ERRO ao recuperar token (STORAGE)")
        })
      });

      console.log("pushobject 2 = ", pushObject);


      this.push.listChannels().then(channels => {
        console.log("Canais abertos: ", channels);
      });

      pushObject.unregister().then(res => {
        console.log("Serviço push desativado => ", res);
        this.dismissedLoading = true;
      }).catch(err => {
        this.loading.dismiss();
        console.error("Falha ao desativar serviço push => ", err);
        if (String(err) == "SERVICE_NOT_AVAILABLE") {
          this.exibirAlerta();
          this.ligado = true;
          this.dismissedLoading = true;
        }
      });
    }
  }

  exibirAlerta() {
    let alert;
    if (this.ligado) {
      alert = this.alertCtrlr.create({
        title: "Desabilitar o serviço de alerta PUSH",
        message: "Não foi possível desabilitar o serviço de alerta via PUSH. Por favor, verifique sua conexão com a internet.",
        buttons: ['OK'],
      });
      alert.present();
    } else {
      alert = this.alertCtrlr.create({
        title: "Habilitar o serviço de alerta PUSH",
        message: "Não foi possível habilitar o serviço de alerta via PUSH. Por favor, verifique sua conexão com a internet.",
        buttons: ['OK'],
      });
      alert.present();
    }
  }

  exibirAlerta2() {
    let alert;
    if (this.ligado) {
      alert = this.alertCtrlr.create({
        title: "Desabilitar o serviço de alerta PUSH",
        message: "Não foi possível desabilitar o serviço de alerta via PUSH. Por favor, certifique-se de que a luminária esteja ligada e conectada a rede wifi.",
        buttons: ['OK'],
      });
      alert.present();
    } else {
      alert = this.alertCtrlr.create({
        title: "Habilitar o serviço de alerta PUSH",
        message: "Não foi possível habilitar o serviço de alerta via PUSH. Por favor, certifique-se de que a luminária esteja ligada e conectada a rede wifi.",
        buttons: ['OK'],
      });
      alert.present();
    }
  }

  salvarAlarme(dado) {
    let alertaEmail = this.alertCtrlr.create({
      title: 'Email',
      subTitle: 'Por favor, preencha corretamente o campo de e-mail.',
      buttons: ['OK']
    });

    let alertaTemperatura = this.alertCtrlr.create({
      title: 'Temperatura',
      subTitle: 'Por favor, informe os valores de temperatura máxima e mínima.',
      buttons: ['OK']
    });

    let alertaSalvar = this.alertCtrlr.create({
      title: 'ERRO!',
      subTitle: 'Erro ao tentar salvar o alarme de temperatura.',
      buttons: ['OK']
    });

    this.loading = this.loader.create({
      content: "Salvando...",
    });

    this.loading.present()
      .then(() => {
        // if (this.tempMax && this.tempMin) {
        // console.log("Passou na validação 1.o nível");
        // if (this.email != null || !this.emailLigado) {
        //   if (this.email.match("[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}") || !this.emailLigado) {
        let temperaturasJSON = { tempMin: this.tempMin, tempMax: this.tempMax }
        this.alarmeTemperaturaProvider.setTemperaturas(temperaturasJSON)
          .then(respTemp => {
            this.gravarAlarmesStorage();
            this.tempMaxAtual = this.tempMax;
            this.tempMinAtual = this.tempMin;
            console.log("Temperatuas enviadas => ", respTemp);
            this.toast.create({ message: dado + " alterada!", duration: 2000, position: 'top' }).present();
            // if (this.emailAtual != this.email) {
            //   let emailJSON = { email: this.email, emailLigado: this.email ? true : false };
            //   this.alarmeTemperaturaProvider.setEmail(emailJSON)
            //     .then(respEmail => {
            //       console.log("Email enviado => ", respEmail);
            //       this.emailAtual = this.email;
            //       this.loading.dismiss();
            //     }).catch(erroEmail => {
            //       console.error("Erro ao enviar email => ", erroEmail);
            //       this.loading.dismiss();
            //     });
            // }
            this.loading.dismiss();
          }).catch(erroTemp => {
            alertaSalvar.present();
            console.error("Erro ao enviar temperaturas... Desligando alarmes! => ", erroTemp);
            this.loading.dismiss();
          });
        console.log("Passou na validação 2.o nível");
        //   } else {
        //     alertaEmail.present();
        //     console.error("ERRO na validação 2.o nível!!!");
        //     this.loading.dismiss();
        //   }
        // } else {
        //   alertaEmail.present();
        //   console.error("EMAIL null!");
        //   this.loading.dismiss();
        // }
        // } else {
        //   //alertaTemperatura.present();
        //   console.error("ERRO na validação 1.o nível!!!");
        //   this.loading.dismiss();
        // }
      });
  }

  gravarAlarmesStorage() {
    this.storage.set('tempMax', this.tempMax);
    this.storage.set('tempMin', this.tempMin)
      .then(() => {
        this.toast.create({ message: "Configurações Salvas!", duration: 2000, position: 'top' }).present();
      }).catch((e) => {
        console.error("ERRO! ", e)
      });
  }
}