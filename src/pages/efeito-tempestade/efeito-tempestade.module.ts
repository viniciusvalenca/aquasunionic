import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EfeitoTempestadePage } from './efeito-tempestade';

@NgModule({
  declarations: [
    EfeitoTempestadePage,
  ],
  imports: [
    IonicPageModule.forChild(EfeitoTempestadePage),
  ],
})
export class EfeitoTempestadePageModule {}
