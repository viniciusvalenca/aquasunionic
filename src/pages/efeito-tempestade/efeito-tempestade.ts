import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, App, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UtilProvider } from '../../providers/util/util';
import { EfeitoTempestadeProvider } from '../../providers/efeito-tempestade/efeito-tempestade';
import { IndisponivelPage } from '../indisponivel/indisponivel';

@IonicPage()
@Component({
  selector: 'page-efeito-tempestade',
  templateUrl: 'efeito-tempestade.html',
})
export class EfeitoTempestadePage {

  indisponivelPage = IndisponivelPage;
  tempestade: boolean;
  loading: any;
  mode: any;
  modoTempestade: any;
  listaUniao: Array<any> = [];
  nomesLuminarias: Array<any> = [];
  nome: any;
  link: boolean;
  ligarLuminaria: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,
    public util: UtilProvider, public loader: LoadingController, private toast: ToastController,
    private app: App, private efeitoTempestadeProvider: EfeitoTempestadeProvider, public events: Events) {
    // this.carregarEstadoTemporizador()
    this.carregarNomeLuminaria();
    this.eventoPower();
    this.carregarPower();
    this.eventoLink();
    this.eventoListaUniao();
    this.carregarModoTempestade();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EfeitoTempestadePage');
  }

  eventoLink() {
    this.events.subscribe('link', (link) => {
      this.link = link;
      console.log("this.link em efeito-tempestade.ts = ", link);
    });
  }

  eventoListaUniao() {
    this.events.subscribe('listaUniao', (listaUniao) => {
      this.listaUniao = listaUniao;
      console.log("this.listaUniao em efeito-tempestade.ts = ", listaUniao);
    });
  }

  eventoPower() {
    this.events.subscribe('ligarLuminaria', (ligarLuminaria) => {
      this.ligarLuminaria = ligarLuminaria;
      console.log("this.ligarLuminaria em efeito-temestade.ts = ", ligarLuminaria);      
    });
  }

  carregarPower() {
    this.storage.get("power").then(res => {
      this.ligarLuminaria = res;
      console.log("power efeito-tempestade.ts = ", this.ligarLuminaria)      
    })
  }

  carregarNomeLuminaria() {
    this.storage.get("nomeLuminaria").then(res => {
      this.nome = res;
      console.log("nomeLuminaria = ", this.nome)
      this.carregarNomesLuminarias();
    });
  }

  carregarNomesLuminarias() {
    this.storage.get("nomesLuminarias").then(res => {
      this.nomesLuminarias = res;
      console.log("nomesLuminarias (LuminariasPage) = ", this.nomesLuminarias)

      let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
      console.log("index = ", index)
      this.link = this.nomesLuminarias[index].link;
      console.log("this.link = ", this.link)

      for (let i = 0; i < this.nomesLuminarias.length; i++) {
        if (this.nomesLuminarias[i].link) {
          this.listaUniao.push({ ix: [i] });
        }
      }
    })
  }

  carregarModoTempestade() {
    this.storage.get("storm").then(resp => {
      this.tempestade = resp;
      console.log("storm = ", this.tempestade)
    })
    this.storage.get("modoTempestade").then(resp => {
      this.modoTempestade = resp;
      console.log("modoTempestade = ", this.modoTempestade)
    });
    this.storage.get("modeStorm").then(resp => {
      this.mode = resp;
      if(this.mode == null){
        this.mode = 0;
      }
      console.log("modeStorm = ", this.mode)   
      this.nomeModo(this.mode);   
    });
    this.nomeModo(this.mode);
  }

  ligarDesligarTemp() {
    // this.tempestade = !this.tempestade;
    console.log("Estado Tempestade: ", this.tempestade);

    let estadoTempestade = this.tempestade ? "Iniciando" : "Finalizando";
    let mensagem = this.tempestade ? "A tempestade vai começar!" : "Acabou a tempestade.";
    this.loading = this.loader.create({
      content: estadoTempestade + " tempestade...",
    });

    let dados = {
      storm: this.tempestade ? 1 : 0,
      mode: this.mode,
    };

    this.publicarTempestadeLigada(this.tempestade);

    this.loading.present().then(() => {
      if (this.link) {
        let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
        for (let i = 0; i < this.listaUniao.length; i++) {
          this.efeitoTempestadeProvider.setStormMulti(dados, this.listaUniao[i].ix)
            .then((resp) => {
              if (index == this.listaUniao[i].ix) {
                this.toast.create({ message: mensagem, duration: 2000, position: 'top' }).present();
                this.loading.dismiss();
                this.storage.set("tempestade", this.tempestade).then(resp => {
                  console.log("estado tempestade gravado (Storage): ", resp)
                });
              }
              console.log("POST ligarDesligarTemp() resposta: ", resp);
            })
            .catch((e) => {
              if (index == this.listaUniao[i].ix) {
                this.loading.dismiss();
                this.tempestade = false;//pasará por IonDIDLeave(){}
                this.navCtrl.setRoot(this.indisponivelPage);
                this.tempestade = !this.tempestade;
              }
              console.error("ERRO!", e);
            });
        }
      } else {
        this.efeitoTempestadeProvider.setStorm(dados)
          .then((resp) => {
            console.log("POST ligarDesligarTemp() resposta: ", resp);
            this.toast.create({ message: mensagem, duration: 2000, position: 'top' }).present();
            this.loading.dismiss();
            this.storage.set("tempestade", this.tempestade).then(resp => {
              console.log("estado tempestade gravado (Storage): ", resp)
            });
          })
          .catch((e) => {
            this.loading.dismiss();
            console.error("ERRO!", e);
            this.tempestade = false;//pasará por IonDIDLeave(){}
            this.navCtrl.setRoot(this.indisponivelPage);
            this.tempestade = !this.tempestade;
          });
      }
    });
  }

  nomeModo(modo) {
    switch (modo) {
      case 0:
        this.modoTempestade = "Fraca";
        break;

      case 1:
        this.modoTempestade = "Normal";
        break;

      case 2:
        this.modoTempestade = "Intenso";
        break;
    }
    this.storage.set('modoTempestade', this.modoTempestade)
      .then((res) => {
        console.log("modoTempestade gravado (Storage): ", res)
      });
    this.storage.set("modeStorm", modo).then(res => {
      console.log("modo tempestade gravado (Storage) = ", res);
    })
  }
  
  publicarTempestadeLigada(tempestadeLigada) {
    this.events.publish('tempestadeLigada', tempestadeLigada); 
    console.log("Tentando mudar o nome da variavel power em home.ts e luminaria.ts...");
  }

}