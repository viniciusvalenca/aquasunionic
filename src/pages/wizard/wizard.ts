import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, Content, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { WizardProvider } from '../../providers/wizard/wizard';
import { ProcurarLuminariasPage } from '../procurar-luminarias/procurar-luminarias';


@Component({
  selector: 'page-wizard',
  templateUrl: 'wizard.html',
})
export class WizardPage {


  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;

  wifiList: any = {};
  wifiSSID: string = "";
  wifiPASS: string = "";
  hostname: string = "";
  stealth: boolean = false;
  public type = 'password';
  public showPass = false;
  public refresherEnabled = true;
  currentIndex: number;
  objectIndex: number;

  constructor(public navCtrl: NavController, private http: Http, private wizardProvider: WizardProvider,
    public loadingCtrl: LoadingController, public navParams: NavParams,
  ) {

    this.loadWifi();

  }

  loadWifi() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    loading.present().then(() => {
      this.wizardProvider.getWifi()
        .then(res => {
          console.log("getWifi(): ", res);
          this.wifiList = res;
          loading.dismiss();
        }).catch(err => {
          console.error("getWifi(): ", err);
        });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicePage');
    this.slides.lockSwipes(true);
  }

  goToSlide(slideNo) {
    this.slides.lockSwipes(false);
    this.content.scrollToTop();
    this.slides.slideTo(slideNo);
    this.slides.lockSwipes(true);
  }

  wifiClick(wifi) {
    console.log('Clicked: ' + wifi);
    if (wifi != undefined) {
      this.wifiSSID = wifi;
    } else
      this.wifiSSID = ""
    this.refresherEnabled = false;
    this.goToSlide(1);

  }

  wifiStealthClick() {
    console.log('Clicked: Stealth');
    this.stealth = true;
    this.goToSlide(1);

  }

  saveWifi() {

    console.log('Save:');

    this.wizardProvider.setWifi(this.wifiSSID, this.wifiPASS, this.stealth, this.hostname)
      .then(res => {
        console.log("setWifi(): ", res);
        this.goToSlide(2);
      }).catch(err => {
        console.error("setWifi(): ", err);
      });
  }

  disableSave() {
    if (this.wifiPASS == "" || this.wifiPASS == "" || this.hostname == "") return true;
    return false;
  }

  showBack() {
    if (this.slides.getActiveIndex() > 0) return true;
    return false;
  }

  doRefresh(refresher) {
    this.wizardProvider.refresh(refresher).then(res => {
      console.log("doRefresh(): ", res);
      this.wifiList = res;
      refresher.complete();
    });
  }

  showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  confirmWifi() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
      this.wizardProvider.confirm().then(res => {
        console.log("confirm(): ", res);
        if (res['_body'] === 'false') {
          //alert("Problema na configuração");
          this.goToSlide(0);
          this.loadWifi();
        } else {
          //alert("true");
          this.navCtrl.setRoot(ProcurarLuminariasPage);
        }
      },
        err => {
        });

    }, 10000);


  }

  slidesBack() {
    console.log(this.slides.getActiveIndex())
    this.currentIndex = this.slides.getActiveIndex();
    if (this.currentIndex - 1 == 0) {
      this.refresherEnabled = true;
    }
    this.goToSlide(this.currentIndex - 1);
  }



}

