import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { ResetValoresProvider } from '../../providers/reset-valores/reset-valores';

@IonicPage()
@Component({
  selector: 'page-reset-fabrica',
  templateUrl: 'reset-fabrica.html',
})
export class ResetFabricaPage {

 constructor(private resetValoresProvider: ResetValoresProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetFabricaPage');
  }

  // initializeBackButtonCustomHandler(): void {
  //   this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function (event) {
  //     console.log('Prevent Back Button Page Change');
  //   }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  // }
  ////////////////////////UNIFICAR CODIGO: RECUPERARENDPAGE + RESETFABRICAPAGE + ACESSONEGADOPAGE
  confirmacaoReset() {
    this.resetValoresProvider.confirmacaoReset();
  } 

}