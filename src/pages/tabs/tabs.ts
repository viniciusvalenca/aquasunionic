import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { LuminariaPage } from '../luminaria/luminaria';
import { ConfiguracoesPage } from '../configuracoes/configuracoes';
import { EfeitosPage } from '../efeitos/efeitos';
import { FuncoesPage } from '../funcoes/funcoes';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = LuminariaPage;
  tab3Root = FuncoesPage;
  tab4Root = ConfiguracoesPage;

  twinstar: boolean;
  uv: boolean;
  feeder: boolean;
  sensorPh: boolean;
  sensorNivel: boolean;
  canaisLuminaria: any;
  dummy: boolean;
  itensMenu: any = 0;

  constructor(public storage: Storage) {}

ionViewWillEnter(){
  this.carregarOpcoesMenu();
  console.log("itensMenu = ", this.itensMenu)
}

  carregarOpcoesMenu() {
    /////////////////////LUMINARIA////////////////////////////////
    this.storage.get('canaisLuminaria')
      .then((resp) => {
        this.canaisLuminaria = resp;
        console.log("tabs.ts LUMINARIA resgatado (Storage) => ", resp);
        if (this.canaisLuminaria > 0) {         
          this.itensMenu++;
        } 
        console.log("canaisLuminarias itensMenu = ", this.itensMenu)
      })
    /////////////////////TWINSTAR////////////////////////////////
    this.storage.get('twinstar')
      .then((resp) => {
        this.twinstar = resp;
        console.log("tabs.ts TWINSTAR resgatado (Storage) => ", resp);
        if(this.twinstar){
          this.itensMenu++;
        }
        console.log("twinstar itensMenu = ", this.itensMenu)
      })
    /////////////////////CAMARAUV////////////////////////////////
    this.storage.get('uv')
      .then((resp) => {
        this.uv = resp;
        console.log("tabs.ts CAMARAUV resgatado (Storage) => ", resp);   
        if(this.twinstar){
          this.itensMenu++;
        }     
        console.log("uv itensMenu = ", this.itensMenu)
      })
    /////////////////////FEEDER////////////////////////////////
    this.storage.get('feeder')
      .then((resp) => {
        this.feeder = resp;
        console.log("tabs.ts FEEDER resgatado (Storage) => ", resp);
        if(this.feeder){
          this.itensMenu++;
        }
        console.log("feeder itensMenu = ", this.itensMenu)
      })
    /////////////////////SENSORPH////////////////////////////////
    this.storage.get('sensorPh')
      .then((resp) => {
        this.sensorPh = resp;
        console.log("tabs.ts SENSORPH resgatado (Storage) => ", resp);
        if(this.sensorPh){
          this.itensMenu++;
        }
        console.log("sensorPh itensMenu = ", this.itensMenu)
      })
    /////////////////////SENSORNIVEL////////////////////////////////
    this.storage.get('sensorNivel')
      .then((resp) => {
        this.sensorNivel = resp;
        console.log("tabs.ts SENSORNIVEL resgatado (Storage) => ", resp);
        if(this.sensorNivel){
          this.itensMenu++;
        }
        this.dummy = true;
        console.log("sensorNivel itensMenu = ", this.itensMenu)
      })            
  }
}
