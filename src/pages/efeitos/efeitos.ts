import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { EfeitoTempestadePage } from '../efeito-tempestade/efeito-tempestade';

@IonicPage()
@Component({
  selector: 'page-efeitos',
  templateUrl: 'efeitos.html',
})
export class EfeitosPage {

  constructor(public navCtrlr: NavController, public navParams: NavParams, private app: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EfeitosPage');
  }

  backButtonAction() {
    console.log('backButtonAction EfeitosPage');
    let nav = this.app.getActiveNavs()[0];
      nav.parent.select(0)
  }

  abrirPaginaTemp(){
    this.navCtrlr.push(EfeitoTempestadePage);
  }

}
