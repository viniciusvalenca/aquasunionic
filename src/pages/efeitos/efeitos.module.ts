import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EfeitosPage } from './efeitos';

@NgModule({
  declarations: [
    EfeitosPage,
  ],
  imports: [
    IonicPageModule.forChild(EfeitosPage),
  ],
})
export class EfeitosPageModule {}
