import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-info-luminaria',
  templateUrl: 'info-luminaria.html',
})
export class InfoLuminariaPage {

  nome: any;

  loading: any;

  // nomesLuminarias: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage, public toast: ToastController, public events: Events,
    public loader: LoadingController) {
    this.carregarNomeLuminaria();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoLuminariaPage');
  }

  carregarNomeLuminaria() {
    this.storage.get("nomeLuminaria").then(res => {
      this.nome = res;
    });
  }

  gravar() {
    this.loading = this.loader.create({
      content: "Alterando nome...",
    });
    this.loading.present()
      .then(() => {
        this.storage.get("nomeLuminaria").then(res00 => {
          let nomeL = res00;
          this.storage.get("nomesLuminarias").then(res01 => {
            let nomesLuminarias: Array<any> = [];
            nomesLuminarias = res01;
            console.log("infoLuminariaPage :: gravar() => nomesLuminarias = ", nomesLuminarias);
            console.log("infoLuminariaPage :: gravar() => nomeL = ", nomeL);
            let index = nomesLuminarias.findIndex(i => i.alias === nomeL);
            console.log("infoLuminariaPage :: gravar() => index = ", index);
            this.storage.set("nomeLuminaria", this.nome).then(resp => {
              nomesLuminarias[index].alias = this.nome;
              console.log("Nome da luminária guardado (Storage)", this.nome);
              // this.selecionarLuminaria();          
              this.toast.create({ message: "Nome da luminária alterado.", duration: 2000, position: 'top' }).present();
              this.storage.set("nomesLuminarias", nomesLuminarias).then(res02 => {
                console.log("infoLuminariaPage :: gravar() => nomesLuminarias = ", res02)
                this.publicarNomesLuminariasMenu(nomesLuminarias);
              })
            });
          })
        });
      });
  }

  ///////////////////UNIFICAR////////////////////////////////////////////////
  publicarNomesLuminariasMenu(nomesLuminarias) {
    this.events.publish('nomes', nomesLuminarias);
    console.log("Tentando mudar o nome da variavel em app.components.ts...");
    this.loading.dismiss();
  }

}
