import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfoLuminariaPage } from './info-luminaria';

@NgModule({
  declarations: [
    InfoLuminariaPage,
  ],
  imports: [
    IonicPageModule.forChild(InfoLuminariaPage),
  ],
})
export class InfoLuminariaPageModule {}
