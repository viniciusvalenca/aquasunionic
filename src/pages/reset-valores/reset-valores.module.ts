import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResetValoresPage } from './reset-valores';

@NgModule({
  declarations: [
    ResetValoresPage,
  ],
  imports: [
    IonicPageModule.forChild(ResetValoresPage),
  ],
})
export class ResetValoresPageModule {}
