import { Component, Inject, forwardRef } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { ResetValoresProvider } from '../../providers/reset-valores/reset-valores';

@IonicPage()
@Component({
  selector: 'page-reset-valores',
  templateUrl: 'reset-valores.html',
})
export class ResetValoresPage {

resetValoresProvider: any;

  constructor(@Inject(forwardRef(() => ResetValoresProvider)) resetValoresProvider) {
    this.resetValoresProvider = resetValoresProvider;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetValoresPage');
  }

}
