import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Slides } from 'ionic-angular';
import { PresetsPage } from '../presets/presets';
import { LuminariaProvider } from '../../providers/luminaria/luminaria';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { IndisponivelPage } from '../indisponivel/indisponivel';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { PresetsProvider } from '../../providers/presets/presets';
import { Events } from 'ionic-angular/util/events';
import { ViewController } from 'ionic-angular/navigation/view-controller';

@IonicPage()
@Component({
  selector: 'page-luminaria',
  templateUrl: 'luminaria.html',
})
export class LuminariaPage {

  @ViewChild("graph") canvas: ElementRef;

  @ViewChild(Slides) slides: Slides;

  canaisLuminaria: number;
  graficos: Array<any> = [];
  parametrosGrafico: any = {};
  corTexto: any = 'black';
  radius: any = 0;
  radiusManual: any = 0;
  backgroundColor: any = 'darkgrey';
  backgroundPadding: any = "-8";
  backgroundStroke: any = 'transparent';
  backgroundStrokeWidth: any = "0";
  backgroundOpacity: any = "0.5";
  showBackground: boolean = true;
  showBackgroundManual: boolean = true;
  space: any = "3";
  showInnerStroke: boolean = false;
  showInnerStrokeManual: boolean = true;
  innerStrokeWidth: any = "1";
  innerStrokeWidthManual: any = "2";
  innerStrokeColor: any = 'white';
  animation: boolean = false;
  animationManual: boolean = false;
  animationDuration: any = "50";
  showTitle: boolean = true;
  titleFontSize: any = 0;
  titleFontSizeManual: any = 0;
  titleColor: any = this.corTexto;
  subtitleColor: any = 'rgb(39, 37, 38)';
  subtitleFontSize: any = "10";
  subtitleFontSizeManual: any = "10";
  showSubtitle: boolean = false;
  showSubtitleManual: boolean = true;
  responsive: boolean = false;
  outerStrokeLinecap: any = "butt";
  outerStrokeWidth: any = "6";
  outerStrokeColor: any = "'black'";
  unitsColor: any = this.corTexto;
  proxHorario: any;
  corCanais: any = [
    'whitesmoke',
    'blue',
    'red',
    'green',
    'violet',
    'lightblue',
    'yellow',
    'pink',
    'orange',
  ]
  subtitlesCanais: any = [
    "Canal A",
    "Canal B",
    "Canal C",
    "Canal D",
    "Canal E",
    "Canal F",
    "Canal G",
    "Canal H",
    "Canal I",
  ];

  // public doughnutChartDataA: number[] = [0];
  // public doughnutChartLabelsA: string[] = ['Canal A'];
  // public doughnutChartTypeA: string = 'doughnut';
  // // public doughnutChartOptionsA: any = { responsive: true, maintainAspectRatio: false, }
  // public doughnutChartColorsA: Array<any> = [
  //   { // all colors in order
  //     backgroundColor: [this.corCanalA]
  //   }
  // ];
  // public doughnutChartOptions: any = {
  //   tooltips: {
  //     enabled: false
  //   }
  // }

  // public doughnutChartDataB: number[] = [0];
  // public doughnutChartLabelsB: string[] = ['Canal B'];
  // public doughnutChartTypeB: string = 'doughnut';
  // // public doughnutChartOptionsB: any = { responsive: true, maintainAspectRatio: false, }
  // public doughnutChartColorsB: Array<any> = [
  //   { // all colors in order
  //     backgroundColor: [this.corCanalB]
  //   }
  // ];

  // public doughnutChartDataC: number[] = [0];
  // public doughnutChartLabelsC: string[] = ['Canal C'];
  // public doughnutChartTypeC: string = 'doughnut';
  // // public doughnutChartOptionsC: any = { responsive: true, maintainAspectRatio: false, }
  // public doughnutChartColorsC: Array<any> = [
  //   { // all colors in order
  //     backgroundColor: [this.corCanalC]
  //   }
  // ];

  pageRoot = PresetsPage; 
  numeroPreset: number;
  ligado: boolean = false;
  ligadoEstadoAnterior: boolean;
  tempLeds: any = {};
  fixLeds: boolean;
  resposta: any;
  loading: any;
  botaoEditarSalvar: boolean;
  segmento: any;
  listaUniao: Array<any> = [];
  nomesLuminarias: Array<any> = [];
  nome: any;
  link: boolean;  

  presets = [
    {
      nome: "preset1",
      hora: "00:00",
      gradual: 0,
      led1Tensao: 0,
      led2Tensao: 0,
      led3Tensao: 0,
      led4Tensao: 0,
      led5Tensao: 0,
      led6Tensao: 0,
      led7Tensao: 0,
      led8Tensao: 0,
      led9Tensao: 0,
    },
    {
      nome: "preset2",
      hora: "01:00",
      gradual: 0,
      led1Tensao: 0,
      led2Tensao: 0,
      led3Tensao: 0,
      led4Tensao: 0,
      led5Tensao: 0,
      led6Tensao: 0,
      led7Tensao: 0,
      led8Tensao: 0,
      led9Tensao: 0,
    },
  ];

  presetsList: any;
  // ledsPresets: Array<any>;
  dadosPresetsPage: any;
  ligarLuminaria: boolean;
  tempestadeLigada: boolean;
  tituloPagina: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public presetsProvider: PresetsProvider,
    private luminariaProvider: LuminariaProvider, private storage: Storage, public alertCtrlr: AlertController,
    private toast: ToastController, public loader: LoadingController, public viewCtrlr: ViewController,
    public util: UtilProvider, private app: App, public events: Events) {

    // this.carregarLedsPresets();
    // this.carregarListaUniao();
    this.eventoPower();
    this.eventoPowerTempestade();
    this.carregarPower();
    this.carregarTempestade();
    this.carregarQtdeCanais();
    this.carregarNomeLuminaria();
    this.eventoLink();
    this.eventoListaUniao();
    this.carregarPresets();

    // this.carregarLedManualInicio();
    this.carregarEstadoTemporizador();
    this.eventoNomeLuminaria();
  }

  eventoLink() {
    this.events.subscribe('link', (link) => {
      this.link = link;
      console.log("this.link em Luminaria.ts = ", link);
    });
  }

  eventoPower() {
    this.events.subscribe('ligarLuminaria', (ligarLuminaria) => {
      this.ligarLuminaria = ligarLuminaria;
      console.log("this.link em Luminaria.ts = ", ligarLuminaria);
      if (!this.ligarLuminaria) {
        this.tituloPagina = "Luminária Desligada"
      } else {
        this.tituloPagina = "Controle " + this.segmento;
      }
    });
  }

  eventoPowerTempestade() {
    this.events.subscribe('tempestadeLigada', (tempestadeLigada) => {
      this.tempestadeLigada = tempestadeLigada;
      console.log("this.tempestadeLigada (storm) em Luminaria.ts = ", tempestadeLigada);
    });
  }

  publicarTimer() {
    this.events.publish('timer', this.ligado);
    console.log("Tentando a variavel timer em home.ts...");
    // this.loading.dismiss();
  }

  eventoListaUniao() {
    this.events.subscribe('listaUniao', (listaUniao) => {
      this.listaUniao = listaUniao;
      console.log("this.listaUniao em Luminaria.ts = ", listaUniao);
    });
  }

  ngOnChanges() {
    this.carregarPresets();

    // this.carregarLedManualInicio();

    this.carregarEstadoTemporizador();
  }

  ionViewWillEnter() {
    // setTimeout(()=>{
    //   this.presets = this.navParams.get('param1');
    // }, 500);    
  }

  ionViewWillLeave() {
    if (this.fixLeds == true) {
      this.botaoEditarSalvar = false;
      this.presets[this.presets.length - 1].led1Tensao = this.tempLeds.led1Tensao
      this.presets[this.presets.length - 1].led2Tensao = this.tempLeds.led2Tensao
      this.presets[this.presets.length - 1].led3Tensao = this.tempLeds.led3Tensao
      this.presets[this.presets.length - 1].led4Tensao = this.tempLeds.led4Tensao
      this.presets[this.presets.length - 1].led5Tensao = this.tempLeds.led5Tensao
      this.presets[this.presets.length - 1].led6Tensao = this.tempLeds.led6Tensao
      this.presets[this.presets.length - 1].led7Tensao = this.tempLeds.led7Tensao
      this.presets[this.presets.length - 1].led8Tensao = this.tempLeds.led8Tensao
      this.presets[this.presets.length - 1].led9Tensao = this.tempLeds.led9Tensao

      this.regularLeds();
      this.modoLuminaria(0);
      this.fixLeds = false;
    }
  }

  ionViewCanEnter() { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LuminariaPage');
  }

  backButtonAction() {
    console.log('backButtonAction LuminariaPage');
    let nav = this.app.getActiveNavs()[0];
    if (this.botaoEditarSalvar === true)
      this.estadoEditarSalvar();
    else
      nav.parent.select(0)
  }

  eventoNomeLuminaria() {
    this.events.subscribe('valorLuminaria', (zerar) => {
      this.presets[this.presets.length - 1].led1Tensao = zerar;
      console.log("Zerando views em luminaria.ts = ", this.presets[this.presets.length - 1].led1Tensao);
    });
  }

  alertaTemporizador() {
    let alert = this.alertCtrlr.create({
      title: "Falha de comunicação",
      message: "Não foi possível se comunicar com a luminária. Por favor, verifique se ela encontra-se ligada e conectada a rede WiFi.",
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Alerta Falha Termômetro');
          }
        },
      ]
    });
    alert.present();
  }

  setTempLeds() {
    this.tempLeds = {
      led1Tensao: this.presets[this.presets.length - 1].led1Tensao,
      led2Tensao: this.presets[this.presets.length - 1].led2Tensao,
      led3Tensao: this.presets[this.presets.length - 1].led3Tensao,
      led4Tensao: this.presets[this.presets.length - 1].led4Tensao,
      led5Tensao: this.presets[this.presets.length - 1].led5Tensao,
      led6Tensao: this.presets[this.presets.length - 1].led6Tensao,
      led7Tensao: this.presets[this.presets.length - 1].led7Tensao,
      led8Tensao: this.presets[this.presets.length - 1].led8Tensao,
      led9Tensao: this.presets[this.presets.length - 1].led9Tensao,
    }
  }

  carregarPower() {
    this.storage.get("power").then(res => {
      this.ligarLuminaria = res;
      console.log("power luminaria.ts = ", this.ligarLuminaria)
      if (!this.ligarLuminaria) {
        this.tituloPagina = "Luminária Desativada"
      }
    })
  }

  carregarTempestade() {
    this.storage.get("storm").then(res => {
      this.tempestadeLigada = res;
      console.log("storm em Luminaria.ts = ", this.tempestadeLigada)
    })
  }

  carregarQtdeCanais() {
    this.storage.get("canaisLuminaria").then(res => {
      console.log("Qtde de canais recuperados (Storage): ", res);
      this.canaisLuminaria = Number(res);
      this.configuraGrafico(this.canaisLuminaria);
    })
  }

  carregarNomeLuminaria() {
    this.storage.get("nomeLuminaria").then(res => {
      this.nome = res;
      console.log("nomeLuminaria = ", this.nome)
      this.carregarNomesLuminarias();
    });
  }

  carregarNomesLuminarias() {
    this.storage.get("nomesLuminarias").then(res => {
      this.nomesLuminarias = res;
      console.log("nomesLuminarias (LuminariasPage) = ", this.nomesLuminarias)
      let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
      console.log("index = ", index)
      this.link = this.nomesLuminarias[index].link;
      console.log("this.link = ", this.link)
      for (let i = 0; i < this.nomesLuminarias.length; i++) {
        if (this.nomesLuminarias[i].link) {
          this.listaUniao.push({ ix: [i] });
        }
      }
    })
  }
  ///////////////////////////////////////////////////////////

  carregarPresets() {
    this.loading = this.loader.create({
      content: "Carregando...",
    });
    if (this.segmento != "Temporizado") {
      this.loading.present();
    }
    this.storage.get("presets").then(res => {
      this.presets = res;
      // this.presetsList.splice(this.presetsList.lenght-1, 1);  
      // this.presetsList.pop();      
      this.presetsList = this.presets.filter(i => i.nome !== "presetManual");
      console.log("presets recuperados (storage): ", this.presets)
      console.log("presetsList = ", this.presetsList)
      // this.preencherLedTensao();
      this.alterarGrafico();
      this.setTempLeds();
      if (this.segmento != "Temporizado") {
        this.loading.dismiss();
      }
    })
  }

  // preencherLedTensao() {
  //   for (let i = 0; i < this.presetsList.length; i++) {
  //     for (let j = 0; j < this.canaisLuminaria; j++) {
  //       this.ledTensao[i].push(this.presetsList[i]["led" + (j + 1) + "Tensao"]);
  //     }
  //   }
  // }

  carregarEstadoTemporizador() {
    this.storage.get('temporizadorLigado').then((respostaLigado) => {
      this.ligado = (respostaLigado == 0 ? false : true);
      this.ligadoEstadoAnterior = this.ligado;
      if (this.ligado) {
        this.segmento = "Temporizado";
      } else {
        this.segmento = "Manual";
      }
      this.tituloPagina = "Controle " + this.segmento;
      console.log("Temporizador ligado (storage): ", respostaLigado);
      console.log("Temporizador ligado (storage): ", this.ligado);
      console.log("this.segmento (Temporizado ou Manual) (storage): ", this.segmento);
    })
  }

  configuraGrafico(numeroCanais) {
    for (let i = 0; i < this.canaisLuminaria; i++) {
      this.parametrosGrafico = {
        numeroCanal: 0,
        nomeCanal: "led" + (i + 1) + "Tensao",
        corTexto: this.corTexto,
        radius: 11,
        radiusManual: String(numeroCanais == 1 ? 45 : numeroCanais == 2 ? 40 : numeroCanais == 3 ? 33 : numeroCanais == 4 ? 27 : numeroCanais == 5 ? 21 : 16),
        backgroundColor: this.backgroundColor,
        backgroundPadding: this.backgroundPadding,
        backgroundStroke: this.backgroundStroke,
        backgroundStrokeWidth: this.backgroundStrokeWidth,
        backgroundOpacity: this.backgroundOpacity,
        showBackground: this.showBackground,
        showBackgroundManual: this.showBackgroundManual,
        space: this.space,
        showInnerStroke: this.showInnerStroke,
        innerStrokeWidth: this.innerStrokeWidth,
        innerStrokeColor: this.innerStrokeColor,
        animation: this.animation,
        animationManual: this.animationManual,
        animationDuration: this.animationDuration,
        showTitle: this.showTitle,
        titleFontSize: String(numeroCanais == 1 ? 14 : numeroCanais == 2 ? 13 : numeroCanais == 3 ? 12 : numeroCanais == 4 ? 10 : numeroCanais == 5 ? 8 : 6),
        titleFontSizeManual: String(numeroCanais == 1 ? 14 : numeroCanais == 2 ? 13 : numeroCanais == 3 ? 12 : numeroCanais == 4 ? 10 : numeroCanais == 5 ? 8 : 6),
        titleColor: this.titleColor,
        subtitle: "Canal " + (i + 1),
        subtitleColor: this.subtitleColor,
        subtitleFontSize: this.subtitleFontSize,
        showSubtitle: this.showSubtitle,
        showSubtitleManual: this.showSubtitleManual,
        responsive: this.responsive,
        outerStrokeLinecap: this.outerStrokeLinecap,
        outerStrokeWidth: this.outerStrokeWidth,
        unitsColor: this.unitsColor,
        corCanal: this.corCanais[i],
        subtitlesCanal: this.subtitlesCanais[i],
        percentCanal: 0,
        valorCanal: 0,
        percentCanalManual: 0,
        valorCanalManual: 0,
      };
      this.graficos.push(this.parametrosGrafico);
    }
  }

  alterarGrafico() {
    for (let i = 0; i < this.canaisLuminaria; i++) {
      this.graficos[i].percentCanalManual = (this.presets[this.presets.length - 1]["led" + (i + 1) + "Tensao"] / 1020) * 100;
      this.graficos[i].valorCanalManual = this.presets[this.presets.length - 1]["led" + (i + 1) + "Tensao"];
      console.log("Grafico Canal (%)" + (i + 1) + ": " + this.graficos[i].percentCanalManual + "%")
      // console.log("Grafico Canal (Tensão)" + (i + 1) + ": " + this.graficos[i].valorCanalManual)
      this.graficos[i].numeroCanal = (i + 1);
    }
  }

  regularLeds() {
    if (!this.fixLeds) {
      this.fixLeds = true;
    }
    this.alterarGrafico();
    if (this.link) {
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.resposta = this.luminariaProvider.getRegulagemMulti(this.presets, this.listaUniao[i].ix)
          .subscribe(
          data => {
            this.resposta = data;
            console.log("Regular LEDs. Resposta Multi (LuminariaPage): ", data);
          },
          err => {
            console.log(err);
          },
          () => {
            console.log('GET Request completo')
          }
          );
        console.log(this.resposta, "Led 1 = " + this.presets[this.presets.length - 1].led1Tensao, "Led 2 = " + this.presets[this.presets.length - 1].led2Tensao, "Led 3 = " + this.presets[this.presets.length - 1].led3Tensao);
      }
    } else {
      this.resposta = this.luminariaProvider.getRegulagem(this.presets)
        .subscribe(
        data => {
          this.resposta = data;
          console.log("Regular LEDs. Resposta (LuminariaPage): ", data);
        },
        err => {
          console.log(err);
        },
        () => {
          console.log('GET Request completo')
        }
        );
      console.log(this.resposta, "Led 1 = " + this.presets[this.presets.length - 1].led1Tensao, "Led 2 = " + this.presets[this.presets.length - 1].led2Tensao, "Led 3 = " + this.presets[this.presets.length - 1].led3Tensao);
    }
  }

  regularLedsJSON() {
    let dados = {
      nome: "presetManual",
      led1Tensao: this.presets[this.presets.length - 1].led1Tensao,
      led2Tensao: this.presets[this.presets.length - 1].led2Tensao,
      led3Tensao: this.presets[this.presets.length - 1].led3Tensao,
      led4Tensao: this.presets[this.presets.length - 1].led4Tensao,
      led5Tensao: this.presets[this.presets.length - 1].led5Tensao,
      led6Tensao: this.presets[this.presets.length - 1].led6Tensao,
      led7Tensao: this.presets[this.presets.length - 1].led7Tensao,
      led8Tensao: this.presets[this.presets.length - 1].led8Tensao,
      led9Tensao: this.presets[this.presets.length - 1].led9Tensao,
    };
    if (this.link) {
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.resposta = this.luminariaProvider.setRegulagemJSONMulti(dados, this.listaUniao[i].ix)
          .then((resp) => {
            console.log("POST resposta Multi: ", resp);
          })
          .catch((e) => {
            console.error("ERRO!", e);
          });
        this.estadoEditarSalvar();
        console.log(this.resposta, "Led 1 = " + this.presets[this.presets.length - 1].led1Tensao, "Led 2 = " + this.presets[this.presets.length - 1].led2Tensao, "Led 3 = " + this.presets[this.presets.length - 1].led3Tensao);
      }
    } else {
      this.resposta = this.luminariaProvider.setRegulagemJSON(dados)
        .then((resp) => {
          console.log("POST resposta: ", resp);
        })
        .catch((e) => {
          console.error("ERRO!", e);
        });
      this.estadoEditarSalvar();
      console.log(this.resposta, "Led 1 = " + this.presets[this.presets.length - 1].led1Tensao, "Led 2 = " + this.presets[this.presets.length - 1].led2Tensao, "Led 3 = " + this.presets[this.presets.length - 1].led3Tensao);
    }
  }

  abrirPaginaPreset(index) {
    console.log("Index = ", index + 1);
    this.numeroPreset = index + 1;
    let presets = this.presets;
    this.navCtrl.push(this.pageRoot, { param1: this.numeroPreset, param2: this.presetsList, callback: this.callback, param3: this.listaUniao, param4: this.link });
  }

  callback = data => {
    this.dadosPresetsPage = data;
    console.log('Dados recebidos de volta da página presetsPage: ', this.dadosPresetsPage);
    this.presetsList = this.dadosPresetsPage;
    for (let i = 0; i < this.presetsList.lenght; i++) {
      this.presets[i] = this.presetsList[i];
    }
    this.storage.set("presets", this.presets).then(res => {
      console.log("presets salvos (storage): ", res);
    })
  };

  salvarRegulagemLedsJSON() {
    this.loading = this.loader.create({
      content: "Salvando...",
    });
    let dados = {
      nome: "presetManual",
      led1Tensao: this.presets[this.presets.length - 1].led1Tensao,
      led2Tensao: this.presets[this.presets.length - 1].led2Tensao,
      led3Tensao: this.presets[this.presets.length - 1].led3Tensao,
      led4Tensao: this.presets[this.presets.length - 1].led4Tensao,
      led5Tensao: this.presets[this.presets.length - 1].led5Tensao,
      led6Tensao: this.presets[this.presets.length - 1].led6Tensao,
      led7Tensao: this.presets[this.presets.length - 1].led7Tensao,
      led8Tensao: this.presets[this.presets.length - 1].led8Tensao,
      led9Tensao: this.presets[this.presets.length - 1].led9Tensao,
    };

    this.loading.present().then(() => {
      if (this.link) {
        let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
        for (let i = 0; i < this.listaUniao.length; i++) {
          this.resposta = this.luminariaProvider.setRegulagemJSONMulti(dados, this.listaUniao[i].ix)
            .then((resp) => {
              console.log("POST resposta: ", resp);
              if (index == this.listaUniao[i].ix) {
                this.storage.set('presets', this.presets);
                this.toast.create({ message: "Configuração Manual Salva.", duration: 2000, position: 'top' }).present();
                this.loading.dismiss();
                this.fixLeds = false;
                this.setTempLeds();
                this.estadoEditarSalvar();//verificar 
              }
            })
            .catch((e) => {
              if (index == this.listaUniao[i].ix) {
                this.estadoEditarSalvar();//verificar
                this.loading.dismiss();
                // this.navCtrl.setRoot(this.indisponivelPage);
              }
              this.alertaTemporizador();
              console.error("ERRO!", e);
            });
        }
      } else {
        this.resposta = this.luminariaProvider.setRegulagemJSON(dados)
          .then((resp) => {
            console.log("POST resposta: ", resp);
            this.storage.set('presets', this.presets);
            this.toast.create({ message: "Configuração Manual Salva.", duration: 2000, position: 'top' }).present();
            this.loading.dismiss();
            this.fixLeds = false;
            this.setTempLeds();
            this.estadoEditarSalvar();//verificar 
          })
          .catch((e) => {
            this.estadoEditarSalvar();//verificar
            this.loading.dismiss();
            console.error("ERRO!", e);
            this.alertaTemporizador();
            // this.navCtrl.setRoot(this.indisponivelPage);
          });
      }
    });
    console.log(this.resposta, "Led 1 = " + this.presets[this.presets.length - 1].led1Tensao, "Led 2 = " + this.presets[this.presets.length - 1].led2Tensao, "Led 3 = " + this.presets[this.presets.length - 1].led3Tensao);
  }

  ligarDesligarTemporizador() {
    console.log("segmento: ", this.segmento)
    this.tituloPagina = "Controle " + this.segmento;
    if (this.botaoEditarSalvar) {
      this.estadoEditarSalvar();
    }
    // this.ligado = !this.ligado;
    this.segmento === "Temporizado" ? this.ligado = true : this.ligado = false;//Verificar se bug sumiu
    this.publicarTimer();
    if (this.ligado != this.ligadoEstadoAnterior) {
      this.ligadoEstadoAnterior = this.ligado;
      let conteudo;
      if (this.ligado) {
        conteudo = "Ativando Temporizador...";
      } else {
        conteudo = "Ativando Controle Manual...";
      }
      this.loading = this.loader.create({
        content: conteudo,
      });

      let timer = { timer: this.ligado ? 1 : 0 };
      console.log("timer: ", timer);
      this.loading.present().then(() => {
        if (this.link) {
          let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
          for (let i = 0; i < this.listaUniao.length; i++) {
            this.luminariaProvider.setTimerJSONMulti(timer, this.listaUniao[i].ix)
              .then((resp) => {
                if (index == this.listaUniao[i].ix) {
                  this.storage.set('temporizadorLigado', this.ligado).then((respostaLigado) => {
                    if (this.ligado) {
                      this.botaoEditarSalvar = false;
                      //Replicar os presets entre as luminárias aqui...
                      // this.presetsProvider.getTemporizador()
                      //   .then((resp) => {
                      //     this.setTemporizadores(resp);
                      //   });
                      // this.getStorageLEdsManual2();                      
                      this.carregarPresets();
                      this.publicarTimer();
                    }
                    console.log('Temporizador: ', respostaLigado);
                  });
                  this.loading.dismiss();
                }
              })
              .catch((e) => {
                if (index == this.listaUniao[i].ix) {
                  this.loading.dismiss();
                  this.ligado = !this.ligado;
                  this.ligadoEstadoAnterior = !this.ligadoEstadoAnterior;
                  this.segmento === "Temporizado" ? this.segmento = "Manual" : this.segmento = "Temporizado";
                  this.tituloPagina = "Controle " + this.segmento;
                  this.alertaTemporizador();
                  this.publicarTimer();
                }
                console.error("ERRO ao ligar Temporizador!", e)
                console.error("this.ligadoEstadoAnterior = ", this.ligadoEstadoAnterior)
                console.error("this.ligado = ", this.ligado)
                console.error("segmento: ", this.segmento)
              });
          }
        } else {
          this.luminariaProvider.setTimerJSON(timer)
            .then((resp) => {
              this.storage.set('temporizadorLigado', this.ligado).then((respostaLigado) => {
                if (this.ligado) {
                  this.botaoEditarSalvar = false;
                  // this.getStorageLEdsManual2();
                  this.carregarPresets();
                }
                console.log('Temporizador: ', respostaLigado);
              });
              this.loading.dismiss();
            })
            .catch((e) => {
              this.loading.dismiss();
              this.ligado = !this.ligado;
              this.ligadoEstadoAnterior = !this.ligadoEstadoAnterior;
              this.segmento === "Temporizado" ? this.segmento = "Manual" : this.segmento = "Temporizado";
              this.tituloPagina = "Controle " + this.segmento;
              console.error("ERRO ao ligar Temporizador!", e)
              console.error("this.ligadoEstadoAnterior = ", this.ligadoEstadoAnterior)
              console.error("this.ligado = ", this.ligado)
              console.error("segmento: ", this.segmento)
              this.alertaTemporizador();
              this.publicarTimer();
            });
        }
      })
    }
  }

  estadoEditarSalvar() {
    this.botaoEditarSalvar = !this.botaoEditarSalvar;
    //let dados = {modo: this.botaoEditarSalvar ? 1 : 0};
    let dados = this.botaoEditarSalvar ? 1 : 0;
    this.modoLuminaria(dados);
    console.log('Estado Botao Editar/Salvar: ', this.botaoEditarSalvar);
  }

  modoLuminaria(dados) {
    if (this.link) {
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.luminariaProvider.setModoMulti(dados, this.listaUniao[i].ix)
          .then((resp) => {
            console.log("GET Request Modo Completo ");
          })
          .catch((e) => {
            console.error("ERRO!", e);
          });
      }
    } else {
      this.luminariaProvider.setModo(dados)
        .then((resp) => {
          console.log("GET Request Modo Completo ");
        })
        .catch((e) => {
          console.error("ERRO!", e);
        });
    }
  }

  salvarNovoPresetJSON() {
    console.log("Preset atual: preset" + this.presets.length);
    let dados = {
      nome: "preset" + this.presets.length,
      led1Tensao: 0,
      led2Tensao: 0,
      led3Tensao: 0,
      led4Tensao: 0,
      led5Tensao: 0,
      led6Tensao: 0,
      led7Tensao: 0,
      led8Tensao: 0,
      led9Tensao: 0,
    };

    this.resposta = this.presetsProvider.setRegulagemJSON(dados)
      .then((resp) => {
        console.log("POST Salvar Novo Preset: ", resp);
        this.salvarNovoTemporizadorJSON();
      })
      .catch((e) => {
        console.error("ERRO!", e);
        this.botaoEditarSalvar = false;//pasará por IonDIDLeave(){}
        this.alertaTemporizador();
      });
    console.log("Variável this.resposta = " + this.resposta, "Led 1 = " + this.presets[this.presets.length - 1].led1Tensao, "Led 2 = " + this.presets[this.presets.length - 1].led2Tensao, "Led 3 = " + this.presets[this.presets.length - 1].led3Tensao);
  }

  salvarNovoTemporizadorJSON() {
    let presetsJSON = [];
    for (let i = 0; i < this.presetsList.length; i++) {
      let hrFim;
      console.log("Iteração [" + i + "] salvarNovoTemporizadorJSON()");
      console.log("presets[" + i + "].hora = ", this.presetsList[i].hora);
      if (i < (this.presetsList.length - 1)) {
        hrFim = this.util.convertHoraMilisegundos(this.presetsList[i + 1].hora);
      } else {
        hrFim = this.util.convertHoraMilisegundos(this.presetsList[0].hora);
      }
      presetsJSON.push(
        {
          hrInicio: this.util.convertHoraMilisegundos(this.presetsList[i].hora),
          hrFinal: hrFim,
          nome: this.presetsList[i].nome,
          gradual: this.presetsList[i].gradual
        }
      );
    }
    this.setTemporizadores(presetsJSON);
  }

  setTemporizadores(presetsJSON) {
    if (this.link) {
      let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.presetsProvider.setTemporizadorJSONMulti(presetsJSON, this.listaUniao[i].ix)
          .then((resp) => {
            console.log("POST resposta  salvarTemporizadorJSON(): ", resp);
            console.log("POST Salvando Novo Temporizador: ", resp);
            if (index == this.listaUniao[i].ix) {
              this.loading.dismiss();
            }
          })
          .catch((e) => {
            if (index == this.listaUniao[i].ix) {
              this.loading.dismiss();
              this.alertaTemporizador();
            }
            console.error("ERRO ao salvar Novo Temporizador!", e);
          });
      }
    } else {
      this.presetsProvider.setTemporizadorJSON(presetsJSON)
        .then((resp) => {
          console.log("POST resposta  salvarTemporizadorJSON(): ", resp);
          console.log("POST Salvando Novo Temporizador: ", resp);
          this.loading.dismiss();
        })
        .catch((e) => {
          this.loading.dismiss();
          console.error("ERRO ao salvar Novo Temporizador!", e);
          this.alertaTemporizador();
        });
    }
  }

  addPreset(index) {
    let hora;
    console.log("Ultima hora ARRAY:", this.presetsList[this.presetsList.length - 1].hora);
    if (this.presetsList != null) {
      hora = this.util.convertHoraMilisegundos(this.presetsList[this.presetsList.length - 1].hora);
      if (hora <= this.util.convertHoraMilisegundos("22:59")) {
        hora += 3600000;
      }
      console.log("Hora: ", hora);
    }
    if (this.presetsList.length < 12) {
      this.presetsList.push(
        {
          nome: "preset" + (this.presetsList.length + 1),
          hora: this.util.convertMilisegundosHora(hora),
          gradual: 0,
          led1Tensao: 0,
          led2Tensao: 0,
          led3Tensao: 0,
          led4Tensao: 0,
          led5Tensao: 0,
          led6Tensao: 0,
          led7Tensao: 0,
          led8Tensao: 0,
          led9Tensao: 0,
        })
      // for (let i = 0; i < this.presetsList.lenght; i++) {
      //   this.presets[i] = this.presetsList[i];
      // }
      // let dados = this.presetsList; //??
      let dados = {
        nome: "preset" + (this.presets.length + 1),
        hora: this.util.convertMilisegundosHora(hora),
        gradual: 0,
        led1Tensao: 0,
        led2Tensao: 0,
        led3Tensao: 0,
        led4Tensao: 0,
        led5Tensao: 0,
        led6Tensao: 0,
        led7Tensao: 0,
        led8Tensao: 0,
        led9Tensao: 0,
      };
      this.presets.splice(this.presets.length - 1, 0, dados);
      this.storage.set("presets", this.presets).then(res => {
        console.log("presets salvos AddPreset() (storage): ", res);
      })
    }
    // Enviar POST http
    // this.salvarNovoPresetJSON();
    this.salvarNovoTemporizadorJSON();
  }

  delPreset() {
    if (this.presetsList.length > 2) {
      this.presetsList.pop();
      console.log("delPreset() presetsList (pop): ", this.presetsList);
      // for (let i = 0; i < this.presetsList.lenght; i++) {
      //   this.presets[i] = this.presetsList[i];
      // }
      this.presets.splice(this.presets.length - 2, 1);
      console.log("delPreset() presets (splice): ", this.presets);
      this.storage.set("presets", this.presets).then(res => {
        console.log("presets salvos delPreset (storage): ", res);
        this.salvarNovoTemporizadorJSON();
      })
    }
  }

  horafinal(preset) {
    let index = this.presets.indexOf(preset);
    let nextItem;
    if (index >= 0 && index < this.presets.length - 1) {
      nextItem = this.presets[index + 1]
    } else {
      nextItem = this.presets[0]
    }
    return nextItem.hora
  }

  public chartClicked(e: any): void {
    console.log("Click no gráfico: ", e);
    this.goToSlide(e);
  }

  // public chartHovered(e: any): void {
  //   console.log(e);
  // }

  goToSlide(slide) {
    this.slides.slideTo(slide, 500);
  }
}