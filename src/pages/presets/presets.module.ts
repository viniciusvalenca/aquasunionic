import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PresetsPage } from './presets';
import { HttpClientModule } from '@angular/common/http';
// import {ChartsModule} from 'ng2-charts'
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  declarations: [
    PresetsPage,
  ],
  imports: [
    IonicPageModule.forChild(PresetsPage),
    HttpClientModule,
    // ChartsModule,
    NgCircleProgressModule.forRoot({
      radius: 12,
      outerStrokeWidth: 5,
      animation: false,
      animationDuration: 50,
      showTitle: true,
      showSubtitle: false,
      responsive: false,
      outerStrokeLinecap: "butt",
      backgroundPadding: 0,
      backgroundStrokeWidth: 5,
      outerStrokeColor: "black",      
    })
  ],
})
export class PresetsPageModule {}
