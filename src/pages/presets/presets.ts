import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { PresetsProvider } from '../../providers/presets/presets';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { IndisponivelPage } from '../indisponivel/indisponivel';
import { Platform } from 'ionic-angular/platform/platform';
import { App } from 'ionic-angular';
import { UtilProvider } from '../../providers/util/util';
import { LuminariaPage } from '../luminaria/luminaria';
import { Slides } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-presets',
  templateUrl: 'presets.html',
})

export class PresetsPage {

  @ViewChild("graph") canvas: ElementRef;

  @ViewChild(Slides) slides: Slides;

  canaisLuminaria: number = 3;
  graficos: Array<any> = [];
  parametrosGrafico: any = {};
  corTexto: any = 'black';
  radius: any = 0;
  radiusManual: any = "30";
  backgroundColor: any = 'darkgrey';
  backgroundPadding: any = "-8";
  backgroundStroke: any = 'transparent';
  backgroundStrokeWidth: any = "0";
  backgroundOpacity: any = "0.5";
  showBackground: boolean = true;
  showBackgroundManual: boolean = true;
  space: any = "3";
  showInnerStroke: boolean = false;
  showInnerStrokeManual: boolean = true;
  innerStrokeWidth: any = "1";
  innerStrokeWidthManual: any = "2";
  innerStrokeColor: any = 'white';
  animation: boolean = false;
  animationManual: boolean = false;
  animationDuration: any = "50";
  showTitle: boolean = true;
  titleFontSize: any = 0;
  titleColor: any = this.corTexto;
  subtitleColor: any = 'rgb(39, 37, 38)';
  subtitleFontSize: any = "10";
  subtitleFontSizeManual: any = "10";
  showSubtitle: boolean = false;
  showSubtitleManual: boolean = true;
  responsive: boolean = false;
  outerStrokeLinecap: any = "butt";
  outerStrokeWidth: any = "6";
  outerStrokeColor: any = "'black'";
  unitsColor: any = this.corTexto;
  proxHorario: any;
  corCanais: any = [
    'whitesmoke',
    'blue',
    'red',
    'green',
    'violet',
    'lightblue',
    'yellow',
    'pink',
    'orange',
  ]
  subtitlesCanais: any = [
    "Canal A",
    "Canal B",
    "Canal C",
    "Canal D",
    "Canal E",
    "Canal F",
    "Canal G",
    "Canal H",
    "Canal I",
  ];


  numeroPreset: number = 1;

  resposta: any;

  botaoEditarSalvar: boolean;

  gradual: boolean = false;

  loading: any;

  alert: any;

  hora: any;

  horaAnterior: any;

  horaAtual: any;

  horaPosterior: any;

  presets = [
    {
      nome: "preset1",
      hora: "00:00",
      gradual: 0,
      led1Tensao: 0,
      led2Tensao: 0,
      led3Tensao: 0,
      led4Tensao: 0,
      led5Tensao: 0,
      led6Tensao: 0,
      led7Tensao: 0,
      led8Tensao: 0,
      led9Tensao: 0,
    },
    {
      nome: "preset2",
      hora: "01:00",
      gradual: 0,
      led1Tensao: 0,
      led2Tensao: 0,
      led3Tensao: 0,
      led4Tensao: 0,
      led5Tensao: 0,
      led6Tensao: 0,
      led7Tensao: 0,
      led8Tensao: 0,
      led9Tensao: 0,
    },
  ];

  nome: any;
  nomesLuminarias: any;
  listaUniao: any;
  link: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform,
    public presetsProvider: PresetsProvider, private storage: Storage, public util: UtilProvider,
    public loader: LoadingController, public alertCtrlr: AlertController,
    private toast: ToastController, private app: App) {

    // this.lerParametros();
    this.carregarQtdeCanais();
    this.carregarPresets();
    this.carregarNomeLuminaria();
    // this.carregarNomesLuminarias();
    this.alertaFalhaComunicacao();
  }

  ionViewWillEnter() { }

  ionViewDidLeave() {
    //this.resetarValores();
    let dados = 0;
    if (this.botaoEditarSalvar) {
      if (this.link) {
        for (let i = 0; i < this.listaUniao.length; i++) {
          this.presetsProvider.setModoMulti(dados, this.numeroPreset, this.listaUniao[i].ix)
            .then((resp) => {
              console.log("GET resposta Modo: ", resp);
            })
            .catch((e) => {
              console.error("ERRO!", e);
            });
        }
      } else {
        this.presetsProvider.setModo(dados, this.numeroPreset)
          .then((resp) => {
            console.log("GET resposta Modo: ", resp);
          })
          .catch((e) => {
            console.error("ERRO!", e);
          });
      }
    }
  }

  ionViewDidLoad() {
    // this.lerParametros();
    // this.carregarPresets();
    console.log('ionViewDidLoad PresetsPage');
  }

  alertaFalhaComunicacao() {
    this.alert = this.alertCtrlr.create({
      title: "Falha de comunicação",
      message: "Não foi possível se comunicar com a luminária. Por favor, verifique se ela encontra-se ligada e conectada a rede WiFi.",
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Alerta Falha Termômetro');
          }
        },
      ]
    });
  }
  ////////////////////////TESTE//////////////////////////////////////
  alertaAvisoHorariosAjustes() {
    let alert = this.alertCtrlr.create({
      title: "Ajuste de horários",
      message: "Uma configuração ilegal dentro dos horários dos ajustes foi detectada. O horário inicial do Ajuste " + this.numeroPreset + ", deve ser maior ou igual ao horário inicial do Ajuste " + (this.numeroPreset - 1) + ".",
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.horaAtual = this.presets[this.numeroPreset - 2].hora;
            console.log('Alerta Aviso Horário Errado');
          }
        },
      ]
    });
    alert.present();
  }
  /////////////////////////////////////////////////////////////////

  backButtonAction() {
    console.log('backButtonAction PresetsPage');
    let nav = this.app.getActiveNavs()[0];
    if (this.botaoEditarSalvar === true) {
      this.estadoEditarSalvar();
    }
    this.navCtrl.popToRoot();
  }

  popCallback() {
    let parametros = this.navParams.get("callback");
    console.log("PopPresetsPage: ", parametros);
    this.navCtrl.pop().then(() => {
      parametros(this.presets)
    });
  }

  carregarQtdeCanais() {
    this.storage.get("canaisLuminaria").then(res => {
      console.log("Qtde de canais recuperados (Storage): ", res);
      this.canaisLuminaria = Number(res);
      this.configuraGrafico(this.canaisLuminaria);
    })
  }

  carregarNomeLuminaria() {
    this.storage.get("nomeLuminaria").then(res => {
      this.nome = res;
      console.log("nomeLuminaria = ", this.nome)
      this.carregarNomesLuminarias();
    });
  }

  carregarNomesLuminarias() {
    this.storage.get("nomesLuminarias").then(res => {
      this.nomesLuminarias = res;
      console.log("nomesLuminarias (LuminariasPage) = ", this.nomesLuminarias)
    });
  }

  lerParametros() {
    this.numeroPreset = this.navParams.get('param1');
    this.presets = this.navParams.get('param2');
    console.log("this.presets = ", this.presets)
    this.alterarGrafico();
    this.listaUniao = this.navParams.get('param3');
    this.link = this.navParams.get("param4");

    if (this.numeroPreset == 1) {
      this.horaAnterior = this.presets[this.numeroPreset].hora;
    } else {
      this.horaAnterior = this.presets[this.numeroPreset - 2].hora;
    }

    this.horaAtual = this.presets[this.numeroPreset - 1].hora;

    if (this.numeroPreset == this.presets.length) {
      // this.horaPosterior = this.presets[0].hora;
      this.horaPosterior = "23:59";
    } else {
      this.horaPosterior = this.presets[this.numeroPreset].hora;
    }

    this.gradual = this.presets[this.numeroPreset - 1].gradual == 1 ? true : false;
  }

  carregarPresets() {
    this.storage.get("presets").then(res => {
      this.presets = res;
      this.lerParametros();
      console.log("presets recuperados (storage): ", this.presets)
      this.estadoEditarSalvar();
    })
  }

  configuraGrafico(numeroCanais) {
    for (let i = 0; i < this.canaisLuminaria; i++) {
      this.parametrosGrafico = {
        numeroCanal: 0,
        nomeCanal: "led" + (i + 1) + "Tensao",
        corTexto: this.corTexto,
        radius: String(numeroCanais == 1 ? 45 : numeroCanais == 2 ? 40 : numeroCanais == 3 ? 33 : numeroCanais == 4 ? 27 : numeroCanais == 5 ? 21 : 16),
        radiusManual: this.radiusManual,
        backgroundColor: this.backgroundColor,
        backgroundPadding: this.backgroundPadding,
        backgroundStroke: this.backgroundStroke,
        backgroundStrokeWidth: this.backgroundStrokeWidth,
        backgroundOpacity: this.backgroundOpacity,
        showBackground: this.showBackground,
        showBackgroundManual: this.showBackgroundManual,
        space: this.space,
        showInnerStroke: this.showInnerStroke,
        innerStrokeWidth: this.innerStrokeWidth,
        innerStrokeColor: this.innerStrokeColor,
        animation: this.animation,
        animationManual: this.animationManual,
        animationDuration: this.animationDuration,
        showTitle: this.showTitle,
        titleFontSize: String(numeroCanais == 1 ? 14 : numeroCanais == 2 ? 13 : numeroCanais == 3 ? 12 : numeroCanais == 4 ? 10 : numeroCanais == 5 ? 8 : 6),
        titleColor: this.titleColor,
        subtitle: "Canal " + (i + 1),
        subtitleColor: this.subtitleColor,
        subtitleFontSize: this.subtitleFontSize,
        showSubtitle: this.showSubtitle,
        showSubtitleManual: this.showSubtitleManual,
        responsive: this.responsive,
        outerStrokeLinecap: this.outerStrokeLinecap,
        outerStrokeWidth: this.outerStrokeWidth,
        unitsColor: this.unitsColor,
        corCanal: this.corCanais[i],
        subtitlesCanal: this.subtitlesCanais[i],
        percentCanal: 0,
        valorCanal: 0,
        percentCanalManual: 0,
        valorCanalManual: 0,
      };
      this.graficos.push(this.parametrosGrafico);
    }
  }

  alterarGrafico() {
    for (let i = 0; i < this.canaisLuminaria; i++) {
      this.graficos[i].percentCanalManual = (this.presets[this.numeroPreset - 1]["led" + (i + 1) + "Tensao"] / 1020) * 100;
      this.graficos[i].valorCanalManual = this.presets[this.numeroPreset - 1]["led" + (i + 1) + "Tensao"];
      console.log("Grafico Canal (%)" + (i + 1) + ": " + this.graficos[i].percentCanalManual + "%")
      // console.log("Grafico Canal (Tensão)" + (i + 1) + ": " + this.graficos[i].valorCanalManual)
      this.graficos[i].numeroCanal = (i + 1);
    }
  }

  regulagemPreset() {
    if (this.link) {
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.resposta = this.presetsProvider.getRegulagemMulti(this.presets, this.numeroPreset, this.listaUniao[i].ix).subscribe(
          data => {
            this.resposta = data;
            console.log("Regular LEDs. Resposta (PresetsPage): ", data);
          },
          err => {
            console.log(err);
          },
          () => {
            console.log('GET Request completo')
          }
        );
        this.alterarGrafico();
        console.log(this.resposta, "Led A = " + this.presets[this.numeroPreset - 1].led1Tensao, "Led B = " + this.presets[this.numeroPreset - 1].led2Tensao, "Led C = " + this.presets[this.numeroPreset - 1].led3Tensao);
      }
    } else {
      this.resposta = this.presetsProvider.getRegulagem(this.presets, this.numeroPreset).subscribe(
        data => {
          this.resposta = data;
          console.log("Regular LEDs. Resposta (PresetsPage): ", data);
        },
        err => {
          console.log(err);
        },
        () => {
          console.log('GET Request completo')
        }
      );
      this.alterarGrafico();
      console.log(this.resposta, "Led A = " + this.presets[this.numeroPreset - 1].led1Tensao, "Led B = " + this.presets[this.numeroPreset - 1].led2Tensao, "Led C = " + this.presets[this.numeroPreset - 1].led3Tensao);
    }
  }

  regulagemPresetJSON() {
    let dados = {
      nome: "preset" + this.numeroPreset,
      led1Tensao: this.presets[this.numeroPreset - 1].led1Tensao,
      led2Tensao: this.presets[this.numeroPreset - 1].led2Tensao,
      led3Tensao: this.presets[this.numeroPreset - 1].led3Tensao,
      led4Tensao: this.presets[this.numeroPreset - 1].led4Tensao,
      led5Tensao: this.presets[this.numeroPreset - 1].led5Tensao,
      led6Tensao: this.presets[this.numeroPreset - 1].led6Tensao,
      led7Tensao: this.presets[this.numeroPreset - 1].led7Tensao,
      led8Tensao: this.presets[this.numeroPreset - 1].led8Tensao,
      led9Tensao: this.presets[this.numeroPreset - 1].led9Tensao,
    };
    if (this.link) {
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.resposta = this.presetsProvider.setRegulagemJSONMulti(dados, this.listaUniao[i].ix)
          .then((resp) => {
            console.log("POST resposta: ", resp);
          })
          .catch((e) => {
            console.error("ERRO!", e);
          });
        console.log(this.resposta, "Led A = " + this.presets[this.numeroPreset - 1].led1Tensao, "Led B = " + this.presets[this.numeroPreset - 1].led2Tensao, "Led C = " + this.presets[this.numeroPreset - 1].led3Tensao);
      }
    } else {
      this.resposta = this.presetsProvider.setRegulagemJSON(dados)
        .then((resp) => {
          console.log("POST resposta: ", resp);
        })
        .catch((e) => {
          console.error("ERRO!", e);
        });
      console.log(this.resposta, "Led A = " + this.presets[this.numeroPreset - 1].led1Tensao, "Led B = " + this.presets[this.numeroPreset - 1].led2Tensao, "Led C = " + this.presets[this.numeroPreset - 1].led3Tensao);
    }
  }

  salvarPresetJSON() {
    this.loading = this.loader.create({
      content: "Salvando...",
    });

    let dados = {
      nome: "preset" + this.numeroPreset,
      led1Tensao: this.presets[this.numeroPreset - 1].led1Tensao,
      led2Tensao: this.presets[this.numeroPreset - 1].led2Tensao,
      led3Tensao: this.presets[this.numeroPreset - 1].led3Tensao,
      led4Tensao: this.presets[this.numeroPreset - 1].led4Tensao,
      led5Tensao: this.presets[this.numeroPreset - 1].led5Tensao,
      led6Tensao: this.presets[this.numeroPreset - 1].led6Tensao,
      led7Tensao: this.presets[this.numeroPreset - 1].led7Tensao,
      led8Tensao: this.presets[this.numeroPreset - 1].led8Tensao,
      led9Tensao: this.presets[this.numeroPreset - 1].led9Tensao,
    };

    this.loading.present().then(() => {
      if (this.link) {
        let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
        for (let i = 0; i < this.listaUniao.length; i++) {
          this.resposta = this.presetsProvider.setRegulagemJSONMulti(dados, this.listaUniao[i].ix)
            .then((resp) => {
              console.log("POST resposta: ", resp);
              if (index == this.listaUniao[i].ix) {
                this.storage.set('presets', this.presets);
                this.toast.create({ message: "Preset salvo.", duration: 2000, position: 'top' }).present();
                this.salvarTemporizadorJSON();
              }
            })
            .catch((e) => {
              if (index == this.listaUniao[i].ix) {
                this.loading.dismiss();
                this.botaoEditarSalvar = false;//pasará por IonDIDLeave(){}
                this.alert.present();
                // this.navCtrl.setRoot(this.indisponivelPage);
              }
              console.error("ERRO!", e);
            });
        }
      } else {
        this.resposta = this.presetsProvider.setRegulagemJSON(dados)
          .then((resp) => {
            console.log("POST resposta: ", resp);
            this.storage.set('presets', this.presets);
            this.toast.create({ message: "Preset salvo.", duration: 2000, position: 'top' }).present();
            this.salvarTemporizadorJSON();
          })
          .catch((e) => {
            this.loading.dismiss();
            console.error("ERRO!", e);
            this.botaoEditarSalvar = false;//pasará por IonDIDLeave(){}
            this.alert.present();
            // this.navCtrl.setRoot(this.indisponivelPage);
          });
      }
    });
    console.log("Variável this.resposta = " + this.resposta, "Led A = " + this.presets[this.numeroPreset - 1].led1Tensao, "Led B = " + this.presets[this.numeroPreset - 1].led2Tensao, "Led C = " + this.presets[this.numeroPreset - 1].led3Tensao);
  }

  salvarTemporizadorJSON() {
    let presetsJSON = [];
    ///////////TESTE/////////////
    let flagErroHorarios = false;
    /////////////////////////////
    console.log("presets.ts this.presets = ", this.presets)
    let hrFim;
    ////TESTE/////
    let hrInicio;
    //////////////
    for (let i = 0; i < this.presets.length; i++) {
      console.log("presets.ts (for) this.presets[i] = ", this.presets[i])
      hrInicio = this.util.convertHoraMilisegundos(this.presets[i].hora);
      if (i < (this.presets.length - 1)) {//Define a hora final de cada ajuste automaticamente
        hrFim = this.util.convertHoraMilisegundos(this.presets[i + 1].hora)
      } else {
        hrFim = this.util.convertHoraMilisegundos(this.presets[0].hora)
      }

      presetsJSON.push(
        {
          hrInicio: hrInicio,
          hrFinal: hrFim,
          nome: this.presets[i].nome,
          gradual: this.presets[i].gradual,
        });
    }
    if (this.link) {
      let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.presetsProvider.setTemporizadorJSONMulti(presetsJSON, this.listaUniao[i].ix)
          .then((resp) => {
            if (index == this.listaUniao[i].ix) {
              this.toast.create({ message: "Configurações Salvas!", duration: 2000, position: 'top' }).present();
              this.estadoEditarSalvar();
              this.popCallback();
              this.loading.dismiss();
              // if (flagErroHorarios) {
              //   this.alertaAvisoHorariosAjustes();
              // }
            }
            console.log("Resposta: ", resp)
          })
          .catch((e) => {
            if (index == this.listaUniao[i].ix) {
              this.loading.dismiss();
              this.alert.present();
            }
            console.error("ERRO!", e);
          });
      }
    } else {
      this.presetsProvider.setTemporizadorJSON(presetsJSON)
        .then((resp) => {
          this.toast.create({ message: "Configurações Salvas!", duration: 2000, position: 'top' }).present();
          this.estadoEditarSalvar();
          this.popCallback();
          this.loading.dismiss();
          // if (flagErroHorarios) {
          //   this.alertaAvisoHorariosAjustes();
          // }
        })
        .catch((e) => {
          this.loading.dismiss();
          console.error("ERRO!", e);
          this.alert.present();
        });
    }
  }

  ligarDesligarGradual() {
    this.gradual = !this.gradual;
    this.presets[this.numeroPreset - 1].gradual = this.gradual ? 1 : 0;
    console.log('Estado Gradual: ', this.gradual ? 1 : 0);
    console.log('Estado Gradual (boolean): ', this.gradual);
    console.log('Estado Gradual (this.presets[this.numeroPreset - 1].gradual) (boolean): ', this.gradual);
  }

  estadoEditarSalvar() {
    this.botaoEditarSalvar = !this.botaoEditarSalvar;
    // dados = 0 (sugestão)  
    let dados = this.botaoEditarSalvar ? 1 : 0;
    console.log("estadoEditarSalvar(): this.presetsProvider.setModo(" + dados + ", " + this.numeroPreset + ") ");
    if (this.link) {
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.resposta = this.presetsProvider.setModoMulti(dados, this.numeroPreset, this.listaUniao[i].ix)
          .then((resp) => {
            console.log("GET resposta: ", resp);
          })
          .catch((e) => {
            console.error("ERRO!", e);
          });
        console.log('Estado Botao Editar/Salvar: ', this.botaoEditarSalvar);
      }
    } else {
      this.resposta = this.presetsProvider.setModo(dados, this.numeroPreset)
        .then((resp) => {
          console.log("GET resposta: ", resp);
        })
        .catch((e) => {
          console.error("ERRO!", e);
        });
      console.log('Estado Botao Editar/Salvar: ', this.botaoEditarSalvar);
    }
  }

  ajustarTempoPreset() {
    this.presets[this.numeroPreset - 1].hora = this.horaAtual;
    //////TESTE: proibir o usuário de colocar a hora anterior maior que a posterior///////// 
    if ((this.numeroPreset) > 1)
      if (this.presets[this.numeroPreset - 2].hora > this.presets[this.numeroPreset - 1].hora) {
        this.presets[this.numeroPreset - 1].hora = this.presets[this.numeroPreset - 2].hora;        
        this.alertaAvisoHorariosAjustes();
      }
    ////////////////////////////////////////////////////////////////////////////////////////
  }

  public chartClicked(e: any): void {
    console.log("Click no gráfico: ", e);
    this.goToSlide(e);
  }

  goToSlide(slide) {
    this.slides.slideTo(slide, 500);
  }

  resetarValores() {
    this.presets[this.numeroPreset - 1].led1Tensao = 0;
    this.presets[this.numeroPreset - 1].led2Tensao = 0;
    this.presets[this.numeroPreset - 1].led3Tensao = 0;
    this.presets[this.numeroPreset - 1].led4Tensao = 0;
    this.presets[this.numeroPreset - 1].led5Tensao = 0;
    this.presets[this.numeroPreset - 1].led6Tensao = 0;
    this.presets[this.numeroPreset - 1].led7Tensao = 0;
    this.presets[this.numeroPreset - 1].led8Tensao = 0;
    this.presets[this.numeroPreset - 1].led9Tensao = 0;
  }
}