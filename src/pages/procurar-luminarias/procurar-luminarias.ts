import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, ToastController, ItemSliding, NavParams } from 'ionic-angular';
import { ProcurarLuminariasProvider } from '../../providers/procurar-luminarias/procurar-luminarias';
import { RecarregarProvider } from '../../providers/recarregar/recarregar';
import { HomeProvider } from '../../providers/home/home';
import { LuminariaProvider } from '../../providers/luminaria/luminaria';
import { PresetsProvider } from '../../providers/presets/presets';
import { UtilProvider } from '../../providers/util/util';
import { ValuesProvider } from '../../providers/values/values';
import { AutenticacaoProvider } from '../../providers/autenticacao/autenticacao';
import { IndisponivelPage } from '../indisponivel/indisponivel';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { FusoHoraProvider } from '../../providers/fuso-hora/fuso-hora';
import { Storage } from '@ionic/storage';
import { App } from "ionic-angular";
import { Zeroconf } from '@ionic-native/zeroconf';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Events } from 'ionic-angular/util/events';
import { ResetFabricaProvider } from '../../providers/reset-fabrica/reset-fabrica';
import { EfeitoTempestadeProvider } from '../../providers/efeito-tempestade/efeito-tempestade';
// import { Http } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-procurar-luminarias',
  templateUrl: 'procurar-luminarias.html',
})

export class ProcurarLuminariasPage extends RecarregarProvider {

  resposta: any;

  loading: any;

  servicoMDNS: any = [];

  tentarNovamente: boolean;

  timer: any;

  dadoNomeLuminaria: any;

  dadosLuminarias: any = [];

  nomeLuminaria: any;

  indexNome: any;

  n: number = 0;

  modo: any;

  recover: boolean;

  cancelar: boolean;

  constructor(public app: App, public navCtrl: NavController, public zeroconf: Zeroconf,
    public toast: ToastController, public loader: LoadingController, public navParams: NavParams,
    public procurarLuminariasProvider: ProcurarLuminariasProvider, public efeitoTempestadeProvider: EfeitoTempestadeProvider,
    public homeProvider: HomeProvider, public luminariaProvider: LuminariaProvider,
    public presetsProvider: PresetsProvider, public storage: Storage, public http: HttpClient,
    public util: UtilProvider, private value: ValuesProvider, public events: Events,
    public alertCtrlr: AlertController, public resetFabricaProvider: ResetFabricaProvider,
    private autenticacaoProvider: AutenticacaoProvider, public fusoHoraProvider: FusoHoraProvider) {
    super(app, homeProvider, luminariaProvider, efeitoTempestadeProvider, presetsProvider, storage, util, loader, toast, fusoHoraProvider);
    //this.botaoVisivel = this.navParams.get('param');
    console.log("ionViewDidEnter ProcurarLuminariasPage");
    this.modo = this.navParams.get('param1');
    this.recover = this.navParams.get('recover');
    this.cancelar = this.navParams.get('cancelar');
    this.watchMDNS();
  }

  ionViewDidEnter() { }

  ionViewWillLeave() {
    this.pararWatchMDNS();
    if (this.cancelar) {
      this.popCallback();
    }
  }

  // backButtonAction() {}

  popCallback() {
    // this.navCtrl.pop().then(() => {
    console.log("acionando callback...")
    let parametros = this.navParams.get("callback");
    parametros(true);
    // });
  }

  selecionarLuminaria(nmdns, ipmdns, slidingItem: ItemSliding) {
    // console.log("ipmdns = ", ipmdns);
    // console.log("nmdns = ", nmdns);
    slidingItem.close();
    let confirm = this.alertCtrlr.create({
      title: 'Confirmação.',
      message: 'Deseja conectar-se à luminária: ' + nmdns + ' ?',
      buttons: [
        {
          text: 'NÃO',
          handler: () => {
            console.log('Recusa conexão');
          }
        },
        {
          text: 'SIM',
          handler: () => {
            this.adicionaLuminaria(nmdns, ipmdns)
            console.log('Confirma conexão');
          }
        }
      ]
    });
    confirm.present();
  }

  removerLuminaria(nmdns, ipmdns, slidingItem: ItemSliding) {
    // console.log("ipmdns = ", ipmdns);
    // console.log("nmdns = ", nmdns);
    slidingItem.close();
    let confirm = this.alertCtrlr.create({
      title: 'Confirmação.',
      message: 'Deseja realmente remover a luminária: ' + nmdns + ' ?',
      buttons: [
        {
          text: 'NÃO',
          handler: () => {
            console.log('Recusa remoção');
          }
        },
        {
          text: 'SIM',
          handler: () => {
            this.removeReiniciaLuminaria(nmdns, ipmdns);
            console.log('Confirma remoção');
          }
        }
      ]
    });
    confirm.present();
  }

  alertaAcesoNegado() {
    let alert = this.alertCtrlr.create({
      title: 'Acesso Negado',
      message: 'Seu acesso a esta luminária foi negado. Por favor, reinicie a luminária pelo botão localizado em seu painel, conforme consta no manual e tente novamente.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Acesso negado ciente');
          }
        },
      ]
    });
    alert.present();
  }

  alertaErroCoexao() {
    let alert = this.alertCtrlr.create({
      title: 'Erro de comunicação',
      message: 'Falha ao tentar conectar à esta luminária.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Falha de comunicação');
          }
        },
      ]
    });
    alert.present();
  }

  alertaErroRemocao(nome) {
    let alert = this.alertCtrlr.create({
      title: 'Erro de comunicação',
      message: "Falha ao tentar remover a luminária " + nome + ".",
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Falha de comunicação');
          }
        },
      ]
    });
    alert.present();
  }

  adicionaLuminaria(nmdns, ipmdns) {
    this.storage.get("nomesLuminarias")
      .then(res => {
        if (this.modo != 2) {
          let resposta = [];
          resposta = res;
          console.log("ProcurarLuminariasPage selecionarLuminaria(nmdns, ipmdns) => nomesLuminarias (resposta): ", resposta)
          if (resposta != null) {
            if (resposta.length > 0) {
              this.modo = 1;
            }
          } else {
            ipmdns = ipmdns[0];
            this.modo = 0;
          }
        }
        console.log("ProcurarLuminariasPage selecionarLuminaria(nmdns, ipmdns) => modo: ", this.modo)
        this.getMDNS(nmdns, ipmdns, this.modo);
      });
  }

  removeReiniciaLuminaria(nmdns, ipmdns) {
    let loadingRemover = this.loader.create({
      content: "Removendo a luminária " + nmdns + "...",
    });
    console.log("nmdns = ", nmdns);
    console.log("ipmdns = ", ipmdns);
    loadingRemover.present().then(() => {
      this.storage.get("nomesLuminarias")
        .then(res00 => {
          let resposta = [];
          resposta = res00;
          console.log("removerLuminaria() => nomesLuminarias (resposta): ", resposta)
          this.indexNome = resposta.findIndex(i => i.name === nmdns);
          let luminariaRemovida = resposta.splice(this.indexNome, 1);
          console.log("luminariaRemovida = ", luminariaRemovida)
          console.log("Nome da luminária (" + nmdns + ") removida do array")
          console.log("removerLuminaria() => nomesLuminarias (resposta): ", resposta)
          this.publicarNomesLuminarias(resposta);
          this.storage.set("nomesLuminarias", resposta)
            .then(resNomesLuminarias => {
              console.log("Luminaria atualizada (Storage): ", resNomesLuminarias)
            });
          this.storage.get("auths")
            .then(res01 => {
              let resposta = [];
              resposta = res01;
              console.log("removerLuminaria() => auths  (resposta): ", resposta)
              let authRemovida = resposta.splice(this.indexNome, 1);
              console.log("authRemovida = ", authRemovida)
              console.log("Autenticação da luminária (" + nmdns + ") removida do array")
              console.log("removerLuminaria() => auths  (resposta): ", resposta)
              this.storage.set("auths", resposta)
                .then(resAuths => {
                  console.log("Array de auths atualizado (Storage): ", resAuths);
                });
              this.storage.get("urls")
                .then(res02 => {
                  let resposta = [];
                  resposta = res02;
                  console.log("removerLuminaria() => urls  (resposta): ", resposta)
                  let urlRemovida = resposta.splice(this.indexNome, 1);
                  console.log("urlRemovida = ", urlRemovida)
                  console.log("Url da luminária (" + nmdns + ") removida do array")
                  console.log("removerLuminaria() => urls  (resposta): ", resposta)
                  this.storage.set("urls", resposta)
                    .then(resUrls => {
                      console.log("Array de urls atualizado (Storage): ", resUrls);
                    });
                });
            });
        });
      this.resetFabricaProvider.getReset(nmdns)
        .then(resetNMDNS => {
          console.log("Luminária " + nmdns + " removida com sucesso.")
          loadingRemover.dismiss();
          this.toast.create({ message: "Luminária " + nmdns + " removida com sucesso.", duration: 2000, position: 'top' }).present();
        }).catch(ErroNMDNS => {
          console.error("Erro ao tentar remover Luminaria no NMDNS (getReset()): ", ErroNMDNS);
          this.resetFabricaProvider.getReset(ipmdns)
            .then(resetIPMDNS => {
              console.log("Luminária " + nmdns + " removida com sucesso.")
              loadingRemover.dismiss();
              this.removeLuminariaArray(nmdns);
              this.toast.create({ message: "Luminária " + nmdns + " removida com sucesso.", duration: 2000, position: 'top' }).present();
            }).catch(ErroIPMDNS => {
              // this.alertaErroRemocao(nmdns);
              loadingRemover.dismiss();
              this.removeLuminariaArray(nmdns);
              this.toast.create({ message: "Luminária " + nmdns + " removida com sucesso, porém, a luminária não pode ser reiniciada.", duration: 4000, position: 'top' }).present();
              console.error("Erro ao tentar remover Luminaria no IPMDNS (getReset()): ", ErroIPMDNS);
            });
        });
    });
  }

  removeLuminariaArray(nmdns) {
    let indexNome = this.dadosLuminarias.findIndex(i => i.name === nmdns);
    // this.dadosLuminarias.splice(indexNome, 1);
    if (this.dadosLuminarias[indexNome].status == true) {
      this.dadosLuminarias[indexNome].cadastrada = false;
    } else {
      this.dadosLuminarias.splice(indexNome, 1);
    }
  }

  watchMDNS() {
    this.n = 0;
    this.dadosLuminarias = null;
    this.dadosLuminarias = [];
    let x = 0;
    this.servicoMDNS = [];
    this.zeroconf.close();
    this.tentarNovamente = false;
    let posicao = 0;
    this.loading = this.loader.create({
      content: "Procurando Luminárias...",
    });

    this.tentarNovamente = true;

    this.loading.present().then(() => {

      ////////////////////////////TESTAR////////////////////////////
      this.timer = setTimeout(() => {
        if (this.servicoMDNS.length <= 0 || this.servicoMDNS.length == null) {
          this.loading.dismiss();
          this.zeroconf.stop();
          this.toast.create({ message: "Nenhuma luminária Encontrada!", duration: 2000, position: 'top' }).present();
          // console.log("Tentar Novamente: (dps do zeroconf unwatch) ", this.tentarNovamente);
          console.log("Timeout: Nenhuma Luminária encontrada ");
        }
        return;
      }, 5000);
      ////////////////////////////////////////////////////////     
      this.zeroconf.watch('_coil-aurora._tcp.', 'local.')
        .subscribe(result => {
          console.log("Iniciando a busca por luminárias...")
          if (result.action == 'resolved') {
            console.log('service resolved', result.service);
            if (String(result.service.ipv4Addresses) != '0.0.0.0') {
              // this.tentarNovamente = false;
              this.servicoMDNS.push(result.service);
              // this.toast.create({ message: "Luminária(s) Encontrada(s)!", duration: 4000, position: 'bottom' }).present();
            }
            //this.servicoMDNS += JSON.parse(JSON.stringify(result.service));
            console.log("Resposta ZEROCONF: ", result.service);
            if (this.servicoMDNS.length > 0) {
              x++;
              console.log("Vezes da iteração MDNS ZeroConf: x = ", x);
              this.checarExistencia();
              clearTimeout(this.timer);
              console.log("this.servicoMDNS = ", this.servicoMDNS);
            }
          }
        });
      console.log("loading.dismiss() depois do watch.");
      //this.servicoMDNS = JSON.parse('{"domain":"local.","type":"_http._tcp.","name":"AQUASUNDEVICE","port":80,"hostname":"aquasundevice.local.","ipv4Addresses":["192.168.0.36"],"ipv6Addresses":[],"txtRecord":{}}{"domain":"local.","type":"_http._tcp.","name":"esp8266","port":80,"hostname":"esp8266.local.","ipv4Addresses":["192.168.0.42"],"ipv6Addresses":[],"txtRecord":{}}');
    });
  }

  checarExistencia() {
    console.log("Checando existência...");
    let valores = {
      name: "",
      ipv4Addresses: "",
      cadastrada: false,
      selecionada: false,
      down: false,
    };
    this.storage.get("nomeLuminaria").then(res0 => {
      this.dadoNomeLuminaria = res0;
      this.storage.get("nomesLuminarias")
        .then(res => {
          if (res != null && res.length > 0) {

            this.dadosLuminarias = null;
            this.dadosLuminarias = [];

            let resposta = res;
            console.log("checarExistencia() nomesLuminarias (res) = ", resposta)
            console.log("this.servicoMDNS.length = ", this.servicoMDNS.length);
            for (let i = 0; i < this.servicoMDNS.length; i++) {
              console.log("this.servicoMDNS[" + i + "].name = ", this.servicoMDNS[i].name)
              let index00 = resposta.findIndex(respi => respi.name === this.servicoMDNS[i].name);
              console.log("index00 = ", index00);
              if (index00 >= 0) {
                valores.name = this.servicoMDNS[i].name;
                valores.ipv4Addresses = this.servicoMDNS[i].ipv4Addresses[0];
                valores.cadastrada = true;
              } else {
                valores.name = this.servicoMDNS[i].name;
                valores.ipv4Addresses = this.servicoMDNS[i].ipv4Addresses[0];
                valores.cadastrada = false;
              }

              this.dadosLuminarias.push({ name: valores.name, ipv4Addresses: valores.ipv4Addresses, cadastrada: valores.cadastrada, selecionada: valores.selecionada, down: valores.down });

              // this.dadosLuminarias.push({ name: valores.name, ipv4Addresses: valores.ipv4Addresses, cadastrada: valores.cadastrada, selecionada: valores.selecionada, status: valores.status });
              console.log("this.dadosLuminarias[" + i + "].cadastrada: ", this.dadosLuminarias[i].cadastrada);
              console.log("this.dadosLuminarias[" + i + "].selecionada: ", this.dadosLuminarias[i].selecionada);
              console.log("this.dadosLuminarias: ", this.dadosLuminarias);
            }

            for (let j = 0; j < resposta.length; j++) {
              let index03 = this.dadosLuminarias.findIndex(resp => resp.name === resposta[j].name);
              console.log("index03 = ", index03)
              if (index03 < 0) {
                valores.name = resposta[j].name;
                console.log("resposta[" + j + "].name = ", resposta[j].name);
                valores.ipv4Addresses = "0.0.0.0";
                valores.cadastrada = true;
                valores.down = true;
                this.dadosLuminarias.push({ name: valores.name, ipv4Addresses: valores.ipv4Addresses, cadastrada: valores.cadastrada, selecionada: valores.selecionada, down: valores.down });
              }
            }

            console.log("Nome da luminária: this.dadoNomeLuminaria (alias)", this.dadoNomeLuminaria);
            let index01 = resposta.findIndex(respi => respi.alias === this.dadoNomeLuminaria);
            console.log("resposta.findIndex(respi => respi.alias === this.dadoNomeLuminaria) (index01)", index01);
            if (index01 >= 0) {
              let index02 = this.dadosLuminarias.findIndex(respi => respi.name === resposta[index01].name);
              console.log("resposta.findIndex(respi => respi.alias === this.nomesLuminarias) (index02)", index02);
              if (index02 >= 0) {
                this.dadosLuminarias[index02].selecionada = true;
              }
            }
          } else {
            this.dadosLuminarias = this.servicoMDNS;
          }
          this.n++;
          console.log("n = ", this.n);
          if (this.n >= this.servicoMDNS.length) {
            this.loading.dismiss();
            this.zeroconf.stop();
          }
          // this.zeroconf.close();

        });
    });
  }

  atualizarLuminariaAdicionada(nome, recover) {
    let index = this.dadosLuminarias.findIndex(i => i.name === nome);
    if (recover) {
      for (let i = 0; i < this.dadosLuminarias.length; i++) {
        this.dadosLuminarias[i].selecionada = false;
      }
      this.dadosLuminarias[index].selecionada = true;
    }
    this.dadosLuminarias[index].cadastrada = true;

    console.log("atualizarLuminariaAdicionada(): this.dadosLuminarias = ", this.dadosLuminarias);
    console.log("atualizarLuminariaAdicionada(): this.dadoNomeLuminaria = ", this.dadoNomeLuminaria);
  }

  atualizarLuminariaSelecionada(ipmdns) {
    this.storage.get("urls").then(resUrls => {
      console.log("resUrls = ", resUrls);
      let index = resUrls.findIndex(i => i.url === ipmdns);
      console.log("Index resUrls = ", index);
      this.storage.get("nomesLuminarias").then(res => {
        for (let i = 0; i < res.length; i++) {
          res[i].selecionada = false;
        }
        res[index].selecionada = true;
        this.publicarNomesLuminarias(res);
        this.storage.set("nomeLuminaria", res[index].alias)
          .then(respNome => {
            console.log("Nome da luminária alterado");
          })
        this.storage.set("nomesLuminarias", res)
          .then(respNomes => {
            console.log("recuperar-ends.ts nomesLuminarias alterado: ", respNomes);
          });
      });
    });
  }

  guardarNomeLuminaria(nome) {
    if (nome != null) {
      this.nomeLuminaria = nome;
      console.log("this.servicoMDNS = ", this.servicoMDNS);
      this.storage.get("nomesLuminarias")
        .then(res => {
          let nomesLuminarias = [];
          if (res != null) {
            nomesLuminarias = res;
          }
          if (nomesLuminarias.length > 0) {
            nomesLuminarias.push({ name: nome, alias: nome, status: true, selecionada: false, link: false });
          } else {
            nomesLuminarias.push({ name: nome, alias: nome, status: true, selecionada: true, link: false });
          }
          if (nomesLuminarias.length > 0) {
            this.storage.set("nomesLuminarias", nomesLuminarias)
              .then(res => {
                console.log("guardarNomesLuminarias() guardados (Sorage): ", res);
                this.publicarNomesLuminarias(nomesLuminarias);
              })
          }
        })
      this.storage.get("nomeLuminaria").then(res => {
        if (res == null) {
          this.storage.set("nomeLuminaria", nome).then(resp => {
            console.log("Nome da luminária guardado (Storage)", nome);
          });
        }
      });
    } else {
      console.error("guardarNomeLuminaria(nome): o nome veio nulo!!!");
    }
  }

  ///////////////////UNIFICAR////////////////////////////////////////////////
  publicarNomesLuminarias(nomesLuminarias) {
    this.events.publish('nomes', nomesLuminarias);
    console.log("Tentando mudar o nome da variavel em app.components.ts e home.ts...");
    // this.loading.dismiss();
  }

  pararWatchMDNS() {
    this.zeroconf.unwatch('_http._tcp.', 'local.')
      .then(res => {
        console.log("unwatch() res: ", res);
      }).catch(err => {
        console.error("unwatch() err: ", err);
      });
    clearTimeout(this.timer);
  }

  getMDNS(nmdns, ipmdns, modo) {
    this.cancelar = false;
    let auth = new Date().toLocaleString();
    let msg;
    if (modo == 0) {
      msg = "Configurando o APP Aurora...";
    } else if (modo == 1 || modo == 2) {
      msg = "Adicionando luminária...";
    }
    // this.pararWatchMDNS();//NOVO! TESTAR!
    this.zeroconf.close();
    this.loading = this.loader.create({
      content: "Configurando o APP Aurora...",
    });
    this.loading.present()
      .then(() => {
        this.storage.set("luminaria", nmdns);
        this.procurarLuminariasProvider.getDadosLuminariaNome(nmdns)//Tenta resolver por nome
          .then((resp) => {
            this.resposta = resp;
            console.log("GET Request Resolvendo Nome Completo ", resp);
            if (modo == 0 || modo == 2) {
              this.value.setUrl(nmdns);//Altera o valor da variável url do ESP no APP
            }
            this.autenticacaoProvider.getAutorizacaoJSON(nmdns)//Busca a existência de uma chave no ESP
              .then(respauth => {
                console.log("Autorizacao ESP DNS: resp =", respauth);
                if (respauth == null || respauth['autorizacao'] == "0") {//Se não exitir chave no ESP...
                  this.storage.set("firstRun", 1)//Grava o atributo firstRun para indicar que o APP já foi executado 1 vez
                    .then(res00 => {
                      // this.storage.set("url", nmdns)//Grava na memória o valor acima para futuras execuções do APP
                      //   .then(res01 => {
                      //     console.log("Sucesso ao gravar a url (Nome) values.provider: " + nmdns + "=> ", res01)
                      this.guardarNomeLuminaria(nmdns);
                      console.log("this.value.addUrl(nmdns): ", nmdns);
                      this.value.addUrl(nmdns);
                      this.value.addAuth(auth);
                      this.autenticacaoProvider.setAutorizacaoJSON(auth, nmdns)//Grava no ESP a chave de autenticação
                        .then(res02 => {
                          console.log("Sucesso ao gravar autorização no ESP! => " + res02 + ", auth = " + auth);
                          if (modo == 0) {
                            this.storage.set("auth", auth)//Grava na memória do APP a chave de autenticação
                              .then(res03 => {
                                console.log("auth gravada: ", auth)
                                this.loading.dismiss();
                                console.log("this.carregarLuminaria() NMDNS;")
                                this.carregarLuminaria(this.nomeLuminaria);
                              }).catch(err03 => {
                                console.error("Erro ao gravar a autenticação: " + auth + "=> ", err03)
                              });
                          } else if (modo == 1) {
                            this.atualizarLuminariaAdicionada(nmdns, this.recover);
                            this.loading.dismiss();
                            this.toast.create({ message: "Luminária adicionada com sucesso!", duration: 2000, position: 'top' }).present();
                          } else if (modo == 2) {
                            this.atualizarLuminariaSelecionada(nmdns)
                            this.loading.dismiss();
                            this.carregarLuminaria(this.nomeLuminaria);
                          }
                        }).catch(err02 => {
                          console.error("Erro ao gravar autorização no ESP!", err02);
                        })
                      // }).catch(err01 => {
                      //   console.error("Erro ao gravar a url values.provider: " + nmdns + "=> ", err01)
                      // });
                    });
                } else {
                  console.log("Acesso NEGADO!");
                  this.loading.dismiss();
                  this.alertaAcesoNegado();
                  // this.toast.create({ message: "Acesso Negado a esta luminária!", duration: 5000, position: 'bottom' }).present();
                  //this.navCtrl.push(AcessoNegadoPage);
                }
              }).catch(erroauth => {
                console.error("Nome => Erro (POST) ao tentar autenticar...", erroauth);
                this.loading.dismiss();
                this.alertaErroCoexao();
              })
          }).catch((e) => {//Não conseguiu resolver por nome!
            this.resposta = e;
            console.error("ERRO Resolvendo Nome!", e);
            this.procurarLuminariasProvider.getDadosLuminariaIP(ipmdns)//Tenta resolver por IP
              .then((resp) => {
                this.resposta = resp;
                console.log("GET Request Resolvendo IP Completo ", resp);
                if (modo == 0 || modo == 2) {
                  this.value.setUrl(ipmdns);//Altera o valor da variável url do ESP no APP
                }
                this.autenticacaoProvider.getAutorizacaoJSON(ipmdns)//Busca a existência de uma chave no ESP
                  .then(respauth => {
                    console.log("Autorizacao ESP IP: resp =", respauth);
                    if (respauth == null || respauth['autorizacao'] == "0") {//Se não exitir chave...
                      this.storage.set("firstRun", 1)//Grava o atributo firstRun para indicar que o APP já foi executado 1 vez
                        .then(res00 => {
                          // this.storage.set("url", ipmdns)//Grava na memória o valor acima para futuras execuções do APP
                          //   .then(res01 => {
                          //     console.log("Sucesso ao gravar a url (IP) em values.provider: " + ipmdns + "=> ", res01)
                          this.guardarNomeLuminaria(nmdns);
                          console.log("this.value.addUrl(ipmdns): ", ipmdns);
                          this.value.addUrl(ipmdns);
                          this.value.addAuth(auth);
                          this.autenticacaoProvider.setAutorizacaoJSON(auth, ipmdns)//Grava no ESP a chave de autenticação
                            .then(res02 => {
                              console.log("Sucesso ao gravar autorização no ESP!", res02);
                              if (modo == 0) {
                                this.storage.set("auth", auth)//Grava na memória do APP a chave de autenticação
                                  .then(res03 => {
                                    console.log("auth gravada: ", auth)
                                    this.loading.dismiss();
                                    console.log("this.carregarLuminaria() IPMDNS;")
                                    this.carregarLuminaria(this.nomeLuminaria);
                                  }).catch(err03 => {
                                    console.error("Erro ao gravar a autenticação: " + auth + "=> ", err03)
                                  });
                              } else if (modo == 1) {
                                this.atualizarLuminariaAdicionada(nmdns, this.recover);
                                this.loading.dismiss();
                                this.toast.create({ message: "Luinária adicionada com sucesso!", duration: 2000, position: 'top' }).present();
                              } else if (modo == 2) {
                                this.atualizarLuminariaSelecionada(ipmdns);
                                this.loading.dismiss();
                                this.carregarLuminaria(this.nomeLuminaria);
                              }
                            }).catch(err02 => {
                              console.error("Erro ao gravar autorização no ESP!", err02);
                            })
                          // }).catch(err01 => {
                          //   console.error("Erro ao gravar a url values.provider: " + ipmdns + "=> ", err01)
                          // });
                        })
                        .catch((e) => {
                          this.resposta = e;
                          console.error("ERRO Resolvendo IP!", e);
                          this.loading.dismiss();
                        });
                    } else {
                      console.log("Acesso NEGADO!");
                      this.loading.dismiss();
                      this.alertaAcesoNegado();
                      // this.toast.create({ message: "Acesso Negado à esta luminária!", duration: 5000, position: 'bottom' }).present();
                      // this.navCtrl.push(AcessoNegadoPage);
                    }
                  }).catch(erroauth => {
                    console.error("IP => Erro (POST) ao tentar autenticar...", erroauth);
                    this.loading.dismiss();
                    this.alertaErroCoexao();
                  })
              }).catch((e) => {
                console.error("ERRO Resolvendo IP!", e);
                this.loading.dismiss();
                this.alertaErroCoexao();
              });
          });
      });
  }
}