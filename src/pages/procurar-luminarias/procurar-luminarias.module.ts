import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProcurarLuminariasPage } from './procurar-luminarias';

@NgModule({
  declarations: [
    ProcurarLuminariasPage,
  ],
  imports: [
    IonicPageModule.forChild(ProcurarLuminariasPage),
  ],
})
export class ProcurarLuminariasPageModule {}
