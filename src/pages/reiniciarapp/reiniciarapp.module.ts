import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReiniciarappPage } from './reiniciarapp';

@NgModule({
  declarations: [
    ReiniciarappPage,
  ],
  imports: [
    IonicPageModule.forChild(ReiniciarappPage),
  ],
})
export class ReiniciarappPageModule {}
