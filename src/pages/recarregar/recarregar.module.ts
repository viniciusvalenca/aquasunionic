import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecarregarPage } from './recarregar';

@NgModule({
  declarations: [
    RecarregarPage,
  ],
  imports: [
    IonicPageModule.forChild(RecarregarPage),
  ],
})
export class RecarregarPageModule {}
