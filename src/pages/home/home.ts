import { Component } from '@angular/core';
import { NavController, IonicPage, AlertController, Platform, LoadingController } from 'ionic-angular';
import { HomeProvider } from '../../providers/home/home';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/mergeMapTo';
import 'rxjs/add/observable/timer';
import { ValuesProvider } from '../../providers/values/values';
import { AlarmeTemperaturaPage } from '../alarme-temperatura/alarme-temperatura';
import { Storage } from '@ionic/storage';
import { UtilProvider } from '../../providers/util/util';
import { Events } from 'ionic-angular/util/events';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { ModalController } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { LuminariaProvider } from '../../providers/luminaria/luminaria';
import { PresetsProvider } from '../../providers/presets/presets';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  canaisLuminaria: number;
  graficos: Array<any> = [];
  parametrosGrafico: any = {};
  corTexto: any = 'black';
  radius: any = 0;
  backgroundColor: any = 'darkgrey';
  backgroundPadding: any = "-8";
  backgroundStroke: any = 'transparent';
  backgroundStrokeWidth: any = "0";
  backgroundOpacity: any = "0.5";
  showBackground: boolean = true;
  space = "5";
  showInnerStroke: boolean = true;
  innerStrokeWidth: any = "2";
  innerStrokeColor: any = 'white';
  animation: boolean = false;
  animationDuration: any = "0";
  showTitle: boolean = true;
  titleFontSize: any = 0;
  titleColor: any = this.corTexto;
  subtitleColor: any = 'rgb(39, 37, 38)';
  subtitleFontSize: any = 0;
  showSubtitle: boolean = true;
  responsive: boolean = false;
  outerStrokeLinecap: any = "butt";
  outerStrokeWidth: any = "6";
  unitsColor: any = this.corTexto;
  proxHorario: any;
  corCanais: any = [
    'whitesmoke',
    'blue',
    'red',
    'green',
    'violet',
    'lightblue',
    'yellow',
    'pink',
    'orange',
  ]
  subtitlesCanais: any = [
    "Canal A",
    "Canal B",
    "Canal C",
    "Canal D",
    "Canal E",
    "Canal F",
    "Canal G",
    "Canal H",
    "Canal I",
  ];

  loading: any;
  temperatura: string = "0";
  tempMax: any;
  tempMin: any;
  mostrarTemperatura: boolean;
  resposta: any;
  contadorFalhas: any = 3;
  mostrarAlerta: boolean = true;
  nome: any;
  modal: any;
  presetAtual: any;
  nomesLuminarias: Array<any> = [];
  timer: boolean;
  link: boolean;
  ligarLuminaria: boolean;
  tempestadeLigada: boolean;
  listaUniao: Array<any> = [];
  appCarregado: boolean;

  constructor(public navCtrl: NavController, public homeProvider: HomeProvider, private screenOrientation: ScreenOrientation,
    public value: ValuesProvider, private storage: Storage, public util: UtilProvider, public modalCtrl: ModalController,
    public alertCtrlr: AlertController, private platform: Platform, public luminariaProvider: LuminariaProvider,
    public presetsProvider: PresetsProvider, public loader: LoadingController, public events: Events) {
    console.log('HOME PAGE');

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT).then(() => {
      console.info("Orientação: Retrato.");
    });
    // this.configuraGrafico();
    this.carregarPower();
    this.eventoPowerTempestade();
    this.carregarTempestade();
    this.carregarValorLink();
    this.carregarQtdeCanais();
    this.eventoNomesLuminarias();
    this.eventoTimer();
    this.carregarNomesLuminarias();
    this.carregarLimitesTemperatura();
    this.carregarTemperatura();
  }

  configuraGrafico(numeroCanais) {
    for (let i = 0; i < numeroCanais; i++) {
      this.parametrosGrafico = {
        corTexto: this.corTexto,
        radius: String(numeroCanais == 1 ? 45 : numeroCanais == 2 ? 40 : numeroCanais == 3 ? 33 : numeroCanais == 4 ? 27 : numeroCanais == 5 ? 21 : 16),
        backgroundColor: this.backgroundColor,
        backgroundPadding: this.backgroundPadding,
        backgroundStroke: this.backgroundStroke,
        backgroundStrokeWidth: this.backgroundStrokeWidth,
        backgroundOpacity: this.backgroundOpacity,
        showBackground: this.showBackground,
        space: this.space,
        showInnerStroke: this.showInnerStroke,
        innerStrokeWidth: this.innerStrokeWidth,
        innerStrokeColor: this.innerStrokeColor,
        animation: this.animation,
        animationDuration: this.animationDuration,
        showTitle: this.showTitle,
        titleFontSize: String(numeroCanais == 1 ? 14 : numeroCanais == 2 ? 13 : numeroCanais == 3 ? 12 : numeroCanais == 4 ? 10 : numeroCanais == 5 ? 8 : 6),
        titleColor: this.titleColor,
        subtitle: "Canal " + (i + 1),
        subtitleColor: this.subtitleColor,
        subtitleFontSize: String(numeroCanais == 1 ? 12 : numeroCanais == 2 ? 11 : numeroCanais == 3 ? 10 : numeroCanais == 4 ? 9 : numeroCanais == 5 ? 7 : 6),
        showSubtitle: this.showSubtitle,
        responsive: this.responsive,
        outerStrokeLinecap: this.outerStrokeLinecap,
        outerStrokeWidth: this.outerStrokeWidth,
        unitsColor: this.unitsColor,
        corCanal: this.corCanais[i],
        subtitlesCanal: this.subtitlesCanais[i],
        valorCanal: 0,
      };
      this.graficos.push(this.parametrosGrafico);
    }
  }

  eventoPowerTempestade() {
    this.events.subscribe('tempestadeLigada', (tempestadeLigada) => {
      this.tempestadeLigada = tempestadeLigada;
      console.log("this.tempestadeLigada (storm) em Home.ts = ", tempestadeLigada);
    });
  }

  eventoNomesLuminarias() {
    this.events.subscribe('nomes', (nomes) => {
      this.nomesLuminarias = nomes;
      console.log("this.nomesLuminarias em Home.ts = ", nomes);
      // for (let i = 0; i < this.nomesLuminarias.length; i++) {
      //   if (this.nomesLuminarias[i].link) {
      //     this.listaUniao.push({ ix: [i] });
      //   }
      // }
    });
  }

  eventoTimer() {
    this.events.subscribe('timer', (timer) => {
      this.timer = timer;
      console.log("this.timer em Home.ts = ", timer);
    });
  }

  backButtonAction() {
    let alert;
    //this.viewCtrl.dismiss();
    alert = this.alertCtrlr.create({
      title: "Sair do programa",
      message: "Tem certeza que deseja realmente sair do programa?",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Saída do programa cancelada');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            console.log('Saiu do programa');
            this.platform.exitApp();
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidEnter() {
    this.carregarNomeLuminaria();
    // this.selecionarLuminaria();
    this.exibirTemperatura();
    this.verificarTemperatura();
  }

  ionViewDidLeave() {
    // if (this.mostrarTemperatura) {
    clearInterval(this.value.timer);
    console.log("Timer CANCELADO!");
    // }
  }

  // selecionarLuminaria() {
  //   this.events.publish('nome', this.nome);
  //   console.log("Tentando mudar o nome da variavel em app.components.ts...");
  // }

  alertaFalhaComunicacaoHome() {
    let alert;
    alert = this.alertCtrlr.create({
      title: "Falha de comunicação",
      message: "Não foi possível obter informações de temperatura da luminária. Por favor, verifique se ela encontra-se ligada e conectada a rede WiFi.",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Alerta Falha Termômetro');
            this.mostrarAlerta = true;
            this.exibirTemperatura();
          }
        },
      ]
    });
    alert.present();
  }

  carregarPower() {
    this.storage.get("power").then(res => {
      this.ligarLuminaria = res;
      console.log("power home.ts = ", this.ligarLuminaria)
    })
  }

  carregarTempestade() {
    this.storage.get("storm").then(resp => {
      this.tempestadeLigada = resp;
      console.log("storm home.ts = ", this.tempestadeLigada)
    })
  }

  carregarValorLink() {
    this.storage.get("linkHomePage").then(res => {
      this.link = res;
      console.log("this. link (home.ts) = ", this.link)
    })
  }

  carregarQtdeCanais() {
    this.storage.get("canaisLuminaria").then(res => {
      console.log("Qtde de canais recuperados (Storage): ", res);
      this.canaisLuminaria = Number(res);
      this.configuraGrafico(this.canaisLuminaria);
    })
  }

  carregarNomeLuminaria() {
    this.storage.get("nomeLuminaria").then(res => {
      this.nome = res;
      console.log("nomeLuminaria = ", this.nome)
    });
  }

  carregarNomesLuminarias() {
    this.storage.get("nomesLuminarias").then(res => {
      this.nomesLuminarias = res;
      console.log("nomesLuminarias (HomePage) = ", this.nomesLuminarias)
      // for (let i = 0; i < this.nomesLuminarias.length; i++) {
      //   if (this.nomesLuminarias[i].link) {
      //     this.listaUniao.push({ ix: [i] });
      //   }
      // }
    });
  }

  carregarLimitesTemperatura() {
    this.storage.get('tempMax').then((respostaTempMax) => {
      this.tempMax = respostaTempMax;
      if (this.tempMax == null) {
        this.tempMax = 200;
      }
      console.log("Temperatura Máxima recuperada (Storage) => ", this.tempMax)
    })
    this.storage.get('tempMin').then((respostaTempMin) => {
      this.tempMin = respostaTempMin;
      if (this.tempMin == null) {
        this.tempMin = 0;
      }
      console.log("Temperatura Mínima recuperada (Storage) => ", this.tempMin)
    })
    console.log("this.temperatura = ", this.temperatura);
  }

  carregarTemperatura() {
    this.storage.get('temperatura1').then(res => {
      this.temperatura = res;
      if (res > 0) {
        this.mostrarTemperatura = true;
        // this.exibirTemperatura();
      } else {
        this.mostrarTemperatura = false;
      }
      console.log("Possui termômetro? ", this.mostrarTemperatura);
      console.log("Abrindo home.html, temperatura: ", res);
    });
  }

  alertaAvisoUnir() {
    let opcoesAlerta = {
      title: 'Aviso',
      message: 'As luminárias foram unidas. Para uma melhor experiência com esta funcionalidade do aplicativo Aurora, por favor, certifique-se de que todas as luminárias selecionadas estejam devidamente ligadas e conectadas à rede WiFi.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Aviso ciente');
          }
        },
      ]
    };
    let alert = this.alertCtrlr.create(opcoesAlerta);
    alert.present();
  }

  alertaSelecionarLuminarias() {
    let opcoesAlerta = {
      cssClass: 'alertCustomCss',
      title: "Selecionar Luminárias",
      message: "Por favor, selecione as luminárias que deseja unir ou separar. ",
      enableBackdropDismiss: false,
      inputs: [],
      buttons: [
        {
          cssClass: 'alertCustomCssButtons',
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelando a escolha');
          }
        },
        {
          cssClass: 'alertCustomCssButtons',
          text: 'OK',
          handler: (dados) => {
            console.log("dados = ", dados)
            this.sincLuminarias(dados);
          }
        }
      ]
    }

    for (let i = 0; i < this.nomesLuminarias.length; i++) {
      // opcoesAlerta.inputs.push({ name: this.nomesLuminarias[i].alias, value: { ix: i, nome: this.nomesLuminarias[i].name }, label: this.nomesLuminarias[i].alias, type: 'checkbox', checked: this.nomesLuminarias[i].link, disabled: false });

      if (this.nomesLuminarias[i].alias == this.nome) {
        opcoesAlerta.inputs.push({ name: this.nomesLuminarias[i].alias, value: { ix: i, nome: this.nomesLuminarias[i].name }, label: this.nomesLuminarias[i].alias, type: 'checkbox', checked: true, disabled: true });
      } else {
        opcoesAlerta.inputs.push({ name: this.nomesLuminarias[i].alias, value: { ix: i, nome: this.nomesLuminarias[i].name }, label: this.nomesLuminarias[i].alias, type: 'checkbox', checked: this.nomesLuminarias[i].link, disabled: false });
      }
    }
    console.log("(Alert) this.nomesLuminarias: ", this.nomesLuminarias)
    console.log("(Alert) opcoesAlerta.inputs: ", opcoesAlerta.inputs)
    let alert = this.alertCtrlr.create(opcoesAlerta);
    alert.present();
  }

  abrirAjuda(slide) {
    // console.log("abrirAjuda(" + slide + ")")
    this.modal = this.modalCtrl.create(AboutPage, { slide: slide });
    this.modal.present();
  }

  exibirTemperatura() {
    console.log("ionViewWillEnter() => Mostrar Temperatura", this.mostrarTemperatura);
    // if (this.mostrarTemperatura) {
    console.log("Timer INICIADO!");
    this.value.timer = setInterval(() => {

      this.verificarTemperatura();

    }, 5000);
    // }
  }

  verificarTemperatura() {
    // this.resposta = this.homeProvider.getTemperatura()
    let loading = this.loader.create({
      content: "Obtendo Informações...",
    });
    // ///////////////////////////
    // if (!this.appCarregado) {
    //   loading.present();
    // }
    // ///////////////////////////
    this.resposta = this.homeProvider.gethomeScreen()
      .subscribe(
      data => {
        let dados = {};
        dados = JSON.parse(JSON.stringify(data));
        console.log("dados: ", dados)
        this.timer = dados['timer'];
        this.temperatura = dados['temp'];
        this.presetAtual = dados['name'];
        // console.log("###this.graficos### = ", this.graficos)
        // console.log("###this.parametrosGrafico### = ", this.parametrosGrafico.valorCanal)
        // console.log("###this.graficos[0].parametrosGrafico.valorCanal### = ", this.graficos[0].parametrosGrafico.valorCanal)
        // console.log("##this.graficos[i].parametrosGrafico.valorCanal## = ", (dados['led' + (1) + 'Tensao'] / 1020) * 100)

        for (let i = 0; i < this.canaisLuminaria; i++) {
          this.graficos[i].valorCanal = (dados['led' + (i + 1) + 'Tensao'] / 1020) * 100;
        }
        this.proxHorario = this.util.convertMilisegundosHora(dados['hrFinal']);
        console.log("Timer = ", this.timer)
        console.log("Temperatura = ", this.temperatura)
        console.log("Name = ", this.presetAtual)

        console.log("Próximo ajuste: ", this.proxHorario)
        this.storage.set("temperatura1", this.temperatura);
        console.log("Sucesso!")
        // ////////////////////////////
        // if (!this.appCarregado) {
        //   loading.dismiss();
        //   this.appCarregado = true;
        //   console.log("loading.dismiss() Home.ts")
        // }
        // ////////////////////////////
        console.log(this.temperatura)
        this.contadorFalhas = 3;
      },
      err => {
        if (this.contadorFalhas == 0) {
          this.temperatura = "FALHA...";
          console.log("Erro!");
          console.log(err);
          if (this.mostrarTemperatura) {
            if (this.mostrarAlerta) {
              this.alertaFalhaComunicacaoHome();
              this.mostrarAlerta = false;
            }
          }
          this.contadorFalhas = 3;
          clearInterval(this.value.timer);
          console.log("Timer CANCELADO! (FALHA...)");
        } else {
          this.contadorFalhas--;
          console.log("this.contadorFalhas = ", this.contadorFalhas);
        }
        ///////////////////////////
        if (!this.appCarregado) {
          loading.dismiss();
          this.appCarregado = true;
        }
        ///////////////////////////
      },
      () => console.log('GET Request completo'));
  }

  sincLuminarias(dados) {
    console.log("dados.length 1 sincLuminarias home.ts = ", dados.length)
    let dadosListaUniao = [];
    for (let i = 0; i < dados.length; i++) {
      dadosListaUniao[i] = dados[i];
      console.log("Inserindo dados[" + i + "] em dadosListaUniao em home.ts ::", dados[i])
    }
    console.log("dadosListaUniao = dados home.ts = ", dadosListaUniao)
    // for (let i = 0; i < dados.length; i++) {
    //   if (i > ((dados.lenght / 2) - 1)) {
    //     console.log("Removendo de dados[" + i + "] de dados[] em home.ts ::", dados[i])
    //     dados.splice(i, 1);
    //   }
    // }
    // console.log("Dados :: dados.splice(i, 1) home.ts = ", dados)
    this.loading = this.loader.create({
      content: "Sincronizando...",
    });
    this.loading.present().then(() => {
      // this.listaUniao = dados;
      // this.storage.set("listaUniao", this.listaUniao).then(() => {
      console.log("Lista de luminarias unidas criada (dadosListaUniao)! home.ts", dadosListaUniao)
      for (let i = 0; i < this.nomesLuminarias.length; i++) {
        this.nomesLuminarias[i].link = false;
        console.log("Luminária: " + this.nomesLuminarias[i].nome + ", link: " + this.nomesLuminarias[i].link)
      }
      for (let i = 0; i < (dadosListaUniao.length); i++) {
        this.nomesLuminarias[dadosListaUniao[i].ix].link = true;
        console.log("dados[" + i + "] = ", dadosListaUniao[i])
        console.log("this.nomesLuminarias[dados[" + i + "].ix].link = ", this.nomesLuminarias[dadosListaUniao[i].ix].link)
      }
      this.listaUniao = dadosListaUniao;
      this.publicarNomesLuminariasMenu(this.nomesLuminarias);
      this.publicarListaUniao(this.listaUniao);
      console.log("Sincronizar Luminarias this.listaUniao = ", this.listaUniao)
      // })
      // console.log("this.listaUniao = ", this.listaUniao);
      this.storage.set("nomesLuminarias", this.nomesLuminarias).then(() => {
        console.log("nomesLuminarias guardado (Storage) = ", this.nomesLuminarias)
      })
      if (dados.length > 1) {
        console.log("dados.length 2 sincLuminarias home.ts = ", dados.length)
        this.publicarSincLink(true);
        this.luminariaProvider.getConfig().then(dadosJSON => {
          for (let i = 0; i < dados.length; i++) {
            console.log("dadosJSON['agendador'][" + i + "] (HomePAge)= ", dadosJSON['agendador'])
            console.log("dados[" + i + "].ix = ", dados[i].ix)
            this.presetsProvider.setTemporizadorJSONMulti(dadosJSON['agendador'], dados[i].ix).then(res => {
              console.log("Resposta this.presetsProvider.setTemporizadorJSONMulti(dadosJSON['agendador'], dados[" + i + "].ix) = ", res)
            }).catch(erro => {
              console.error("Resposta this.presetsProvider.setTemporizadorJSONMulti(dadosJSON['agendador'], dados[" + i + "].ix) = ", erro)
            });
            for (let j = 0; j < dadosJSON['presets']; j++) {
              this.luminariaProvider.setRegulagemJSONMulti(dadosJSON['presets'][j], dados[i].ix).then(res => {
                console.log("Resposta this.luminariaProvider.setRegulagemJSONMulti([dadosJSON['presets'][" + j + "]], dados[" + i + "].ix) = ", res)
              }).catch(erro => {
                console.log("Resposta this.luminariaProvider.setRegulagemJSONMulti([dadosJSON['presets'][" + j + "]], dados[" + i + "].ix) = ", erro)
              });
            }
          }
        });
      }
      else {
        this.publicarSincLink(false);
        for (let i = 0; i < this.nomesLuminarias.length; i++) {
          this.nomesLuminarias[i].link = false;
        }
        console.log("this.nomesLuminarias :: ", this.nomesLuminarias)
        this.publicarNomesLuminariasMenu(this.nomesLuminarias);
      }
      this.loading.dismiss();
    });
    // this.alertaAvisoUnir();
  }

  ligarDesligarLuminaria() {
    console.log("ligarDesligarLuminaria(){}")
    console.log("this.listaUniao = ", this.listaUniao)
    this.ligarLuminaria = !this.ligarLuminaria;
    this.publicarLigarLuminaria(this.ligarLuminaria);
    let dados = { power: this.ligarLuminaria == true ? 1 : 0 };
    if (this.link) {
      let index = this.nomesLuminarias.findIndex((i => i.alias === this.nome));
      for (let i = 0; i < this.listaUniao.length; i++) {
        this.homeProvider.setPowerMulti(dados, this.listaUniao[i].ix).then(res => {
          console.log("ligarDesligarLuminaria (Multi) ", res)
        }).catch(err => {
          this.ligarLuminaria = !this.ligarLuminaria;
          console.error("Erro ao ligar a luminária", err)
        })
      }
    } else {
      this.homeProvider.setPower(dados).then(res => {
        console.log("ligarDesligarLuminaria", res)
      }).catch(err => {
        this.ligarLuminaria = !this.ligarLuminaria;
        console.error("Erro ao ligar a luminária", err)
      })
    }

  }

  publicarNomesLuminariasMenu(nomesLuminarias) {
    this.events.publish('nomes', nomesLuminarias);
    console.log("Tentando mudar o nome da variavel em app.components.ts...");
    // this.loading.dismiss();
  }

  publicarSincLink(link) {
    this.link = link;
    this.storage.set("linkHomePage", link).then(() => {
      console.log("Link guardado (Storage)!", link)
    })
    this.events.publish('link', link);
    console.log("Tentando mudar o valor booleano (link) da variavel em luminaria.ts...", link);
    // this.loading.dismiss();
  }

  publicarListaUniao(listaUniao) {
    this.events.publish('listaUniao', listaUniao);
    console.log("Tentando mudar o valor booleano (listaUniao) da variavel em luminaria.ts...", listaUniao);
    // this.loading.dismiss();
  }


  publicarLigarLuminaria(ligarLuminaria) {
    this.events.publish('ligarLuminaria', ligarLuminaria);
    console.log("Tentando mudar o nome da variavel power em luminaria.ts...");
    this.storage.set("power", ligarLuminaria).then(() => {
      console.log("Valor da luminária guardado (Storage)", ligarLuminaria)
    })
  }

  // mudarIP() {
  //   this.storage.set("url", "127.0.0.1")//Grava na memória o valor acima para futuras execuções do APP
  //     .then(res => {
  //       console.log("Numero IP alterado.");
  //     });
  // }

  /*
    testeConverterHora(){
      console.log(this.util.convertHoraMilisegundos("08:30"));
    }
  
    testeConverterMs(){
      console.log(this.util.convertMilisegundossHora("30600000"));
    }
    */
}
