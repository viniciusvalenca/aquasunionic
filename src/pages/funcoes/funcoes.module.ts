import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuncoesPage } from './funcoes';

@NgModule({
  declarations: [
    FuncoesPage,
  ],
  imports: [
    IonicPageModule.forChild(FuncoesPage),
  ],
})
export class FuncoesPageModule {}
