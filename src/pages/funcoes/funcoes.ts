import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EfeitosPage } from '../efeitos/efeitos';
import { TwinstarPage } from '../twinstar/twinstar';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-funcoes',
  templateUrl: 'funcoes.html',
})
export class FuncoesPage {

  twinstar: boolean;
  uv: boolean;
  feeder: boolean;
  sensorPh: boolean;
  sensorNivel: boolean;
  luminaria: any;

  constructor(public navCtrlr: NavController, public navParams: NavParams, public storage: Storage) {
    this.carregarOpcoesMenu();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FuncoesPage');
  }

  carregarOpcoesMenu() {
    /////////////////////LUMINARIA////////////////////////////////
    this.storage.get('canaisLuminaria')
    .then((resp) => {
      this.luminaria = resp;
      console.log("LUMINARIA resgatado (Storage) => ", resp);
    })
    /////////////////////TWINSTAR////////////////////////////////
    this.storage.get('twinstar')
      .then((resp) => {
        this.twinstar = resp;
        console.log("TWINSTAR resgatado (Storage) => ", resp);
      })
    /////////////////////CAMARAUV////////////////////////////////
    this.storage.get('uv')
      .then((resp) => {
        this.uv = resp;
        console.log("CAMARAUV resgatado (Storage) => ", resp);
      })
    /////////////////////FEEDER////////////////////////////////
    this.storage.get('feeder')
      .then((resp) => {
        this.feeder = resp;
        console.log("FEEDER resgatado (Storage) => ", resp);
      })
    /////////////////////SENSORPH////////////////////////////////
    this.storage.get('sensorPh')
      .then((resp) => {
        this.sensorPh = resp;
        console.log("SENSORPH resgatado (Storage) => ", resp);
      })
    /////////////////////SENSORNIVEL////////////////////////////////
    this.storage.get('sensorNivel')
      .then((resp) => {
        this.sensorNivel = resp;
        console.log("SENSORNIVEL resgatado (Storage) => ", resp);
      })
  }

  abrirPaginaEfeitos() {
    this.navCtrlr.push(EfeitosPage);
  }

  abrirPaginaTwinStar() {
    this.navCtrlr.push(TwinstarPage);
  }

  abrirPaginaAlimentador() { }

  abrirPaginaCamaraUV() { }

  abrirPaginaSensorPh(){ }

  abrirPaginaSensorNivel(){ }

}
