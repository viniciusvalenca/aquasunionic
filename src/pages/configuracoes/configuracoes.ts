import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ConfiguracoesProvider } from '../../providers/configuracoes/configuracoes';
import { Storage } from '@ionic/storage';
import { ReiniciarappPage } from '../reiniciarapp/reiniciarapp';
import { App } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { FusoHoraPage } from '../fuso-hora/fuso-hora';
import { AlarmeTemperaturaPage } from '../alarme-temperatura/alarme-temperatura';
import { ResetFabricaPage } from '../reset-fabrica/reset-fabrica';
import { ProcurarLuminariasPage } from '../procurar-luminarias/procurar-luminarias';
import { InfoLuminariaPage } from '../info-luminaria/info-luminaria';

@IonicPage()
@Component({
  selector: 'page-configuracoes',
  templateUrl: 'configuracoes.html',
})
export class ConfiguracoesPage {

  mostrarTemperatura: any;

  constructor(public navCtrlr: NavController, public navParams: NavParams,
    private configuracoesProvider: ConfiguracoesProvider, public alertCtrlr: AlertController,
    public loader: LoadingController, private storage: Storage, public app: App,
    private toast: ToastController) {

  }

  ionViewWillEnter() {
    this.verificarTemperatura();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfiguracoesPage');
  }
  verificarTemperatura() {
    this.storage.get('temperatura1').then(res => {
      if (res > 0) {
        this.mostrarTemperatura = true;
      } else {
        this.mostrarTemperatura = false;
      }
      console.log("Possui termômetro? ", this.mostrarTemperatura);
      console.log("Abrindo configuracoes.html, temperatura: ", res);
    });
  }

  abrirPaginaFusoHora() {
    this.navCtrlr.push(FusoHoraPage);
  }

  abrirPaginaAlertas() {
    this.navCtrlr.push(AlarmeTemperaturaPage);
  }

  abrirPaginaRestaurar() {
    this.navCtrlr.push(ResetFabricaPage);
  }

  abrirPaginaInfoLuminaria() {
    this.navCtrlr.push(InfoLuminariaPage);    
  }

  abrirPaginaProcurarLuminarias() {
    this.navCtrlr.push(ProcurarLuminariasPage);    
  }
}
