import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class LoadersProvider {

  constructor(public http: HttpClient, public loadingCtrl: LoadingController) {
    console.log('Hello LoadersProvider Provider');
  }
  carregando: any = this.loadingCtrl.create({
    content: "Carregando..."
  })

  show() {
    this.carregando.present();
  }

  hide() {
    this.carregando.dismiss();
  }

}
