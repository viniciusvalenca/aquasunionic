import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class ConfiguracoesProvider {

  constructor(public httpClient: HttpClient, private value: ValuesProvider) {
    console.log('Hello ConfiguracoesProvider Provider');
  }
}
