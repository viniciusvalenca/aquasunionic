import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class HomeProvider {

  constructor(public httpClient: HttpClient, public http: HttpClient, public value: ValuesProvider) {
    console.log('Hello HomeProvider Provider');
  }

  getTemperatura() {
    console.log("this.value.url: ", this.value.url);
    return this.httpClient.get(this.value.url + "getTemperatura", { responseType: 'text' });
    // return this.httpClient.get("http://172.20.10.3:8100/proxy/getTemperatura", { responseType: 'text' })
  }

  gethomeScreen() {    
    return this.httpClient.get(this.value.url + "homeScreen", {});
  }

  getTemperaturaInicial() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "getTemperatura", { responseType: 'text' })
        // this.httpClient.get("http://172.20.10.3:8100/proxy/getTemperatura")
        .subscribe(res => {
          console.log("Resposta GET /getTemperatura: ", res);
          resolve(res);
        }, (err) => {
          console.log("Resposta ERRO /getTemperatura: ", err);
          reject(err);
        });
    });
  }


  setPower(dados){
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setPower', JSON.stringify(dados), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setPreset", JSON.stringify(dados), { headers: headers })        
        .subscribe(res => {
          console.log("Resposta setPower Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setPower Provider: ", err);
          reject(err);
        });
    });
  }

  setPowerMulti(dados, urlIndex){
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post('http://' + this.value.urls[urlIndex].url + '/setPower', JSON.stringify(dados), { headers: headers })
      .timeout(2000)
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setPreset", JSON.stringify(dados), { headers: headers })        
        .subscribe(res => {
          console.log("Resposta setPower Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setPower Provider: ", err);
          reject(err);
        });
    });
  }
  // getTemperaturaAdv() { 
  //   let headers = new Headers();
  //   headers.append('content-type', 'application/json;charset=UTF-8');
  //   headers.append('Access-Control-Allow-Origin', '*');
  //   console.log("this.value.url: ", this.value.url);
  //   let option = new RequestOptions ({ headers: headers })
  //   return this.http.get(this.value.url + "getTemperatura", );
  // }
}