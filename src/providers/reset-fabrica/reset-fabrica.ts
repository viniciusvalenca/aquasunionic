import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';
import 'rxjs/add/operator/timeout';

@Injectable()
export class ResetFabricaProvider {

  constructor(public httpClient: HttpClient, private value: ValuesProvider) {
    console.log('Hello ResetFabricaProvider Provider');
  }

  getReset(url) {
    return new Promise((resolve, reject) => {
      this.httpClient.get("http://" + url + "/" + "resetDefault", { responseType: 'text' })
      .timeout(5000)
        .subscribe(res => {
          console.log("Resposta GET /resetDefautls:", res);
          resolve(res);
        }, (err) => {
          console.log("Resposta ERRO /resetDefautls:", err);
          reject(err);
        });
    });
  }
}