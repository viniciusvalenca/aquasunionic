import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';
import 'rxjs/add/operator/map';

@Injectable()
export class TwinstarProvider {

  constructor(public httpClient: HttpClient, public value: ValuesProvider) {
    console.log('Hello LuminariaProvider Provider');
  }

  setTwinStar(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return new Promise((resolve, reject) => {      
      this.httpClient.post(this.value.url + 'setTwinStar', JSON.stringify(dados), { headers: headers })       
        .subscribe(res => {
          console.log("Enviado: ", dados)
          console.log("Resposta setTwinStar Provider: ", res)
          resolve(res);
        }, (err) => {
          reject(err);
          console.error("Erro setTwinStar Provider: ", err)
        });
    });
  }
}