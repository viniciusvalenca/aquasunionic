import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';
import 'rxjs/add/operator/timeout';
// import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class ProcurarLuminariasProvider {

  constructor(public httpClient: HttpClient, public value: ValuesProvider, public http: HttpClient) {
    console.log('Hello MdnsProvider Provider');
  }

  getNome() {
    return this.httpClient.get(this.value.url, {});   
  }

  getDadosLuminariaNome(nomeMDNS) {
    console.log("getDadosLuminariaNome(nomeMDNS)");
    ///////////////PROXY//////////////////////////
    if (this.value.proxy) {
      nomeMDNS = this.value.proxyUrl.slice(7, - 1);
      console.log("Proxy Url: ", this.value.proxyUrl);
      console.log("Proxy: ", nomeMDNS);
    }
    ///////////////PROXY//////////////////////////

    return new Promise((resolveName, rejectName) => {
      this.httpClient.get("http://" + nomeMDNS + "/getAgendador").timeout(3000)
        .subscribe(res => {
          resolveName(res);
          console.log("GET nomeMDNS OK! ", nomeMDNS);
        }, (err) => {
          console.log("GET nomeMDNS ERRO!", nomeMDNS);
          rejectName(err);
        });
    });
  }

  getDadosLuminariaIP(ipMDNS) {
    console.log("getDadosLuminariaNome(ipMDNS)");
    ///////////////PROXY//////////////////////////
    if (this.value.proxy) {
      ipMDNS = this.value.proxyUrl.slice(7, - 1);
      console.log("Proxy Url: ", this.value.proxyUrl);
      console.log("Proxy: ", ipMDNS);
    }
    ///////////////PROXY//////////////////////////

    return new Promise((resolveIP, rejectIP) => {
      this.httpClient.get("http://" + ipMDNS + "/getAgendador")       
        .subscribe(res => {
          resolveIP(res);
          console.log("GET ipMDNS OK! ", ipMDNS);
        }, (err) => {
          rejectIP(err);
          console.log("GET ipMDNS ERRO!", ipMDNS);
        });
    });
  }
}
