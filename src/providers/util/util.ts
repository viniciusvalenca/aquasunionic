import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UtilProvider {

  constructor(public http: HttpClient) {
    console.log('Hello UtilProvider Provider');
  }

  convertHoraMilisegundos(hora) {
    let hr = 0;
    let min = 0;
    let tempoMilisegundos;
    if(hora != null){
      tempoMilisegundos = hora.split(':');
    hr = tempoMilisegundos[0] * 60 * 60 * 1000;
    min = tempoMilisegundos[1] * 60 * 1000;
    tempoMilisegundos = hr + min;
    }else{
      tempoMilisegundos = hora;
    }
    return tempoMilisegundos;
  }

  convertMinSegMilisegundos(hora) {
    let hr = 0;
    let min = 0;
    let seg = 0;
    let tempoMilisegundos;
    if(hora != null){
      tempoMilisegundos = hora.split(':');
    //hr = tempoMilisegundos[0] * 60 * 60 * 1000;
    min = tempoMilisegundos[0] * 60 * 1000;
    seg = tempoMilisegundos[1] * 1000;
    tempoMilisegundos = min + seg;
    }else{
      tempoMilisegundos = hora;
    }
    return tempoMilisegundos;
  }

  convertHoraMinSegMilisegundos(hora) {
    let hr = 0;
    let min = 0;
    let seg = 0;
    let tempoMilisegundos;
    if(hora != null){
      tempoMilisegundos = hora.split(':');
    hr = tempoMilisegundos[0] * 60 * 60 * 1000;
    min = tempoMilisegundos[1] * 60 * 1000;
    seg = tempoMilisegundos[2] * 1000;
    tempoMilisegundos = hr + min + seg;
    }else{
      tempoMilisegundos = hora;
    }
    return tempoMilisegundos;
  }

  convertMilisegundosHoraMinSeg(ms) {
    let milisegundos = Math.floor(((ms % 1000) / 100))
      , segundos = Math.floor(((ms / 1000) % 60))
      , minutos = Math.floor(((ms / (1000 * 60)) % 60))
      , horas = Math.floor(((ms / (1000 * 60 * 60)) % 24));

    let hr = (horas < 10) ? "0" + horas : horas;
    let min = (minutos < 10) ? "0" + minutos : minutos;
    let sec = (segundos < 10) ? "0" + segundos : segundos;

    return hr + ":" + min + ":" + sec;
  } 

  convertMilisegundosHora(ms) {
    let milisegundos = Math.floor(((ms % 1000) / 100))
      , segundos = Math.floor(((ms / 1000) % 60))
      , minutos = Math.floor(((ms / (1000 * 60)) % 60))
      , horas = Math.floor(((ms / (1000 * 60 * 60)) % 24));

    let hr = (horas < 10) ? "0" + horas : horas;
    let min = (minutos < 10) ? "0" + minutos : minutos;
    //let sec = (segundos < 10) ? "0" + segundos : segundos;

    return hr + ":" + min;
  }

  convertMilisegundosMinutos(ms) {
    let milisegundos = Math.floor(((ms % 1000) / 100))
      , segundos = Math.floor(((ms / 1000) % 60))
      , minutos = Math.floor(((ms / (1000 * 60)) % 60))

    let min = (minutos < 10) ? "0" + minutos : minutos;
    //let sec = (segundos < 10) ? "0" + segundos : segundos;

    return min;
  }

  convertMilisegundosMinSeg(ms) {
    let milisegundos = Math.floor(((ms % 1000) / 100))
      , segundos = Math.floor(((ms / 1000) % 60))
      , minutos = Math.floor(((ms / (1000 * 60)) % 60))

    let min = (minutos < 10) ? "0" + minutos : minutos;
    let sec = (segundos < 10) ? "0" + segundos : segundos;

    return min + ":" + sec;
  }

  convertMilisegundosSegundos(ms) {
    let milisegundos = Math.floor(((ms % 1000) / 100))
      , segundos = Math.floor(((ms / 1000) % 60))
      , minutos = Math.floor(((ms / (1000 * 60)) % 60))

    // let min = (minutos < 10) ? "0" + minutos : minutos;
    let sec = (segundos < 10) ? "00:0" + segundos : "00:" + segundos;

    return sec;
  }
}