import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { App, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ResetFabricaProvider } from '../reset-fabrica/reset-fabrica';
import { ProcurarLuminariasPage } from '../../pages/procurar-luminarias/procurar-luminarias';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@Injectable()
export class ResetValoresProvider {

  loading: any;

  unregisterBackButtonAction: any;

  constructor(public alertCtrlr: AlertController,
    public loader: LoadingController, private storage: Storage, private toast: ToastController,
    private resetFabricaProvider: ResetFabricaProvider, private app: App) {

    console.log('Hello ResetValoresProvider Provider');
  }

  confirmacaoReset() {
    let confirm = this.alertCtrlr.create({
      title: 'Tem certeza?',
      message: 'Ao confirmar, o aplicativo voltará ao modo de busca de dispositivos e a luminária voltará ao modo de Reconfiguração conforme descrito no manual.',
      buttons: [
        {
          text: 'NÃO',
          handler: () => {
            console.log('Reset Abortado');
          }
        },
        {
          text: 'SIM',
          handler: () => {
            this.resetFabrica();
            console.log('Reset Efetuado');
          }
        }
      ]
    });
    confirm.present();
  }

  alertaFalhaComunicacao(ip) {
    let alert = this.alertCtrlr.create({
      title: "Falha de comunicação",
      message: "Não foi possível reiniciar a luminária de endereço: " + ip + ".",
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('Alerta Falha Reset');
          }
        },
      ]
    });
    alert.present();
  }

  resetFabrica() {
    this.loading = this.loader.create({
      content: "Reiniciando APP Aurora...",
    });
    this.loading.present()
      .then(() => {
        this.storage.get("urls").then(res0 => {
          console.log("getReset(): res0(urls) = ", res0);
          let urls = [];
          urls = res0;
          if (res0 != null) {
            for (let i = 0; i < urls.length; i++) {
              this.resetFabricaProvider.getReset(res0[i].url)
                .then(res => {
                  console.log("getReset(): res0[" + i + "].url = ", res0[i].url);
                  console.log("Resetar Configurações: ", res);
                }).catch(err => {
                  console.error("getReset(): res0[" + i + "].url = ", res0[i].url);
                  console.error("Erro ao resetar configurações", err);
                  // this.alertaFalhaComunicacao(res0[i].url);
                  return;
                });
            }
            this.zerarAtributos(true);//navctrl.push aqui dentro...
          }
        })
      });
  }

  zerarAtributos(luminaria) {
    if (luminaria) {
      this.storage.clear().then(res => {
        this.storage.set("urls", null).then(respUrls => {
          console.log("Storage: urls = null OK!", respUrls);
        });
        console.log("Storage apagado. -> ProcurarLuminariasPage:: Resposta: ", res);
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
        this.app.getRootNav().setRoot(ProcurarLuminariasPage);
      })
    } else {      
      this.storage.clear();
      console.log("Storage apagado.");
    }
    this.loading.dismiss();
  }
  // zerarAtributos() {
  //   this.storage.set("firstRun", 0).then(respfr => {
  //     console.log("Storage: FirstRun = 0 OK!", respfr);
  //     this.storage.set("url", "127.0.0.1").then(respURL => {
  //       console.log("Storage: url = 127.0.0.1 OK!", respURL);
  //       this.storage.set("auth", 0).then(respAuth => {
  //         console.log("Storage: auth = 0 OK!", respAuth);
  //         this.storage.set("auths", null).then(respAuths => {
  //           console.log("Storage: auths = null OK!", respAuths);
  //           this.storage.set("nomesLuminarias", null).then(nomesLuminarias => {
  //             console.log("Storage: nomesLuminarias = null OK!", nomesLuminarias);
  //             this.storage.set("nomeLuminaria", null).then(nomeLuminaria => {
  //               console.log("Storage: nomeLuminaria = null OK!", nomeLuminaria);
  //               this.storage.set("urls", null).then(respUrls => {
  //                 console.log("Storage: urls = null OK!", respUrls);
  //                 this.loading.dismiss();
  //                 this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  //                 this.app.getRootNav().setRoot(ProcurarLuminariasPage);
  //                 // this.app.getRootNav().setRoot(ReiniciarappPage);
  //                 // this.navCtrlr.setRoot(ReiniciarappPage);
  //                 // this.navCtrl.setRoot(ReiniciarappPage);
  //               }).catch(errUrls => {
  //                 console.error("Storage: urls = 0 ERRO!", errUrls);
  //                 this.loading.dismiss();
  //               });
  //             }).catch(nomeLuminaria => {
  //               console.error("Storage: nomeLuminaria = 0 ERRO!", nomeLuminaria);
  //             });
  //           }).catch(errLuminarias => {
  //             console.error("Storage: nomesLuminarias = 0 ERRO!", errLuminarias);
  //             this.loading.dismiss();
  //           });
  //         }).catch(errAuths => {
  //           console.error("Storage: auths = null ERRO!", errAuths);
  //           this.loading.dismiss();
  //         });
  //       }).catch(errAuth => {
  //         console.error("Storage: auth = 0 ERRO!", errAuth);
  //         this.loading.dismiss();
  //       });
  //     }).catch(errURL => {
  //       console.error("Storage: url = 127.0.0.1 ERRO!", errURL);
  //       this.loading.dismiss();
  //     });
  //   }).catch(errfr => {
  //     console.error("Storage: FirstRun = 0 ERRO!", errfr);
  //     this.loading.dismiss();
  //   });
  // }

  // zerarAtributosSomente() {
  //   this.storage.set("firstRun", 0).then(respfr => {
  //     console.log("Storage: FirstRun = 0 OK!", respfr);
  //     this.storage.set("url", "127.0.0.1").then(respURL => {
  //       console.log("Storage: url = 127.0.0.1 OK!", respURL);
  //       this.storage.set("auth", 0).then(respAuth => {
  //         console.log("Storage: auth = null OK!", respAuth);
  //         this.storage.set("auths", null).then(respAuths => {
  //           console.log("Storage: auths = null OK!", respAuth);
  //           this.storage.set("nomesLuminarias", null).then(nomesLuminarias => {
  //             this.storage.set("nomeLuminaria", null).then(nomeLuminaria => {
  //               console.log("Storage: nomeLuminaria = null OK!", nomeLuminaria);
  //               this.storage.set("urls", null).then(respUrls => {
  //                 console.log("Storage: urls = null OK!", respUrls);
  //                 // this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  //                 // this.app.getRootNav().setRoot(ProcurarLuminariasPage);
  //                 // this.app.getRootNav().setRoot(ReiniciarappPage);
  //                 // this.navCtrlr.setRoot(ReiniciarappPage);
  //                 // this.navCtrl.setRoot(ReiniciarappPage);
  //               }).catch(errUrls => {
  //                 console.error("Storage: urls = 0 ERRO!", errUrls);
  //               });
  //             }).catch(nomeLuminaria => {
  //               console.error("Storage: nomeLuminaria = 0 ERRO!", nomeLuminaria);
  //             });
  //           }).catch(errLuminarias => {
  //             console.error("Storage: nomesLuminarias = 0 ERRO!", errLuminarias);
  //           });
  //         }).catch(errAuths => {
  //           console.error("Storage: auths = null ERRO!", errAuths);
  //         });
  //       }).catch(errAuth => {
  //         console.error("Storage: auth = 0 ERRO!", errAuth);
  //       });
  //     }).catch(errURL => {
  //       console.error("Storage: url = 127.0.0.1 ERRO!", errURL);
  //     });
  //   }).catch(errfr => {
  //     console.error("Storage: FirstRun = 0 ERRO!", errfr);
  //   });
  // }

  // zerarAtributosSemAutenticacao() {
  // this.storage.set("presets", null).then(resPresets => {
  //   console.log("Storage: presets = null OK!", resPresets);
  // }).catch(errfr => {
  //   console.error("Storage: presets = null ERRO!", errfr);
  // });
  // }

}
