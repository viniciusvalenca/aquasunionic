import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class AutenticacaoProvider {

  constructor(public httpClient: HttpClient, private value: ValuesProvider) {
    console.log('Hello AutenticacaoProvider Provider');
  }

  getAutorizacaoJSON(url) {
    ///////////////PROXY//////////////////////////
    if (this.value.proxy) {
      url = this.value.proxyUrl.slice(7, - 1);
      console.log("Proxy Url: ", this.value.proxyUrl);
      console.log("Proxy: ", url);
    }
    ///////////////PROXY//////////////////////////
    return new Promise((resolve, reject) => {
      this.httpClient.get("http://" + url + "/" + "getAutorizacao", {}).timeout(3000)
        .subscribe(res => {
          console.log("getAutorizacaoJSON(url) => http://url/getAutorizacao", "http://" + url + "/" + "getAutorizacao")
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  setAutorizacaoJSON(auth, url) {
    ///////////////PROXY//////////////////////////
    if (this.value.proxy) {
      url = this.value.proxyUrl.slice(7, - 1);
      console.log("Proxy Url: ", this.value.proxyUrl);
      console.log("Proxy: ", url);
    }
    ///////////////PROXY//////////////////////////
    let dadosAuth = { autorizacao: String(auth) }
    let headers = new HttpHeaders();
    headers.set('content-type', 'application/json;charset=UTF-8');
    headers.set('Access-Control-Allow-Origin', '*');
    headers.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.set('Accept', 'application/json');
    headers.set('content-type', 'application/json');
    return new Promise((resolve, reject) => {
      this.httpClient.post("http://" + url + "/" + "setAutorizacao", JSON.stringify(dadosAuth), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setAutorizacao", JSON.stringify(dadosAuth), { headers: headers })
        .subscribe(res => {
          console.log("getAutorizacaoJSON(url) => http://url/getAutorizacao", this.value.url + "setAutorizacao")
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}
