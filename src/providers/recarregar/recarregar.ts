import { App } from "ionic-angular";
import { RecarregarPage } from "../../pages/recarregar/recarregar";
import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';
import { HomeProvider } from '../home/home';
import { LuminariaProvider } from '../luminaria/luminaria';
import { PresetsProvider } from '../presets/presets';
import { UtilProvider } from '../util/util';
import { TabsPage } from '../../pages/tabs/tabs';
import { Storage } from '@ionic/storage';
import { FusoHoraProvider } from "../fuso-hora/fuso-hora";
import { EfeitoTempestadeProvider } from "../efeito-tempestade/efeito-tempestade";

@Injectable()
export class RecarregarProvider {

  canaisLuminaria: number;
  loading: any;
  resposta: any;
  temperatura: any;

  constructor(public app: App, public homeProvider: HomeProvider,
    public luminariaProvider: LuminariaProvider, public efeitoTempestadeProvider: EfeitoTempestadeProvider,
    public presetsProvider: PresetsProvider, public storage: Storage,
    public util: UtilProvider, public loader: LoadingController, public toast: ToastController,
    public fusoHoraProvider: FusoHoraProvider) {

    console.log('Hello RecarregarProvider Provider');

  }

  carregarLuminaria(nomeLuminaria) {
    console.log("recarregar.ts mdns carregarLuminaria(nomeLuminaria) = ", nomeLuminaria)
    // this.gravarQtdeCanaisLuminarias(nomeLuminaria);
    this.carregarTemperatura();
  }

  paginaIndisponivel() {
    this.app.getActiveNav().push(RecarregarPage);
    //this.rootPage = RecarregarPage;    
  }

  // gravarQtdeCanaisLuminarias(nomeLuminaria) {
  //   console.log("Nome da luminária em recarregar.ts = ", nomeLuminaria)
  //   this.canaisLuminaria = Number(String(nomeLuminaria).charAt(2));
  //   console.log("*****Canais da luminária recarregar.ts = ", this.canaisLuminaria);
  //   this.storage.set("canaisLuminaria", this.canaisLuminaria).then(res => {
  //     console.log("Número de canais gravados (Storage): ", res);
  //   })
  // }

  carregarTemperatura() {
    this.loading = this.loader.create({
      content: "Comunicando com a luminária, por favor aguarde.",
    });
    return this.loading.present().then(() => {
      this.resposta = this.homeProvider.getTemperatura()
        .subscribe(data => {
          //this.temperatura = JSON.stringify(data);
          this.temperatura = data;
          console.log("Sucesso!");
          console.log(this.temperatura);
          this.storage.set("temperatura1", this.temperatura)
            .then(res => {
              console.log("Temperatura Inicial guardada!", res);
            }).catch(err => {
              console.error("ERRO!", err);
            })
          // this.loading.dismiss();
          this.carregarConfiguracoes();
        },
        err => {
          //this.pronto = false;         
          this.loading.dismiss();
          this.temperatura = "FALHA...";
          //console.log("Erro pronto!", this.pronto);
          console.error("Erro ao resgatar a Temperatura!", err);
          this.paginaIndisponivel();
          this.toast.create({ message: "Não foi possível obter dados => Termômetro", duration: 4000, position: 'top' }).present();
          return;
        })
    })
    //console.log("Fim do metodo: não retornou");   
  }

  carregarConfiguracoes() {
    this.luminariaProvider.getConfig().then(res => {
      console.log("Configurações gerais da luminária: ", res)
      let dadosJSON = JSON.parse(JSON.stringify(res));
      console.log("UTC app.component.ts => ", res);
      /////////////////////LUMINARIA////////////////////////////////
      this.storage.set('canaisLuminaria', dadosJSON['luminaria'])
        .then((resp) => {
          console.log("CANAISLUMINARIA armazenado (Storage) => ", resp);
        })
      /////////////////////TWINSTAR////////////////////////////////
      this.storage.set('twinstar', dadosJSON['twinstar'] == 1 ? false : true)
        .then((resp) => {
          console.log("TWINSTAR armazenado (Storage) => ", resp);
        })
      /////////////////////CAMARAUV////////////////////////////////
      this.storage.set('uv', dadosJSON['UV'] == 1 ? false : true)
        .then((resp) => {
          console.log("CAMARAUV armazenado (Storage) => ", resp);
        })
      /////////////////////FEEDER////////////////////////////////
      this.storage.set('feeder', dadosJSON['feeder'] == 1 ? false : true)
        .then((resp) => {
          console.log("FEEDER armazenado (Storage) => ", resp);
        })
      /////////////////////SENSORPH////////////////////////////////
      this.storage.set('sensorPh', dadosJSON['sensorPH'] == 1 ? false : true)
        .then((resp) => {
          console.log("SENSORPH armazenado (Storage) => ", resp);
        })
      /////////////////////SENSORNIVEL////////////////////////////////
      this.storage.set('sensorNivel', dadosJSON['sensorNivel'] == 1 ? false : true)
        .then((resp) => {
          console.log("SENSORNIVEL armazenado (Storage) => ", resp);
        })
      /////////////////////UTC////////////////////////////////
      this.storage.set('utc', dadosJSON['utc'])
        .then((resp) => {
          console.log("UTC armazenado (Storage) => ", resp);
        })
      ////////////////ESTADO AGENDADOR/////////////////////       
      this.storage.set("temporizadorLigado", dadosJSON['timer'])
        .then(resp => {
          console.log("TIMER armazenado (Storage) => ", resp);
        })
      /////////////////////POWER////////////////////////////////
      this.storage.set('power', dadosJSON['power'] == 1 ? true : false)
        .then((resp) => {
          console.log("POWER armazenado (Storage) => ", resp);
        })
      ////////////////HORÁRIOS AGENDADOR/////////////////////  
      let presets = [];
      // console.log("dadosJSON['agendador'] = ", dadosJSON['agendador'])
      // for (let i = 0; i < dadosJSON['agendador'].length; i++) {
      for (let i = 0; i < dadosJSON['presets'].length; i++) {
        presets.push(
          {
            nome: dadosJSON['presets'][i].name,
            hora: i < dadosJSON['agendador'].length ? this.util.convertMilisegundosHora(dadosJSON['agendador'][i].hrInicio) : null,
            gradual: i < dadosJSON['agendador'].length ? dadosJSON['agendador'][i].gradual : null,
            led1Tensao: 0,
            led2Tensao: 0,
            led3Tensao: 0,
            led4Tensao: 0,
            led5Tensao: 0,
            led6Tensao: 0,
            led7Tensao: 0,
            led8Tensao: 0,
            led9Tensao: 0,
          })
        console.log("GET Temporizador app.component - Iteração: ", i);
      }
      ////////////////LEDS MANUAL/////////////////////   
      this.canaisLuminaria = Number(dadosJSON['luminaria']);
      console.log("dadosJSON['presets'] recarregar.ts = ", dadosJSON['presets']);
      console.log("dadosJSON['presets'].length recarregar.ts = ", dadosJSON['presets'].length);
      console.log("this.canaisLuminaria em recarregar.ts", this.canaisLuminaria)
      for (let i = 0; i < dadosJSON['presets'].length; i++) {
        for (let j = 0; j < this.canaisLuminaria; j++) {
          presets[i]["led" + (j + 1) + "Tensao"] = dadosJSON['presets'][i]["led" + (j + 1) + "Tensao"];
          console.log(" presets[" + i + "].led" + [j + 1] + "Tensao = ", presets[i]["led" + (j + 1) + "Tensao"])
        }
      }

      this.storage.set('presets', presets)
        .then(res => console.log("gravando presets (storage) recarregar.ts => ", presets))
        .catch(err => console.error("Erro Gravando presets (storage) recarregar.ts => causa: ", err));

      // console.log("this.ledsPresets = ", this.ledsPresets)
      ////////////////EFEITO STORM/////////////////////  
      this.storage.set("storm", dadosJSON["storm"])
        .then(resStorm => {
          console.log("Storm: ", resStorm)
          // this.storage.set("modeStorm", dadosJSON["stormMode"])
          //   .then(resMode => {
          //     console.log("ModeStorm: ", resMode)
          this.app.getRootNav().setRoot(TabsPage);
          this.loading.dismiss();
          // });
        });
      ////////////////LEDS MANUAL/////////////////////
      //Não há necessidade. Os presets são carregados a cada seleção.
    }).catch(err => {
      this.loading.dismiss();
      this.paginaIndisponivel();
      console.error("Erro ao tentar recuperar Informações da luminária em recuperar.ts: ", err);
      return;
    });
  }
}