import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class ValuesProvider {
  url: string;
  urls: Array<any> = [];
  auths: Array<any> = [];
  debug: boolean = true;
  ///////PROXY PARA LIVE RELOAD///////
  proxy: boolean = false; /////////////
  ////////////////////////////////////
  timer: any;
  proxyUrl: string = "http://192.168.0.130:8100/proxy/";//Endereço local da máquina (PC ou Laptop).
  //url:string = "http://aquasundevice/";
  //url:string = "http://172.20.10.3/";
  //url:string = "http://192.168.0.128/";
  //url:string = "http://172.20.10.14/";

  constructor(public http: HttpClient, public storage: Storage) {
    console.log('Hello ValuesProvider Provider');
  }

  setUrl(valUrl) {
    if (this.proxy) {
      this.url = this.proxyUrl;
    } else {
      this.url = "http://" + valUrl + "/";
    }
    this.storage.set("url", valUrl)
      .then(res => {
        console.info("valuesProvider setUrl(): url guardada (Storage)", res);
      })
  }

  setUrls(valUrls) {
    this.urls = valUrls;
    this.storage.set("urls", valUrls)
      .then(res => {
        console.info("valuesProvider setUrls(): urls guardadas (Storage)", res);
      }) 
  }

  addUrl(valUrl) {
    // console.log("addUrl(): valUrl", valUrl);
    if (valUrl != null) {
      this.storage.get("urls")
        .then(res0 => {
          // console.log("addUrl(): res0", res0);
          if (res0 != null) {
            this.urls = res0;
          }
          this.urls.push({ url: valUrl });
          this.storage.set("urls", this.urls)
            .then(res => {
              if (this.debug) {
                console.log("ValuesProvider -> valUrl = ", valUrl);
                console.log("Array urls guardado (Storage): ", this.urls);
              }
            });
        });
    }
  }

  editUrl(valUrl) {
    console.log("editUrl(): valUrl", valUrl);
    if (valUrl != null) {
      this.storage.get("url").then(resUrl => {
        console.log("editUrl(): resUrl (res) (Storage)", resUrl);
        this.storage.get("urls").then(resUrls => {
          console.log("editUrl(): resUrls (res) (Storage)", resUrls);
          if (resUrls.length > 0 || resUrls != null) {
            this.urls = resUrls;
            let index: number = this.urls.findIndex(i => i.url === resUrl);
            console.log("editUrl(): index", index);
            if (index >= 0) {
              this.urls[index].url = valUrl;
              this.storage.set("urls", this.urls)
                .then(res => {
                  if (this.debug) {
                    console.log("ValuesProvider -> valUrl = ", valUrl);
                    console.log("Array urls atualizado (Storage): ", this.urls);
                  }
                });
            } else {
              console.log("Não achou a URL... Não editou...");
            }
          }
        });
      });
    }
  }

  remUrl(valUrl) {
    if (valUrl != null) {
      this.storage.get("urls").then(res => {
        if (res.length > 0 || res != null) {
          this.urls = res;
          let index: number = this.urls.indexOf(valUrl);
          this.urls.splice(index, 1);
          this.storage.set("urls", this.urls)
            .then(res => {
              if (this.debug) {
                console.log("ValuesProvider -> valUrl = ", valUrl);
                console.log("Array urls removido (Storage): ", this.urls);
              }
            });
        }
      })
    }
  }

  addAuth(valAuth) {
    console.log("ValuesProvider -> valAuth = ", valAuth);
    if (valAuth != null) {
      this.storage.get("auths")
        .then(res0 => {
          if (res0 != null) {
            this.auths = res0;
          }
          this.auths.push({ auth: valAuth });
          this.storage.set("auths", this.auths)
            .then(res => {
              if (this.debug) {                
                console.log("Array de auths guardado (Storage): ", this.auths);
              }
            });
        });
    }
  }

  editAuth(valAuth) {
    if (valAuth != null) {
      this.storage.get("auths").then(res => {
        if (res.length > 0 || res != null) {
          this.auths = res;
          let index: number = this.urls.indexOf(valAuth);
          this.auths[index].url = valAuth;
          this.storage.set("auths", this.auths)
            .then(res => {
              if (this.debug) {
                console.log("ValuesProvider -> valAuth = ", valAuth);
                console.log("Array de auths atualizado (Storage): ", this.auths);
              }
            });
        }
      })
    }
  }

  remAuth(valAuth) {
    if (valAuth != null) {
      this.storage.get("auths").then(res => {
        if (res.length > 0 || res != null) {
          this.auths = res;
          let index: number = this.auths.indexOf(valAuth);
          this.auths.splice(index, 1);
          this.storage.set("auths", this.auths)
            .then(res => {
              if (this.debug) {
                console.log("ValuesProvider -> valAuth = ", valAuth);
                console.log("Array de auths removido (Storage): ", this.auths);
              }
            });
        }
      })
    }
  }
}
