import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class PresetsProvider {

  resposta: any;

  constructor(public httpClient: HttpClient, public value: ValuesProvider, public http: HttpClient) {
    console.log('Hello PresetsProvider Provider');
  } 

  getRegulagem(presets, numeroPreset) {
    return this.httpClient.get(this.value.url + "leds", { params: { 
      led1Tensao: presets[numeroPreset - 1].led1Tensao, 
      led2Tensao: presets[numeroPreset - 1].led2Tensao, 
      led3Tensao: presets[numeroPreset - 1].led3Tensao, 
      led4Tensao: presets[numeroPreset - 1].led4Tensao, 
      led5Tensao: presets[numeroPreset - 1].led5Tensao, 
      led6Tensao: presets[numeroPreset - 1].led6Tensao, 
      led7Tensao: presets[numeroPreset - 1].led7Tensao,
      led8Tensao: presets[numeroPreset - 1].led8Tensao, 
      led9Tensao: presets[numeroPreset - 1].led9Tensao,  
    } });
  }

  getRegulagemMulti(presets, numeroPreset, urlIndex) {
    console.log("this.value.urls = ", this.value.urls)
    return this.httpClient.get('http://' + this.value.urls[urlIndex].url + "/leds", { params: { 
      led1Tensao: presets[numeroPreset - 1].led1Tensao, 
      led2Tensao: presets[numeroPreset - 1].led2Tensao, 
      led3Tensao: presets[numeroPreset - 1].led3Tensao, 
      led4Tensao: presets[numeroPreset - 1].led4Tensao, 
      led5Tensao: presets[numeroPreset - 1].led5Tensao, 
      led6Tensao: presets[numeroPreset - 1].led6Tensao, 
      led7Tensao: presets[numeroPreset - 1].led7Tensao,
      led8Tensao: presets[numeroPreset - 1].led8Tensao, 
      led9Tensao: presets[numeroPreset - 1].led9Tensao, 
    } });
  }

  getPreset(preset) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "getPreset", { params: { preset: preset } })
        // this.httpClient.get("http://172.20.10.3:8100/proxy/getPreset", { params: { preset: preset } })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  setRegulagemJSON(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setPreset', JSON.stringify(dados), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setPreset", JSON.stringify(dados), { headers: headers })        
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  setRegulagemJSONMulti(dados, urlIndex) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post('http://' + this.value.urls[urlIndex].url + '/setPreset', JSON.stringify(dados), { headers: headers })
      // this.httpClient.post("http://172.20.10.3:8100/proxy/setPreset", JSON.stringify(dados), { headers: headers })        
        .subscribe(res => {
          console.log("Resposta setRegulagemJSON Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setRegulagemJSON Provider: ", err);
          reject(err);
        });
    });
  }

  setModo(modo, numPreset) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "modo", { params: { setup: modo, preset: 'preset' + numPreset } })
        // this.httpClient.get("http://172.20.10.3:8100/proxy/modo", { params: { setup: modo, preset: 'preset' + numPreset } })        
        .subscribe(res => {
          console.log(res);
          resolve(res);
        }, (err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  setModoMulti(modo, numPreset, urlIndex) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return new Promise((resolve, reject) => {
      this.httpClient.get('http://' + this.value.urls[urlIndex].url + "/modo", { params: { setup: modo, preset: 'preset' + numPreset } })
        // this.httpClient.get("http://172.20.10.3:8100/proxy/modo", { params: { setup: modo, preset: 'preset' + numPreset } })        
        .subscribe(res => {
          console.log(res);
          resolve(res);
        }, (err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  getTemporizador() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "getAgendador", {})
        // this.httpClient.get("http://172.20.10.3:8100/proxy/getAgendador")
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  setTemporizadorJSON(dados) {
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setAgendador', JSON.stringify(dados))
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setAgendador", JSON.stringify(dados))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  setTemporizadorJSONMulti(dados, urlIndex) {
    console.log(dados);
    console.log("")
    return new Promise((resolve, reject) => {
      this.httpClient.post('http://' + this.value.urls[urlIndex].url + '/setAgendador', JSON.stringify(dados))
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setAgendador", JSON.stringify(dados))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}