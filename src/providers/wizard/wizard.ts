import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class WizardProvider {

  constructor(public httpCtrl: HttpClient, public http: Http) {
    console.log('Hello WizardProvider Provider');
  }

  getWifi() {
    return new Promise((resolve, reject) => {
      this.http.get('http://192.168.4.1/wifi/list')
        .map(res => res.json()).subscribe(data => {
          console.log(data);
        },
        err => {
          console.error(err);

        });
    });
  }

  setWifi(wifiSSID, wifiPASS, stealth, hostname) {
    return new Promise((resolve, reject) => {
      this.http.get('http://192.168.4.1/mode/station',
        { params: { 'ssid': wifiSSID, 'password': wifiPASS, 'stealth': stealth, 'hostname': hostname } })
        .subscribe(data => {
          resolve(data);
          console.log(data);
        },
        err => {
          reject(err);
          console.error(err);
        });
    });
  }

  refresh(refresher) {
    return new Promise((resolve, reject) => {
      this.http.get('http://192.168.4.1/wifi/list')
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
          console.log(data);
        },
        err => {
          reject(err);
          console.error(err);
        });
    });
  }

  confirm() {
    return new Promise((resolve, reject) => {
      this.http.get('http://192.168.4.1/wifi/confirm')
        .subscribe(data => {
          resolve(data);
          console.log(data);
        },
        err => {
          reject(err);
          console.error(err);
        });
    });
  }

}