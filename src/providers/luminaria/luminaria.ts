import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';
import 'rxjs/add/operator/map';

@Injectable()
export class LuminariaProvider {

  constructor(public httpClient: HttpClient, public value: ValuesProvider) {
    console.log('Hello LuminariaProvider Provider');
  }

  getRegulagem(presets) {
    return this.httpClient.get(this.value.url + "leds", { params: { 
      led1Tensao: presets[presets.length - 1].led1Tensao, 
      led2Tensao: presets[presets.length - 1].led2Tensao, 
      led3Tensao: presets[presets.length - 1].led3Tensao, 
      led4Tensao: presets[presets.length - 1].led4Tensao, 
      led5Tensao: presets[presets.length - 1].led5Tensao, 
      led6Tensao: presets[presets.length - 1].led6Tensao, 
      led7Tensao: presets[presets.length - 1].led7Tensao,
      led8Tensao: presets[presets.length - 1].led8Tensao, 
      led9Tensao: presets[presets.length - 1].led9Tensao,  
    } });
  }

  getRegulagemMulti(presets, urlIndex) {
    console.log("this.value.urls = ", this.value.urls)
    return this.httpClient.get('http://' + this.value.urls[urlIndex].url + "/leds", { params: { 
      led1Tensao: presets[presets.length - 1].led1Tensao, 
      led2Tensao: presets[presets.length - 1].led2Tensao, 
      led3Tensao: presets[presets.length - 1].led3Tensao, 
      led4Tensao: presets[presets.length - 1].led4Tensao, 
      led5Tensao: presets[presets.length - 1].led5Tensao, 
      led6Tensao: presets[presets.length - 1].led6Tensao, 
      led7Tensao: presets[presets.length - 1].led7Tensao,
      led8Tensao: presets[presets.length - 1].led8Tensao, 
      led9Tensao: presets[presets.length - 1].led9Tensao, 
    } });
  }

  getEstadoTemporizador() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "getTimer", {})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  ///////////////////////CONFIGURAÇÃO GERAL///////////////////
  getConfig() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "config", {})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  ////////////////////////////////////////////////////////////

  setRegulagemJSON(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setPreset', JSON.stringify(dados), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setPreset", JSON.stringify(dados), { headers: headers })        
        .subscribe(res => {
          console.log("Resposta setRegulagemJSON Provider: ", res);
          resolve(res);
        }, (err) => {
          console.error("Erro setRegulagemJSON Provider: ", err);
          reject(err);
        });
    });
  }

  setRegulagemJSONMulti(dados, urlIndex) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post('http://' + this.value.urls[urlIndex].url + '/setPreset', JSON.stringify(dados), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setPreset", JSON.stringify(dados), { headers: headers })        
        .subscribe(res => {
          console.log("Resposta setRegulagemJSON Provider: ", res);
          resolve(res);
        }, (err) => {
          console.error("Erro setRegulagemJSON Provider: ", err);
          reject(err);
        });
    });
  }

  setTimerJSON(timer) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setTimer', JSON.stringify(timer), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setTimer", JSON.stringify(timer), { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  setTimerJSONMulti(timer, urlIndex) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return new Promise((resolve, reject) => {
      this.httpClient.post('http://' + this.value.urls[urlIndex].url + '/setTimer', JSON.stringify(timer), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setTimer", JSON.stringify(timer), { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  setModo(modo) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "modo", { params: { setup: modo, preset: 'presetManual' } })
        // this.httpClient.get("http://172.20.10.3:8100/proxy/modo", { params: { setup: modo, preset: 'presetManual' } })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  setModoMulti(modo, urlIndex) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');
    return new Promise((resolve, reject) => {
      this.httpClient.get('http://' + this.value.urls[urlIndex].url + "/modo", { params: { setup: modo, preset: 'presetManual' } })
        // this.httpClient.get("http://172.20.10.3:8100/proxy/modo", { params: { setup: modo, preset: 'presetManual' } })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}