import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AlarmeTemperaturaProvider {

  constructor(public httpClient: HttpClient, public value: ValuesProvider) {
    console.log('Hello AlarmeTemperaturaProvider Provider');
  }

  setEmail(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setEmail', JSON.stringify(dados), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setEmail", JSON.stringify(dados), { headers: headers })      
        .subscribe(res => {
          console.log("Resposta setEmail Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setEmail Provider: ", err);
          reject(err);
        });
    });
  }

  setTemperaturas(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setTempAlerta', JSON.stringify(dados), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setTempAlerta", JSON.stringify(dados), { headers: headers })     
        .subscribe(res => {
          console.log("Resposta setTemperaturas Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setTemperaturas Provider: ", err);
          reject(err);
        });
    });
  }

  setRegId(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setRegId', JSON.stringify(dados), { headers: headers })
        // this.httpClient.post("http://172.20.10.3:8100/proxy/setRegId", JSON.stringify(dados), { headers: headers })       
        .subscribe(res => {
          console.log("Resposta setRegId Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setRegId Provider: ", err);
          reject(err);
        });
    });
  }

}