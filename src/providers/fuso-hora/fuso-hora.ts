import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ValuesProvider } from '../values/values';

@Injectable()
export class FusoHoraProvider {

  constructor(public httpClient: HttpClient, private value: ValuesProvider) {
    console.log('Hello FusoHoraProvider Provider');
  }

  setUtc(dados) {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json;charset=UTF-8');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    console.log(dados);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.value.url + 'setUtc', JSON.stringify(dados), { headers: headers })
      // this.httpClient.post("http://172.20.10.3:8100/proxy/setUtc", JSON.stringify(dados), { headers: headers })        
        .subscribe(res => {
          console.log("Resposta setUtc Provider: ", res);
          resolve(res);
        }, (err) => {
          console.log("Erro setUtc Provider: ", err);
          reject(err);
        });
    });
  }

  getUtc() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.value.url + "getUtc", {})
      // this.httpClient.get("http://172.20.10.3:8100/proxy/getUtc")
        .subscribe(res => {
          console.log("Resposta GET /getUtc: ", res);
          resolve(res);
        }, (err) => {
          console.log("Resposta ERRO /getUtc: ", err);
          reject(err);
        });
    });
  }

}