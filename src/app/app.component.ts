import { Component } from '@angular/core';
import { Platform, LoadingController, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { HomeProvider } from '../providers/home/home';
import { LuminariaProvider } from '../providers/luminaria/luminaria';
import { Storage } from '@ionic/storage';
import { PresetsProvider } from '../providers/presets/presets';
import { UtilProvider } from '../providers/util/util';
import { ProcurarLuminariasPage } from '../pages/procurar-luminarias/procurar-luminarias';
import { AutenticacaoProvider } from '../providers/autenticacao/autenticacao';
import { AcessoNegadoPage } from '../pages/acesso-negado/acesso-negado';
import { ValuesProvider } from '../providers/values/values';
import { RecarregarPage } from '../pages/recarregar/recarregar';
// import { MqttService, MqttMessage } from 'ngx-mqtt';
import { FusoHoraProvider } from '../providers/fuso-hora/fuso-hora';
import { App } from 'ionic-angular';
import { RecuperarEndPage } from '../pages/recuperar-end/recuperar-end';
import { ResetValoresProvider } from '../providers/reset-valores/reset-valores';
import { IndisponivelPage } from '../pages/indisponivel/indisponivel';
import { EfeitoTempestadeProvider } from '../providers/efeito-tempestade/efeito-tempestade';


@Component({
  templateUrl: 'app.html',
})

export class MyApp {

  canaisLuminaria: number;
  rootPage: any = null;
  temperatura: string = "0";
  resposta: any;
  pronto: boolean = true;
  loading: any;
  nomesLuminarias: Array<any> = [];

  constructor(private platform: Platform, statusBar: StatusBar, private splashScreen: SplashScreen,
    public homeProvider: HomeProvider, private luminariaProvider: LuminariaProvider,
    public presetsProvider: PresetsProvider, private storage: Storage,/* private _mqttService: MqttService,*/
    private util: UtilProvider, private loader: LoadingController, private value: ValuesProvider,
    public alertCtrlr: AlertController, private autenticacaoProvider: AutenticacaoProvider,
    private fusoHoraProvider: FusoHoraProvider, private app: App, public events: Events,
    public efeitoTempestadeProvider: EfeitoTempestadeProvider,
    public resetValoresProvider: ResetValoresProvider) {

    // this.splashScreen.show();

    this.eventoNomeLuminaria();

    platform.registerBackButtonAction(() => {
      let nav = app.getActiveNavs()[0];
      let activeView = nav.getActive();
      console.log('activeView: ', activeView);

      if (activeView != null) {
        if ((typeof activeView.instance.backButtonAction === 'function'))
          activeView.instance.backButtonAction();

        else if (nav.canGoBack()) {
          nav.pop();
        }
        else
          nav.parent.select(0); // goes to the first tab
      }
    });

    platform.ready().then(() => {

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //this.storage.set("firstRun", 1);

      // _mqttService.connect({ username: 'xhqdirfe', password: 'w_wSnirTIXiq' });
      // _mqttService.connect(); 

      // console.log("Observe MQTT ...");
      // _mqttService////////////TESTE-MQTT
      //   .observe('aurora/temperatura/alerta')
      //   .subscribe((message: MqttMessage) => {
      //     console.log("MQTT: ", message.payload.toString());
      //   });

      this.storage.get('firstRun')
        .then((respfr) => {
          console.log("firstRun: ", respfr);
          if (respfr == null || respfr == 0) {
            console.log("Primeira execução");
            this.rootPage = ProcurarLuminariasPage;
            this.splashScreen.hide();
          } else {
            console.log("Segunda+ execução");
            this.carregarNomesLuminarias();
            let auth;
            this.storage.get("url")
              .then((resUrl) => {
                this.value.setUrl(resUrl);
                console.log("Sucesso ao resgatar o valor da url!", resUrl);
                // let urlRes;
                // if(resUrl.length > 0){
                //   urlRes = resUrl[0];
                // }else{
                //   urlRes = resUrl;
                // }
                // console.log("urlRes = ", urlRes);
                this.storage.get("urls").then(resUrls => {
                  let urls = [];
                  urls = resUrls;
                  console.log("app.components.ts urls resgatadas (Storage) = ", urls);
                  this.value.setUrls(urls);
                  let indexUrls = urls.findIndex(i => i.url === resUrl);
                  console.log("indexUrls = ", indexUrls);
                  if (indexUrls >= 0) {
                    this.autenticacaoProvider.getAutorizacaoJSON(resUrl)
                      .then(resp => {
                        console.log("Autorizacao: auth ESP (resp) =", resp);
                        auth = resp;
                        console.log("Autorizacao: auth ESP = ", auth['autorizacao']);
                        this.storage.get('auths')/////////Erro aqui. Investigar.
                          .then((resp) => {
                            let auths = [];
                            auths = resp;
                            console.log("app.component.ts Autorizações recuperadas (Storage) => ", auths);
                            console.log("auths[indexUrls].auth: " + auths[indexUrls].auth + " == " + "auth['autorizacao']: " + auth['autorizacao'] + " ?");
                            if (auths[indexUrls].auth == auth['autorizacao']) {
                              this.storage.set("auth", auths[indexUrls].auth);
                              this.carregarLuminaria();
                              // this.carregarPaginaInicial();
                            } else {
                              this.splashScreen.hide();
                              this.rootPage = AcessoNegadoPage;
                              console.log("Acesso negado!!!");
                            }
                          }).catch(error => {
                            console.error("Erro ao tentar recuperar a autenticacao em app.components.ts: ", error);
                          });
                      }).catch(err => {
                        console.error("ERRO getAutorizacao => ", err);
                        this.splashScreen.hide();
                        this.recuperarEnderecoIP();
                      });
                  } else {
                    console.error("Não existe a URL... indo para recuperarEnderecoIP()");
                    this.splashScreen.hide();
                    this.recuperarEnderecoIP();
                  }
                }).catch(errUrls => {
                  console.error("FALHA ao resgatar o valor das urls!", errUrls);
                  this.splashScreen.hide();
                  this.recuperarEnderecoIP();
                });
              }).catch(errUrl => {
                console.error("FALHA ao resgatar o valor da url!", errUrl);
                this.splashScreen.hide();
                this.recuperarEnderecoIP();
              });
          }
        });
    });
  }

  eventoNomeLuminaria() {
    this.events.subscribe('nomes', (nomes) => {
      this.nomesLuminarias = nomes;
      console.log("this.nomesLuminarias em app.componentes.ts = ", nomes);
    });
  }

  carregarPaginaInicial() {
    this.splashScreen.hide();
    this.rootPage = TabsPage;
  }

  paginaIndisponivel() {
    this.rootPage = RecarregarPage;
    //this.navCtrl.push(IndisponivelPage, { param: true });
  }

  recuperarEnderecoIP() {
    this.splashScreen.hide();
    this.rootPage = RecuperarEndPage;
  }

  carregarLuminaria() {
    // this.carregarNomesLuminarias();
    this.carregarTemperatura();
  }

  carregarNomesLuminarias() {
    this.storage.get("nomesLuminarias")
      .then(res => {
        if (res != null) {
          console.log("nomesLuminarias = ", res);
          this.nomesLuminarias = res;
          this.storage.get("nomeLuminaria").then(res00 => {
            // console.log("app.components.ts:: res00 ", res00);           
            console.log("nomeLuminaria = ", res00);
            let index = this.nomesLuminarias.findIndex(i => i.alias === res00);
            this.nomesLuminarias[index].selecionada = true;
            // this.gravarQtdeCanaisLuminarias(this.nomesLuminarias[index].name);
            console.log("app.components.ts:: Luminaria: " + res00 + " selecionada: " + this.nomesLuminarias[index].selecionada);
          });
          console.log("carregarNomesLuminarias() (Storage) app.componets.ts : ", this.nomesLuminarias);
        } else {
          console.log("Nenhuma luminaria encontrada anteriormente. res = ", res);
        }

      }).catch(err => {
        console.error("ERRO ao regatar carregarNomesLuminarias()");
      })
  }

  // gravarQtdeCanaisLuminarias(nomeLuminaria) {
  //   this.canaisLuminaria = Number(String(nomeLuminaria).charAt(2));
  //   console.log("*****Canais da luminária app.components.ts = ", this.canaisLuminaria);
  //   this.storage.set("canaisLuminaria", this.canaisLuminaria).then(res => {
  //     console.log("Número de canais gravados (Storage): ", res);
  //   })
  // }

  //////////////////home.ts////////////////////
  carregarTemperatura() {
    this.loading = this.loader.create({
      content: "Conectando à luminária, por favor aguarde.",
    });
    this.loading.present().then(() => {
      this.resposta = this.homeProvider.getTemperatura()
        .subscribe(data => {
          //this.temperatura = JSON.stringify(data);
          this.temperatura = data;
          console.log(" carregarTemperatura() :: this.temperatura = ", this.temperatura);
          this.storage.set("temperatura1", this.temperatura)
            .then(res => {
              console.log("Temperatura Inicial guardada!", res);
            }).catch(err => {
              console.error("ERRO!", err);
            })
          // this.loading.dismiss();
          // this.carregarFusoHorario();
          // this.getEstadoTemporizador();
          this.carregarConfiguracoes();
        },
        err => {
          //this.pronto = false;
          this.loading.dismiss();
          this.splashScreen.hide();
          this.temperatura = "FALHA...";
          console.log("Erro pronto!", this.pronto);
          console.error("Erro ao pegar temperatura!", err);
          this.recuperarEnderecoIP();
          return;
        })
    })
    //console.log("Fim do metodo: não retornou");   
  }

  carregarConfiguracoes() {
    this.luminariaProvider.getConfig().then(res => {
      console.log("Configurações gerais da luminária: ", res)
      let dadosJSON = JSON.parse(JSON.stringify(res));
      console.log("UTC app.component.ts => ", res);
      /////////////////////LUMINARIA////////////////////////////////
      this.storage.set('canaisLuminaria', dadosJSON['luminaria'])
        .then((resp) => {
          console.log("CANAISLUMINARIA armazenado (Storage) => ", resp);          
        })
      /////////////////////TWINSTAR////////////////////////////////
      this.storage.set('twinstar', dadosJSON['twinstar'] == 1 ? false : true)
        .then((resp) => {
          console.log("dadosJSON['twinstar'] = ", dadosJSON['twinstar'])
          console.log("TWINSTAR armazenado (Storage) => ", resp);
        })
      /////////////////////CAMARAUV////////////////////////////////
      this.storage.set('uv', dadosJSON['UV'] == 1 ? false : true)
        .then((resp) => {
          console.log("dadosJSON['UV'] = ", dadosJSON['UV'])
          console.log("CAMARAUV armazenado (Storage) => ", resp)
        })
      /////////////////////FEEDER////////////////////////////////
      this.storage.set('feeder', dadosJSON['feeder'] == 1 ? false : true)
        .then((resp) => {
          console.log("dadosJSON['feeder'] = ", dadosJSON['feeder'])
          console.log("FEEDER armazenado (Storage) => ", resp)
        })
      /////////////////////SENSORPH////////////////////////////////
      this.storage.set('sensorPh', dadosJSON['sensorPH'] == 1 ? false : true)
        .then((resp) => {
          console.log("dadosJSON['sensorPH'] = ", dadosJSON['sensorPH'])
          console.log("SENSORPH armazenado (Storage) => ", resp)
        })
      /////////////////////SENSORNIVEL////////////////////////////////
      this.storage.set('sensorNivel', dadosJSON['sensorNivel'] == 1 ? false : true)
        .then((resp) => {
          console.log("dadosJSON['sensorNivel'] = ",dadosJSON['sensorNivel'])
          console.log("SENSORNIVEL armazenado (Storage) => ", resp)
        })
      /////////////////////UTC////////////////////////////////
      this.storage.set('utc', dadosJSON['utc'])
        .then((resp) => {
          console.log("UTC armazenado (Storage) => ", resp);
        })
      /////////////////////POWER////////////////////////////////
      this.storage.set('power', dadosJSON['power'] == 1 ? true : false)
        .then((resp) => {
          console.log("POWER armazenado (Storage) => ", resp);
        })
      ////////////////ESTADO AGENDADOR/////////////////////       
      this.storage.set("temporizadorLigado", dadosJSON['timer']).then(resp => {
        console.log("TIMER armazenado (Storage) => ", resp);
      })
      ////////////////HORÁRIOS AGENDADOR/////////////////////  
      ////////////////PRESETS ORIGINAIS//////////////////////
      this.storage.set("presetsOriginais", dadosJSON['presets']).then(resp => {
        console.log("Presets originais armazenados!")
      })
      ///////////////////////////////////////////////////////
      let presets = [];
      // console.log("dadosJSON['agendador'] = ", dadosJSON['agendador'])
      // for (let i = 0; i < dadosJSON['agendador'].length; i++) {
      for (let i = 0; i < dadosJSON['presets'].length; i++) {
        presets.push(
          {
            nome: dadosJSON['presets'][i].name,
            hora: i < dadosJSON['agendador'].length ? this.util.convertMilisegundosHora(dadosJSON['agendador'][i].hrInicio) : null,
            gradual: i < dadosJSON['agendador'].length ? dadosJSON['agendador'][i].gradual : null,
            led1Tensao: 0,
            led2Tensao: 0,
            led3Tensao: 0,
            led4Tensao: 0,
            led5Tensao: 0,
            led6Tensao: 0,
            led7Tensao: 0,
            led8Tensao: 0,
            led9Tensao: 0,
          })
        console.log("GET Temporizador app.component - Iteração: ", i);
      }
      ////////////////LEDS MANUAL/////////////////////   
      this.canaisLuminaria = Number(dadosJSON['luminaria']);
      console.log("dadosJSON['presets'] app.components.ts = ", dadosJSON['presets']);
      console.log("dadosJSON['presets'].length app.components.ts = ", dadosJSON['presets'].length);
      console.log("this.canaisLuminaria em app.componets.ts", this.canaisLuminaria)
      for (let i = 0; i < dadosJSON['presets'].length; i++) {
        for (let j = 0; j < this.canaisLuminaria; j++) {
          presets[i]["led" + (j + 1) + "Tensao"] = dadosJSON['presets'][i]["led" + (j + 1) + "Tensao"];
          console.log(" presets[" + i + "].led" + [j + 1] + "Tensao = ", presets[i]["led" + (j + 1) + "Tensao"])
        }
      }

      this.storage.set('presets', presets)
        .then(res => console.log("gravando presets (storage) app.components.ts => ", presets))
        .catch(err => console.error("Erro Gravando presets (storage) app.components.ts => causa: ", err));

      // console.log("this.ledsPresets = ", this.ledsPresets)
      ////////////////EFEITO STORM/////////////////////  
      this.storage.set("storm", dadosJSON["storm"])
        .then(resStorm => {
          console.log("Storm: ", resStorm)
          // this.storage.set("modeStorm", dadosJSON["stormMode"])
          //   .then(resMode => {
          //     console.log("ModeStorm: ", resMode)
          this.carregarPaginaInicial();
          this.loading.dismiss();
          this.splashScreen.hide();
          // });
        });
      ////////////////LEDS MANUAL/////////////////////
      //Não há necessidade. Os presets são carregados a cada seleção.
    }).catch(err => {
      this.loading.dismiss();
      this.splashScreen.hide();
      console.log("Erro pronto!", this.pronto);
      this.recuperarEnderecoIP();
      console.error("Erro ao tentar recuperar Presets em app.component.ts: ", err);
      return;
    });
  }

  conectarLuminaria(index) {
    for (let i = 0; i < this.nomesLuminarias.length; i++) {
      this.nomesLuminarias[i].selecionada = false;
    }
    this.nomesLuminarias[index].selecionada = true;
    // this.gravarQtdeCanaisLuminarias(this.nomesLuminarias[index].name);
    console.log("Conectando a luminária: ", index);
    this.storage.get("urls")
      .then(res => {
        // this.publicarReiniciarViews();
        this.rootPage = IndisponivelPage;
        let urls = res;
        console.log("Endereço desta luminária: ", urls[index].url);
        this.value.setUrl(urls[index].url);
        this.storage.get("auths")
          .then(res0 => {
            let auths = res0;
            console.log("Atorização = ", auths[index].auth);
            this.storage.set("auth", auths[index].auth)
              .then(respAuth => {
                console.log("Atorização setada ", respAuth);
              })
            this.storage.set("nomeLuminaria", this.nomesLuminarias[index].alias)
              .then(respNome => {
                console.log("Nome da luminária alterado");
              })
            this.storage.set("nomesLuminarias", this.nomesLuminarias)
              .then(respNomes => {
                console.log("app.component nomesLuminarias alterado: ", respNomes);
              });
            this.carregarLuminaria();
          });
      });
  }

  publicarReiniciarViews() {
    this.events.publish('valorLuminaria', 0);
    console.log("Tentando mudar o nome da variavel em luminaria.ts...");
  }
}