import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Zeroconf } from '@ionic-native/zeroconf';

import { PresetsPageModule } from '../pages/presets/presets.module';
import { LuminariaPageModule } from '../pages/luminaria/luminaria.module';
import { IonicStorageModule } from '@ionic/storage';
import { IndisponivelPageModule } from '../pages/indisponivel/indisponivel.module';
import { RecarregarPageModule } from '../pages/recarregar/recarregar.module';
import { AlarmeTemperaturaPageModule } from '../pages/alarme-temperatura/alarme-temperatura.module';
import { ProcurarLuminariasPageModule } from '../pages/procurar-luminarias/procurar-luminarias.module';
import { AcessoNegadoPageModule } from '../pages/acesso-negado/acesso-negado.module';
import { ConfiguracoesPageModule } from '../pages/configuracoes/configuracoes.module';
import { ReiniciarappPageModule } from '../pages/reiniciarapp/reiniciarapp.module';

import { IndisponivelPage } from '../pages/indisponivel/indisponivel';
import { RecarregarPage } from '../pages/recarregar/recarregar';
import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ConfiguracoesPage } from '../pages/configuracoes/configuracoes';
import { AcessoNegadoPage } from '../pages/acesso-negado/acesso-negado';
import { ProcurarLuminariasPage } from '../pages/procurar-luminarias/procurar-luminarias';
import { ReiniciarappPage } from '../pages/reiniciarapp/reiniciarapp';

import { UtilProvider } from '../providers/util/util';
import { PresetsProvider } from '../providers/presets/presets';
import { HomeProvider } from '../providers/home/home';
import { ValuesProvider } from '../providers/values/values';
import { AutenticacaoProvider } from '../providers/autenticacao/autenticacao';
import { ProcurarLuminariasProvider } from '../providers/procurar-luminarias/procurar-luminarias';
import { RecarregarProvider } from '../providers/recarregar/recarregar';
import { AlarmeTemperaturaProvider } from '../providers/alarme-temperatura/alarme-temperatura';
import { LuminariaProvider } from '../providers/luminaria/luminaria';
import { ConfiguracoesProvider } from '../providers/configuracoes/configuracoes';

//import { SQLite } from '@ionic-native/sqlite';
import { LOCALE_ID } from '@angular/core/src/i18n/tokens';

// import { MqttModule, MqttService } from 'ngx-mqtt';
import { ResetFabricaProvider } from '../providers/reset-fabrica/reset-fabrica';
import { FusoHoraProvider } from '../providers/fuso-hora/fuso-hora';
import { Push, PushObject, PushOptions } from "@ionic-native/push";
import { FusoHoraPage } from '../pages/fuso-hora/fuso-hora';
import { FusoHoraPageModule } from '../pages/fuso-hora/fuso-hora.module';
import { ResetFabricaPage } from '../pages/reset-fabrica/reset-fabrica';
import { ResetFabricaPageModule } from '../pages/reset-fabrica/reset-fabrica.module';
import { EfeitosProvider } from '../providers/efeitos/efeitos';
import { EfeitosPage } from '../pages/efeitos/efeitos';
import { EfeitosPageModule } from '../pages/efeitos/efeitos.module';
import { EfeitoTempestadeProvider } from '../providers/efeito-tempestade/efeito-tempestade';
import { EfeitoTempestadePage } from '../pages/efeito-tempestade/efeito-tempestade';
import { EfeitoTempestadePageModule } from '../pages/efeito-tempestade/efeito-tempestade.module';

// import {ChartsModule} from 'ng2-charts'
import { RecuperarEndPage } from '../pages/recuperar-end/recuperar-end';
import { RecuperarEndPageModule } from '../pages/recuperar-end/recuperar-end.module';
import { WizardProvider } from '../providers/wizard/wizard';
import { ResetValoresProvider } from '../providers/reset-valores/reset-valores';
import { InfoLuminariaPage } from '../pages/info-luminaria/info-luminaria';
import { InfoLuminariaPageModule } from '../pages/info-luminaria/info-luminaria.module';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { TwinstarProvider } from '../providers/twinstar/twinstar';
import { FuncoesPageModule } from '../pages/funcoes/funcoes.module';
import { TwinstarPageModule } from '../pages/twinstar/twinstar.module';

// import { Http, HttpModule } from '@angular/http';

// export const MQTT_SERVICE_OPTIONS = {
//   connectOnCreate: true,
//   hostname: '179.55.123.225', 
//   port: 9001,
  
// };

// export function mqttServiceFactory() {
//   return new MqttService(MQTT_SERVICE_OPTIONS);  
// }

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
  ],

  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    PresetsPageModule,
    AlarmeTemperaturaPageModule,
    LuminariaPageModule,
    IonicStorageModule.forRoot({ name: 'aquasun' }),
    IndisponivelPageModule,
    RecarregarPageModule,
    ProcurarLuminariasPageModule,
    AcessoNegadoPageModule,
    ConfiguracoesPageModule,
    ReiniciarappPageModule,
    FusoHoraPageModule,
    ResetFabricaPageModule,
    EfeitosPageModule,
    EfeitoTempestadePageModule,
    FuncoesPageModule,
    TwinstarPageModule,
    // ChartsModule, 
    RecuperarEndPageModule,
    InfoLuminariaPageModule,    
    NgCircleProgressModule
    // MqttModule.forRoot({
    //   provide: MqttService, 
    //   useFactory: mqttServiceFactory
    // }),    
  ],

  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    IndisponivelPage,
    RecarregarPage,
    ProcurarLuminariasPage,
    AcessoNegadoPage,
    ConfiguracoesPage,
    ReiniciarappPage,
    FusoHoraPage,
    ResetFabricaPage,
    EfeitosPage,
    EfeitoTempestadePage,
    RecuperarEndPage,
    InfoLuminariaPage,
  ],

  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    //{provide: LOCALE_ID, useValue: 'pt-BR'},
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    PresetsProvider,
    HomeProvider,
    ValuesProvider,
    //SQLite,
    AlarmeTemperaturaProvider,
    LuminariaProvider,
    UtilProvider,
    ProcurarLuminariasProvider,
    RecarregarProvider,
    Zeroconf,
    AutenticacaoProvider,
    ConfiguracoesProvider,
    ResetFabricaProvider,
    FusoHoraProvider,
    Push,
    EfeitosProvider,
    EfeitoTempestadeProvider,
    WizardProvider,
    ResetValoresProvider,
    TwinstarProvider,
    // Http,
    // HttpModule
  ]
})

export class AppModule { }
